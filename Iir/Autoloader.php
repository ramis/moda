<?php
namespace Iir;

class Autoloader
{

	public static function registerAutoload ()
	{
		spl_autoload_register (array (__CLASS__, 'autoload'));
	}

	/**
	 * Универсальный загрузчие, совместим с Zend и большей частью современных библиотек
	 *
	 * @static
	 * @param string $class
	 * @return void
	 */
	public static function autoload ($class)
	{
		require str_replace (array ('_', '\\'), '/', $class) . '.php';
	}

}

