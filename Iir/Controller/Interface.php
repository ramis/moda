<?php
namespace Iir;

interface Controller_Interface
{

	/**
	 * Хук перед вызовов action
	 * Применяется для общих действий всех action'ов контроллера, например для обработки общей части url
	 * Action вызывается только если preActiob возвращает true
	 *
	 * @param Route_Result $route_result
	 * @return boolean
	 */
	function preAction (Route_Result $route_result);

}
