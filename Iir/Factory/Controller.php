<?php
namespace Iir;

/**
 * @deprecated
 */
class Factory_Controller
{

	/**
	 *
	 * @var array
	 */
	private static $controllers;

	/**
	 *
	 * @var array
	 */
	private static $helpers;

	/**
	 *
	 * @var string
	 */
	private static $namespace, $prefix;

	public static function setPrefix ($prefix)
	{
		self::$prefix = $prefix;
	}

	/**
	 * Задать пространство имен
	 *
	 * @static
	 * @param sting $namespace_default
	 * @return void
	 */
	public static function setNamespace ($namespace)
	{
		self::$namespace = $namespace;
	}

	/**
	 * Получить контроллер
	 *
	 * @param string $controller
	 * @return object
	 */
	public static function get ($controller)
	{
		if ($controller != '' && isset (self::$controllers[$controller]) === false) {
			$class_name = '\\' . self::$namespace . '\\Domain\\' . self::$prefix . "\\Controller\\$controller";

			self::$controllers[$controller] = new $class_name;

		}

		return self::$controllers[$controller];
	}

	/**
	 * Отдать хелпер
	 *
	 * @param string $helper
	 * @return object
	 */
	public static function getHelper ($helper)
	{
		if (!isset (self::$helpers[$helper])) {
			$class_name = '\\' . self::$namespace .
				'\\Domain_' . self::$prefix . '_Controller_Helper_' . $helper;

			self::$helpers[$helper] = new $class_name;
		}

		return self::$helpers[$helper];
	}

}
