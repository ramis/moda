<?php
namespace Iir;

/**
 * @deprecated
 */
class Factory_Fetcher
{

	private static $existed = array();
	private static $namespace_default = null;

	/**
	 * Задать пространство имен по умолчанию
	 *
	 * @static
	 * @param sting $namespace_default
	 * @return void
	 */
	public static function setDefaultNamespace ($namespace_default)
	{
		self::$namespace_default = $namespace_default;
	}

	/**
	 *  Wrapper для Factory_Fetcher::__callStatic ()
	 *
	 * @param string $fetcher
	 * $param string|null $namespace
	 * @return object
	 */
	public static function get ($fetcher, $namespace = null)
	{
		return self::$fetcher ($namespace);
	}

	/**
	 * Получить фетчер
	 *
	 * @param string $fetcher
	 * $param string|null $namespace
	 * @return object
	 */
	public static function __callStatic ($fetcher, $arguments)
	{
		$namespace = isset ($arguments[0]) ? $arguments[0] : self::$namespace_default;

		$class_name = '\\' . $namespace . '\Fetcher_' . $fetcher;
		if (!isset (self::$existed[$class_name])) {
			self::$existed[$class_name] = new $class_name;
		}

		return self::$existed[$class_name];
	}

}
