<?php
namespace Iir;

class Front_Controller
{

	/**
	 * @var Front_Controller
	 */
	private static $instance;

	/**
	 * @var array
	 */
	private static $plugins = array ();

	/**
	 * @var Router
	 */
	private static $router;

	/**
	 * @var bool
	 */
	private static $dispatch;

	private function __construct ()
	{
	}

	public function __clone ()
	{
		throw new Exception ('singleton clone is not allowed');
	}

	/**
	 * singleton instance
	 *
	 * @return Front_Controller
	 */
	public static function getInstance ()
	{
		if (!isset(self::$instance)) {
			self::$instance = new Front_Controller;
		}

        return self::$instance;
	}

	/**
	 * Вернуть роутер
	 *
	 * @return Router
	 */
	public static function getRouter ()
	{
		return self::$router;
	}

	/**
	 * Задать роутер
	 *
	 * @param $router
	 * @return void
	 */
	public static function setRouter (Router $router)
	{
		self::$router = $router;
	}

	/**
	 * Самый Главный Метод
	 *
	 * @return void
	 */
	public static function run ()
	{
		$route_result = self::$router->parseRequestUrl ();
		//var_dump ($route_result);

		//по умолчанию диспетчеризация нужна
		self::$dispatch = true;

		//тут будет запуск зарегистрированных плагинов, примерно так:
		foreach (self::$plugins as $plugin) {
			$plugin->preDispatch ($route_result);

			//быть может плагин отменил диспетчеризацию
			if (self::$dispatch === false) {
				//отбой дальнейшей работе
				return;
			}
		}

		//дошли до диспетчеризации

		//на этом этапе и контроллер и экшн уже точно определены:
		// - либо роутером
		// - либо плагинами

		/**
		 * @var Controller_Interface $controller
		 */
		$controller = Factory_Controller::get ($route_result->getController ());
		//var_dump ($controller);die;

		if (!($controller instanceof Controller_Interface)) {
//			throw new Exception ('Controller ' . $route_result->getController () .
//				' does not implements Controller_Interface');
		}

		//сюда можно впихнуть проверку на существование контроллера и действия в нем..
		//но можно и не впихивать :-)

		if ($controller instanceof Controller_Interface && $controller->preAction ($route_result)) {
			$callback = array ($controller, 'action' . $route_result->getAction ());
			call_user_func_array ($callback, $route_result->getParams ());
		}
	}

	/**
	 * Зарегистрировать плагин
	 *
	 * @param Front_Controller_Plugin_Interface $plugin
	 * @return void
	 */
	public static function registerPlugin (Front_Controller_Plugin_Interface $plugin)
	{
		self::$plugins[] = $plugin;
	}

	/**
	 * Отменить дальнейшую диспетчеризацию (вызов плагинов и контроллера)
	 *
	 * @return void
	 */
	public static function cancelDispatch ()
	{
		self::$dispatch = false;
	}

	/**
	 *
	 *
	 * @return void
	 */
	public static function hookBeforeRender ()
	{
		foreach (self::$plugins as $plugin) {
			$plugin->postRender ();
		}
	}
}
