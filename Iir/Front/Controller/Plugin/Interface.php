<?php
namespace Iir;

interface Front_Controller_Plugin_Interface
{

	/**
	 * Хук перед диспетчеризацией
	 * Например для контроля прав уровня ядра или отлова 404 ошибки
	 *
	 * @param Route_Result $route_result
	 * @return void
	 */
	public function preDispatch (Route_Result $route_result);

	/**
	 * Перед окончанием вывод результатов работы
	 *
	 * @return void
	 */
	public function postRender ();
}
