<?php
namespace Iir;

/**
 * @deprecated
 */
class Queue
{

	/**
	 * $var \Zend_Log
	 */
	private $log;

	/**
	 * $var \ArrayObject
	 */
	private $workers;

	public function __construct ()
	{
		$this->workers = new \ArrayObject;
	}

	/**
	 * Задать логгер
	 *
	 * @param \Zend_Log $log
	 * @return void
	 */
	public function setLog (\Zend_Log $log)
	{
		$this->log = $log;
	}

	/**
	 * Добавить обработчик
	 *
	 * @param Queue_Worker_Interface $worker
	 * @return void
	 */
	public function appendWorker (Queue_Worker_Interface $worker)
	{
		if ($this->log === null) {
			throw new \Exception ('no log object provided');
		}

		$worker->setLog ($this->log);
		$this->workers->append ($worker);
	}

	/**
	 * Обработать событие
	 *
	 * @param Queue_Item_Interface $item
	 * @return boolab
	 */
	public function process (Queue_Item_Interface $item)
	{
		//уникальный id процесса обработки данного элемента очереди
		$process_md5 = md5 (uniqid (rand (), true));

		$this->log->log ((string)$item, \Zend_Log::INFO, array ('process_md5' => $process_md5));

		$worker_found = false;
		foreach ($this->workers as $worker) {
			/**
			 * @var Queue_Worker_Interface $worker
			 */
			if ($worker->isSuitable ($item)) {
				$this->log->log ('Подходящий обработчик найден - ' . get_class ($worker), \Zend_Log::INFO,
					array ('process_md5' => $process_md5));

				$worker->setProcessMd5 ($process_md5);
				$worker->process ($item);
				$worker_found = true;
				break;
			}
		}

		if ($worker_found === false) {
			$this->log->log ('Подходящий обработчик не найден', \Zend_Log::ERR,
				array ('process_md5' => $process_md5));
		}

		$this->log->log ('Обработка завершена', \Zend_Log::INFO, array ('process_md5' => $process_md5));

		return $worker_found;
	}

}
