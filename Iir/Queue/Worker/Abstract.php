<?php
namespace Iir;

abstract class Queue_Worker_Abstract
{

	/**
	 * @var Zend_Log
	 */
	protected $log;

	/**
	 * @var string
	 */
	protected $process_md5;

	/**
	 * Установить логгер
	 *
	 * @param Zend_Log $log
	 * @return void
	 */
	public function setLog (\Zend_Log $log)
	{
		$this->log = $log;
	}

	/**
	 * Задать уникальный идентификатор процесса
	 *
	 * @param string $process_md5
	 * @return void
	 */
	public function setProcessMd5 ($process_md5)
	{
		$this->process_md5 = $process_md5;
	}

	/**
	 * Сделать запись в Зендовский лог
	 *
	 * @param string $message
	 * @param int $priority
	 * @return void
	 */
	protected function log ($message, $priority)
	{
		$this->log->log ($message, $priority, array (
			'process_md5' => $this->process_md5,
		));
	}

	/**
	 *  Записать в лог ошибку - \Zend_Log::ERR
	 *
	 * @param  $message
	 * @return void
	 */
	protected function logE ($message)
	{
		$this->log ($message, \Zend_Log::ERR);
	}

	/**
	 *  Записать в лог информационное сообщение
	 *
	 * @param  $message
	 * @return void
	 */
	protected function logI ($message)
	{
		$this->log ($message, \Zend_Log::INFO);
	}

}
