<?php
namespace Iir;;

interface Queue_Worker_Interface
{

	/**
	 * Понимает ли разборщик такой элемент очереди
	 *
	 * @abstract
	 * @param Queue_Item_Interface $item
	 * @return boolan
	 */
	function isSuitable (Queue_Item_Interface $item);

	/**
	 * Задать логгер
	 *
	 * @abstract
	 * @param \Zend_Log $log
	 * @return void
	 */
	function setLog (\Zend_Log $log);

	/**
	 * Задать уникальный id процесса
	 *
	 * @abstract
	 * @param string $process_md5
	 * @return void
	 */
	function setProcessMd5 ($process_md5);

	/**
	 * Выполнить элемент очереди
	 *
	 * @abstract
	 * @param Queue_Item_Interface $item
	 * @param string $process_id
	 * @return boolean
	 */
	function process (Queue_Item_Interface $item);

}
