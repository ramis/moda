<?php
namespace Iir;

class Request
{

	public static function isParam ()
	{
		$keys = func_get_args ();
		foreach ($keys as $key) {
			if (isset ($_REQUEST[$key]) === false) {
				return false;
			}
		}
		return true;
	}

	public static function isGetParam ()
	{
		$keys = func_get_args ();
		foreach ($keys as $key) {
			if (isset ($_GET[$key]) === false) {
				return false;
			}
		}
		return true;
	}

	public static function isPostParam ()
	{
		$keys = func_get_args ();
		foreach ($keys as $key) {
			if (isset ($_POST[$key]) === false) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Вернуть параметр запроса
	 *
	 * @param string $key
	 * @param mixed $default_value_if_param_not_exists
	 * @return string|array
	 */
	public static function getParam ($key, $default_value_if_param_not_exists = null)
	{
		return isset ($_REQUEST[$key]) ? $_REQUEST[$key] : $default_value_if_param_not_exists;
	}

	/**
	 * Вернуть параметр запроса
	 *
	 * @param string $key
	 * @param mixed $default_value_if_param_not_exists
	 * @return string|array
	 */
	public static function getGetParam ($key, $default_value_if_param_not_exists = null)
	{
		return isset ($_GET[$key]) ? $_GET[$key] : $default_value_if_param_not_exists;
	}

	/**
	 * Вернуть параметр запроса
	 *
	 * @param string $key
	 * @param mixed $default_value_if_param_not_exists
	 * @return string|array
	 */
	public static function getPostParam ($key, $default_value_if_param_not_exists = null)
	{
		return isset ($_POST[$key]) ? $_POST[$key] : $default_value_if_param_not_exists;
	}

	/**
	 * Проверить наличие куки
	 *
	 * @return bool
	 */
	public static function isCookie ()
	{
		$keys = func_get_args ();
		foreach ($keys as $key) {
			if (isset ($_COOKIE[$key]) === false) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Вернуть значение куки
	 *
	 * @param string $key
	 * @param mixed $default_value_if_param_not_exists
	 * @return string|array
	 */
	public static function getCookie ($key, $default_value_if_param_not_exists = null)
	{
		return isset ($_COOKIE[$key]) ? $_COOKIE[$key] : $default_value_if_param_not_exists;
	}

	/**
	 * Вернуть значение куки
	 *
	 * @param string $key
	 * @param string $value
	 * @param int $expire секунды после текущего time ()
	 * @param string $path путь
	 * @return void
	 */
	public static function setCookie ($key, $value, $expire, $path = '/')
	{
		setcookie ($key, $value, time() + $expire, $path);
		$_COOKIE[$key] = $value;
	}

	/**
	 * Был ли загружен файл
	 *
	 * @param string $field_name
	 * @param string|null $key ключ массива, например input name="$field_name[$key]"
	 * @return boolean
	 */
	public static function isUploadedFile ($alias, $key = null)
	{
		return $key === null ?
			isset ($_FILES[$alias]) && $_FILES[$alias]['error'] !== UPLOAD_ERR_NO_FILE :
			isset ($_FILES[$alias]['name'][$key]) && $_FILES[$alias]['error'][$key] !== UPLOAD_ERR_NO_FILE;
	}

	/**
	 * Количество загруженных файлов (несколько <input type="file" name="a[]"/>)
	 *
	 * @param stirng $alias
	 * @return false|int
	 */
	public static function getUploadedFilesCount ($alias)
	{
		if (isset($_FILES[$alias])) {
			return is_array($_FILES[$alias]) ? count($_FILES[$alias]) : 1;
		} else {
			return false;
		}
	}

	/**
	 * Был ли загружен файл без ошибок
	 *
	 * @param string $field_name
	 * @param string|null $key ключ массива, например input name="$field_name[$key]"
	 * @return boolean
	 */
	public static function isUploadedFileOk ($alias, $key = null)
	{
		return $key === null ?
			$_FILES[$alias]['error'] === UPLOAD_ERR_OK :
			$_FILES[$alias]['error'][$key] === UPLOAD_ERR_OK;
	}

	/**
	 * Путь загруженного файла
	 *
	 * @param string $field_name
	 * @param string|null $key ключ массива, например input name="$field_name[$key]"
	 * @return string
	 */
	public static function getUploadedFilePath ($alias, $key = null)
	{
		return $key === null ?
			$_FILES[$alias]['tmp_name'] :
			$_FILES[$alias]['tmp_name'][$key];
	}

	/**
	 * Type загруженного файла
	 *
	 * @param string $field_name
	 * @param string|null $key ключ массива, например input name="$field_name[$key]"
	 * @return string
	 */
	public static function getUploadedFileType ($alias, $key = null)
	{
		return $key === null ?
			$_FILES[$alias]['type'] :
			$_FILES[$alias]['type'][$key];
	}

	/**
	 * Имя загруженного файла на компьютере клиенте
	 *
	 * @param string $field_name
	 * @param string|null $key ключ массива, например input name="$field_name[$key]"
	 * @return string
	 */
	public static function getUploadedFileName ($alias, $key = null)
	{
		return $key === null ?
			$_FILES[$alias]['name'] :
			$_FILES[$alias]['name'][$key];
	}

	/**
	 * Был ли POST запрос
	 *
	 * @return boolean
	 */
	public static function isRequestPost ()
	{
		return $_SERVER['REQUEST_METHOD'] === 'POST';
	}

	/**
	 * Был ли GET запрос
	 *
	 * @return boolean
	 */
	public static function isRequestGet ()
	{
		return $_SERVER['REQUEST_METHOD'] === 'GET';
	}

}
