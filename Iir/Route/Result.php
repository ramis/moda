<?php
namespace Iir;

class Route_Result
{

	/**
	 * @var string
	 */
	private $controller, $action, $route_name;

	/**
	 * @var array
	 */
	private $params = array ();

	public function getController ()
	{
		return $this->controller;
	}

	public function setController ($controller)
	{
		$this->controller = $controller;
	}

	public function getAction ()
	{
		return $this->action;
	}

	public function setAction ($action)
	{
		$this->action = $action;
	}

	public function getParams ()
	{
		return $this->params;
	}

	public function setParams ($params)
	{
		$this->params = $params;
	}

	public function getRouteName ()
	{
		return $this->route_name;
	}

	public function setRouteName ($route_name)
	{
		$this->route_name = $route_name;
	}

}
