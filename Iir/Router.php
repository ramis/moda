<?php
namespace Iir;

class Router
{

	const ROUTE_STATIC = 0;
	const ROUTE_REGEXP = 1;

	/**
	 * Маршруты
	 *
	 * @var array
	 */
	private $routes = array ();

	/**
	 * Абсолютный путь корня сайта, слеш на конце обязатален
	 *
	 * @var string
	 */
	private $base_href;

	/**
	 * Разобрать запрошенный URL
	 *
	 * @return Route_Result
	 */
	public function parseRequestUrl ()
	{
		$result = new Route_Result;
		$result->setController ('Index');
		$result->setAction ('HomePage');

		//print_r ($_SERVER);
		//die;

		//url без сервера
		//убираем первый слеш
		$url_cutted = $_SERVER['REQUEST_URI'] == '/' ? '' : substr ($_SERVER['REQUEST_URI'], 1);
		$url_cutted = strpos ($url_cutted, '?') !== false ?
			substr ($url_cutted, 0, strpos ($url_cutted, '?')) :
			$url_cutted;

		//var_dump ($_SERVER['REQUEST_URI']);
		//var_dump ($url_cutted);
		//die;

		return $this->parseCuttedURL ($url_cutted);
	}

	/**
	 * Импользуется только для отладки
	 *
	 * @param string $url_cutted
	 * @return Route_Result
	 */
	public function testParseCuttedURL ($url_cutted)
	{
		return $this->parseCuttedURL ($url_cutted);
	}

	/**
	 * Самый главный метод разбора URL
	 *
	 * @param string $url
	 * @return Route_Result
	 */
	private function parseCuttedURL ($url_cutted)
	{
		$result = new Route_Result;

		foreach ($this->routes as $name => $params) {
			if ($params[0] === self::ROUTE_STATIC) {
				//static route
				if ($url_cutted === ($params[1] === '' ? '' : $params[1] . '/')) {
					$result->setRouteName ($name);
					$result->setController ($params[2]);
					$result->setAction ($params[3]);

					break;
				}
			} else {//if ($params[0] === self::ROUTE_REGEXP) {
				//regexp route
				if (preg_match ('#^' . $params[1] . '/$#', $url_cutted, $route_params)) {
					$result->setRouteName ($name);
					$result->setController ($params[2]);
					$result->setAction ($params[3]);

					array_shift ($route_params);
					$result->setParams ($route_params);

					break;
				}
			}
		}

		return $result;
	}

	/**
	 * Задать маршруты
	 *
	 * @param string $base_href абсолютный путь корня сайта, слеш на конце обязатален
	 * @param array $routes
	 * @return void
	 */
	public function setRoutes ($base_href, array $routes)
	{
		$this->base_href = $base_href;
		$this->routes = $routes;
	}

	/**
	 * Вернуть все маршруты
	 *
	 * @return array|null
	 */
	public function getRoutes ()
	{
		return $this->routes;
	}

	/**
	 * Вернуть конкретный роут по названию
	 *
	 * @param string $name
	 * @return array|false
	 */
	public function getRoute($name)
	{
		if (isset($this->routes[$name])){
			return $this->routes[$name];
		} else {
			return false;
		}
	}

	/**
	 * Построение url
	 *
	 * Параметры
	 * 	Первый - идентификатор маршрута, ключ $this->routes
	 * 	Второй - если true - то строить абсолютный адрес, если нет - то как параметр
	 * 	Последний - если массив то get параметры
	 *  Все промежуточные - подстроки для подстановки в regexp шаблон
	 *
	 * @return string
	 */
	public function buildUrl ()
	{
		$args = func_get_args ();

		//route
		if (isset ($this->routes[$args[0]])) {
			$route = $this->routes[$args[0]];
		} else {
			return false;
		}
		unset ($args[0]);

		//absolute
		if (isset ($args[1]) && $args[1] === true) {
			$url = $this->base_href;
			unset ($args[1]);
		} else {
			$url = '/';
		}

		//get params
		end ($args);
		if (is_array (current ($args))) {
			$get = '?';
			foreach (current ($args) as $key => $value) {
				$get.= $key .'=' . urlencode ($value) . '&';
			}
			$get = substr ($get, 0, -1);
			array_pop ($args);
		} else {
			$get = '';
		}

		//route
		if ($route[0] == 'static') {
			$url .= $route[1] === '' ? '' : $route[1] . '/';
		} else {//if ($route[0] == 'regexp') {
			$url .= vsprintf ($route[4] . '/', $args);
		}

		return $url . $get;
	}

}
