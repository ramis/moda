<?php
/**
 * Абстрактный класс, от которого можно наследовать синглетоны
 */
namespace Iir;

abstract class SingletonAbstract
{

	private static $_instances = array ();

	protected function __construct ()
	{
	}

	public static function getInstance ()
	{
		$class = get_called_class ();

		if (!isset (self::$_instances[$class])) {
			self::$_instances[$class] = new $class ();
		}

		return self::$_instances[$class];
	}

	public function __clone ()
	{
		throw new Exception ('singleton clone ' . get_called_class () . ' is not allowed');
	}

}
