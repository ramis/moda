<?php
namespace Iir;

abstract class View_Abstract
{

	/**
	 * Префикс для подключения view по render
	 *
	 * @deprecated
	 * @var string
	 */
	protected $path;

	/**
	 * Задать префикс для подключения view по parse
	 *
	 * @deprecated
	 * @param string $path
	 * @return void
	 */
	protected function setPath ($path)
	{
		$this->path = $path;
	}

	/**
	 * Задать параметр
	 *
	 * @deprecated
	 * @param string $key
	 * @param string $value
	 * @return View_Abstract
	 */
	public function param ($key, $value)
	{
		$this->$key = $value;
		return $this;
	}

	/**
	 * Задать параметры
	 *
	 * @deprecated
	 * @param  $params
	 * @return View_Abstract
	 */
	public function params (array $params)
	{
		foreach ($params as $key => $value) {
			$this->$key = $value;
		}
		return $this;
	}

	/**
	 * Показать шаблон
	 *
	 * @deprecated
	 * @param strin $template
	 * @return void
	 */
	public function render ($template, array $params = array ())
	{
		foreach ($params as $key => $value) {
			$this->$key = $value;
		}

		require $this->path . $template . '.php';
	}

	/**
	 * Построить относительный url (/catalog/...)
	 *
	 * @return string
	 */
	public static function url ()
	{
		$args = func_get_args ();
		$callback = array (\Iir\Front_Controller::getRouter (), 'buildUrl');
		return call_user_func_array ($callback, $args);
	}

	/**
	 * Построить абсолютный url
	 *
	 * @return string
	 */
	public static function urlA ()
	{
		$args = func_get_args ();
		if (!isset ($args[1]) || $args[1] !== true) {
			//флаг абсолютного пути надо добавить
			$route = array_shift ($args);
			array_unshift ($args, true);
			array_unshift ($args, $route);
		}
		$callback = array (\Iir\Front_Controller::getRouter (), 'buildUrl');
		return call_user_func_array ($callback, $args);
	}

}
