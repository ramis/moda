<?php

namespace Image;


class Storage{

	private static $storage;

	private static $path_storage;

	private function __construct (){

	}

	public static function config (){

		if(self::$storage === null){
			self::$storage = new self;

			self::$path_storage = BASE_PATH_IMAGE;
		}

		return self::$storage;
	}

	/**
	 * Получить файл по хешу
	 *
	 * @static
	 * @param string $hash
	 * @return string
	 */
	public static function get ($hash){

		return file_get_contents(self::$path_storage . $hash);
	}

	/**
	 * Сохранить в файл контент
	 *
	 * @static
	 * @param string $content
	 * @param string $ext
	 * @return string
	 */
	public static function set ($content, $ext = 'jpg'){

		$hash = uniqid() . rand(0,5) . '.' . $ext;

		file_put_contents(self::$path_storage . $hash, $content);

		return $hash;
	}


	/**
	 * Сохранить файл по пути
	 *
	 * @static
	 * @param string $path
	 * @param string $ext
	 * @return string
	 */
	public static function setFile ($path, $ext = 'jpg'){

		if(!is_file($path)){
			return false;
		}

		$hash = uniqid() . rand(0,5). '.' . $ext;

		file_put_contents(self::$path_storage . $hash, file_get_contents($path));

		return $hash;
	}

	/**
	 * Сохранить файл в указанную директорию
	 *
	 * @static
	 * @param string $hash
	 * @param string $path
	 * @return boolean
	 */
	public static function copy ($hash, $path){

		if(!is_file(self::$path_storage . $hash)){
			return false;
		}

		file_put_contents($path, self::get($hash));

		return true;
	}

	/**
	 * Удалить файл
	 *
	 * @static
	 * @param string $hash
	 * @return boolean
	 */
	public static function delete ($hash){

		if(!is_file(self::$path_storage . $hash)){
			return false;
		}

		unlink (self::$path_storage . $hash);

		return true;
	}

}