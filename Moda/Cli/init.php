<?php
if(getenv ('APPLICATION_SITE')){
	define ('APPLICATION_SITE', getenv('APPLICATION_SITE'));
}else{
	define ('APPLICATION_SITE', 'moda.pamuc.ru');
}

define ('PATH', realpath (dirname (__FILE__) . '/../../') . '/');

set_include_path (get_include_path () . PATH_SEPARATOR . PATH);

require PATH . 'vendor/autoload.php';

//bootstrap
$bootstrap = new Moda\Cli\Bootstrap;

$bootstrap->initConfig ();

$bootstrap->initImageStorage ();