<?php
namespace Moda;
/**
 * Обработка очереди событий - боевая
 */
require dirname(__FILE__) . "/init.php";

print ("queue process begin\n");

//файл с id процесса
$pidfile = PATH  . 'data/import-excel.pid';

//параллельно разбирать очередь нельзя
if (Common\ProcessChecker::isRunning ($pidfile, true) === false) {
	print ("go\n");

	Common\ProcessChecker::saveCurrentPid ($pidfile);

	$queue = Common\Repositories::queue()->findOneBy(array('is_processed' => false));

	if ($queue instanceof Entity\Queue) {
		$worker = 'Moda\\Queue\\' . $queue->getWorker();

		$worker = new $worker;

		$worker->process ($queue);

	} else {
		print ("queue empty\n");
	}

	unlink ($pidfile);
} else {
	print ("another queue process still running\n");
}

print ("queue process done\n");
