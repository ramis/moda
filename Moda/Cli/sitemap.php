<?php
namespace Moda;
/**
 * Генерация карты сайта
 */
require dirname(__FILE__) . "/init.php";

print ("sitemap process begin\n");

$dir = PATH . 'Moda/Domain/Front/htdocs/sitemaps/';

$dom_document = new \DOMDocument('1.0', 'UTF-8');
$dom_document->formatOutput = true;
$url_set = $dom_document->appendChild(new \DOMElement('urlset'));
$url_set->setAttributeNode(new \DOMAttr('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9'));

$url = $url_set->appendChild(new \DOMElement('url'));
$location = $url->appendChild(new \DOMElement('loc'));
$location->appendChild(new \DOMText(DOMAIN_HTTP_FRONT));
$url->appendChild(new \DOMElement('changefreq', 'daily'));
$url->appendChild(new \DOMElement('priority', '0.8'));
$url->appendChild(new \DOMElement('lastmod', str_replace(" ", "T", date ('Y-m-d H:i:s')."+03:00")));

$url = $url_set->appendChild(new \DOMElement('url'));
$location = $url->appendChild(new \DOMElement('loc'));
$location->appendChild(new \DOMText(DOMAIN_HTTP_FRONT . 'catalog/'));
$url->appendChild(new \DOMElement('changefreq', 'daily'));
$url->appendChild(new \DOMElement('priority', '0.5'));
$url->appendChild(new \DOMElement('lastmod', str_replace(" ", "T", date ('Y-m-d H:i:s')."+03:00")));

for ($i = 0; $i< 26; $i++){
	$loc =  DOMAIN_HTTP_FRONT  . 'catalog/' . chr(97+$i) . '/';
	$url = $url_set->appendChild(new \DOMElement('url'));
	$location = $url->appendChild(new \DOMElement('loc'));
	$location->appendChild(new \DOMText($loc));
	$url->appendChild(new \DOMElement('changefreq', 'daily'));
	$url->appendChild(new \DOMElement('priority', '0.2'));
	$url->appendChild(new \DOMElement('lastmod', str_replace(" ", "T", date ('Y-m-d H:i:s')."+03:00")));
}

$brands = Common\Repositories::brand()->findBy(array(), array('title'=>'asc'));
foreach($brands as $brand){
	$url = $url_set->appendChild(new \DOMElement('url'));
	$location = $url->appendChild(new \DOMElement('loc'));
	$location->appendChild(new \DOMText(DOMAIN_HTTP_FRONT . $brand->getUrl() . '/'));
	$url->appendChild(new \DOMElement('changefreq', 'daily'));
	$url->appendChild(new \DOMElement('priority', '0.7'));
	$url->appendChild(new \DOMElement('lastmod', str_replace(" ", "T", date ('Y-m-d H:i:s')."+03:00")));
}


if(file_exists($dir . "catalog.xml")){
	unlink($dir . "catalog.xml");
}

file_put_contents($dir . "catalog.xml", $dom_document->saveXML ());


$file = $dir . 'sitemap.xml';

$dom = new \DOMDocument('1.0', 'UTF-8');
$dom->formatOutput = true;
$sitemapindex = $dom->appendChild(new \DOMElement('sitemapindex'));
$sitemapindex->setAttributeNode(new \DOMAttr('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9'));

if(file_exists($file)){
	unlink($file);
}

foreach(scandir($dir) as $files){
	if(!is_dir($dir . $files) && preg_match("/^[A-Za-z_\-\d]+\.xml$/", $files)){
		$sitemap = $sitemapindex->appendChild(new \DOMElement('sitemap'));
		$sitemap->appendChild(new \DOMElement('loc', DOMAIN_HTTP_FRONT . 'sitemaps/' . $files));
		$sitemap->appendChild(new \DOMElement('lastmod', date("Y-m-d") . "T" . date("H:i:s") . "+03:00"));
	}
}
file_put_contents($file, $dom->saveXML ());

print ("sitemap process done\n");
