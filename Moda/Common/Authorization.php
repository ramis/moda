<?php

namespace Moda\Common;

use Moda;

class Authorization{

	private $user;

	private static $authorization;

	private function __construct (){

	}

	public static function getInstance (){
		if(self::$authorization === null){
			if(isset($_SESSION['authorization']) && $_SESSION['authorization'] instanceof self){
				self::$authorization = $_SESSION['authorization'];
			}else{
				self::$authorization = new self;
				$_SESSION['authorization'] = self::$authorization;
			}
		}
		return self::$authorization;
	}

	public function login ($nick, $passwd){
		$this->user = Repositories::user()->findOneBy(array ('nick' => $nick, 'passwd' => $passwd));
		$_SESSION['authorization'] = self::$authorization;
		return ($this->user instanceof Moda\Entity\User);
	}

	public function logout (){
		self::$authorization = null;
		unset ($_SESSION['authorization']);
	}

	public function isAuthorization (){
		return ($this->user instanceof Moda\Entity\User);
	}

}