<?php
namespace Moda\Common;

use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

class Bootstrap
{

	protected static $em;
	protected static $m;

	/**
	 * Инициализировать Iir
	 *
	 * @return void
	 */
	public function initIir ()
	{
		//фабрика фетчеров
		\Iir\Factory_Fetcher::setDefaultNamespace ('Moda');

		//фабрика контроллеров
		\Iir\Factory_Controller::setNamespace ('Moda');
		\Iir\Factory_Controller::setPrefix (PATH . 'app/Moda/controller/');

	}


	/**
	 * Обычный старт сессии
	 *
	 * @return void
	 */
	public function initSessionSimple ()
	{
		session_start ();
	}

	/**
	 * Старт сессии с session_id из $_POST['PHPSESSID']
	 *
	 * @return void
	 */
	public function initSessionFromPost ()
	{
		if (isset ($_POST['PHPSESSID'])) {
			session_id ($_POST['PHPSESSID']);
		}
		session_start ();
	}

	/**
	 * Mustache
	 *
	 * @static
	 * @return Mustache_Engine
	 */
	public function getMustache ()
	{

		if (self::$m === null) {
			self::$m = new \Mustache_Engine(array(
				'loader' => new \Mustache_Loader_FilesystemLoader(DOMAIN_PATH_TEMPLATE),
				'charset' => 'UTF-8',
				'escape' => function ($value) {return $value;},
				'partials_loader' => new \Mustache_Loader_FilesystemLoader(DOMAIN_PATH_TEMPLATE . '/helper')
			));
		}
		return self::$m;
	}

	/**
	 * ImageStorage
	 *
	 * @return void
	 */
	public function initImageStorage ()
	{
		\Image\Storage::config();
	}

	/**
	 * @return \Doctrine\ORM\EntityManager
	 */
	public static function getEntityManager()
	{
		if (self::$em === null) {
			$conn = array(
				'pdo' => new \PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"))
			);

			$paths = array(
				realpath(__DIR__ . '/../Entity'),
				realpath(__DIR__ . '/../Repository'),
			);

			$debug = true;

			$cache = null;
			if (!$debug) {
				$cache = new FilesystemCache(PATH . 'cache');
			}

			$config = Setup::createAnnotationMetadataConfiguration($paths, $debug, null, $cache);

			self::$em = EntityManager::create($conn, $config);
		}

		return self::$em;
	}

	/**
	 * Init Memcached
	 *
	 * @return void
	 */
	public function initMemcached()
	{
		//Memcached::getInstance()->addServer('localhost', 11211);
	}

}
