<?php

namespace Moda\Common;

class Memcached
{
	/**
	 * @var \Memcached
	 */
	private $memcached;

	/**
	 * @var Memcached
	 */
	private static $instance;

	private function __construct()
	{

	}



	/**
	 * @return Memcached
	 */
	public static function getInstance()
	{
		if (self::$instance === null) {
			self::$instance = new Memcached();
			self::$instance->memcached = new \Memcached();
		}
		return self::$instance;
	}

	public function addServer($host, $port)
	{
		self::$instance->memcached->addServer($host, $port);
	}

	public function get($key)
	{
		return self::$instance->memcached->get($key);
	}

	public function set($key, $value, $expiration = null)
	{
		self::$instance->memcached->set($key, $value, $expiration);
	}

}

