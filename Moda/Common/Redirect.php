<?php
namespace Moda\Common;

class Redirect
{

	/**
	 * Редирект
	 *
	 * @param string $url
	 * @return void
	 */
	public static function redirect ($url)
	{
		header ("HTTP/1.1 301 Moved Permanently");
		header ('location: ' . $url);
	}

	/**
	 * Построить url и сделать редирект на него
	 *
	 * @return void
	 */
	public static function redirectBuildUrl ()
	{
		$args = func_get_args ();

		//быть может указан якорь
		end ($args);
		$add = current ($args);
		if (is_string ($add) && $add[0] === '#') {
			//якорь не будет передан в роутер, а будет добавлен к результату
			unset ($args[key($args)]);
		} else {
			$add = '';
		}

		$url = call_user_func_array (array (\Iir\Front_Controller::getRouter(), 'buildUrl'), $args);

		header ("HTTP/1.1 301 Moved Permanently");
		header ('location: ' . $url . $add);
	}

}
