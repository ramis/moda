<?php
namespace Moda\Common;

class Repositories
{

	/**
	 * @return \Moda\Repository\Brand
	 */
	public static function brand (){
		return Bootstrap::getEntityManager()->getRepository('Moda\Entity\Brand');
	}

	/**
	 * @return \Moda\Repository\Model
	 */
	public static function model (){
		return Bootstrap::getEntityManager()->getRepository('Moda\Entity\Model');
	}

	/**
	 * @return \Moda\Repository\Market
	 */
	public static function market (){
		return Bootstrap::getEntityManager()->getRepository('Moda\Entity\Market');
	}

	/**
	 * @return \Moda\Repository\Designer
	 */
	public static function designer (){
		return Bootstrap::getEntityManager()->getRepository('Moda\Entity\Designer');
	}

	/**
	 * @return \Moda\Repository\View
	 */
	public static function view (){
		return Bootstrap::getEntityManager()->getRepository('Moda\Entity\View');
	}

	/**
	 * @return \Moda\Repository\Type
	 */
	public static function type (){
		return Bootstrap::getEntityManager()->getRepository('Moda\Entity\Type');
	}

	/**
	 * @return \Moda\Repository\Country
	 */
	public static function country (){
		return Bootstrap::getEntityManager()->getRepository('Moda\Entity\Country');
	}

	/**
	 * @return \Moda\Repository\City
	 */
	public static function city (){
		return Bootstrap::getEntityManager()->getRepository('Moda\Entity\City');
	}

	/**
	 * @return \Moda\Repository\Brand\Type
	 */
	public static function brandType (){
		return Bootstrap::getEntityManager()->getRepository('Moda\Entity\Brand\Type');
	}

	/**
	 * @return \Moda\Repository\Queue
	 */
	public static function queue (){
		return Bootstrap::getEntityManager()->getRepository('Moda\Entity\Queue');
	}

	/**
	 * @return \Moda\Repository\Image
	 */
	public static function image (){
		return Bootstrap::getEntityManager()->getRepository('Moda\Entity\Image');
	}

	/**
	 * @return \Moda\Repository\User
	 */
	public static function user (){
		return Bootstrap::getEntityManager()->getRepository('Moda\Entity\User');
	}

	/**
	 * @return \Moda\Repository\Brand\Type\Product
	 */
	public static function product (){
		return Bootstrap::getEntityManager()->getRepository('Moda\Entity\Brand\Type\Product');
	}

}
