<?php
/**
 * Created by PhpStorm.
 * User: Ramis
 * Date: 11.11.13
 * Time: 22:25
 */
namespace Moda\Common;

class YandexLocation
{

	/**
	 * Получение longitude и latitude при помощи maps yandex
	 *
	 * @param string $address
	 * @return array
	 */
	public static function getLocation ($address){

		$params = array(
			'geocode' => $address,
			'format'  => 'json',
			'results' => 1,
			'key'     => '',
		);

		$coordinate = array();
		$query = 'http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params, '', '&');
		$response = json_decode(file_get_contents($query));

		if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0){
			$tmp = explode(' ', $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos);

			$coordinate['latitude'] = $tmp[1];
			$coordinate['longitude'] = $tmp[0];
		}

		return $coordinate;
	}

}