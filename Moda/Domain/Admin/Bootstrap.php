<?php
namespace Moda\Domain\Admin;

class Bootstrap
	extends \Moda\Common\Bootstrap
{

	/**
	 * Файлы настроек для админской части сайта
	 *
	 * @return null
	 */
	public function initConfig ()
	{
		define ('DOMAIN_PATH_TEMPLATE', PATH . 'Moda/Domain/Admin/tpl');

		require PATH . 'Moda/config/' . APPLICATION_SITE . '-config.php';
	}

	/**
	 * Настройки front-controller для админки
	 *
	 * @return null
	 */
	public function initFrontController ()
	{
		//прописываем маршруты
		$router = new \Iir\Router;

		$routes = array (

			'main' =>
				array (\Iir\Router::ROUTE_STATIC, '', 'User', 'Main'),
			'user_loging' =>
				array (\Iir\Router::ROUTE_STATIC, 'user/login', 'User', 'Login'),
			'user_logout' =>
				array (\Iir\Router::ROUTE_STATIC, 'user/logout', 'User', 'Logout'),

			'image' =>
				array (\Iir\Router::ROUTE_STATIC, 'image/add', 'Image', 'Add'),

			//Бренды
			'brands' =>
				array (\Iir\Router::ROUTE_STATIC, 'brands', 'Brand', 'List'),
			'brands_word' =>
				array (\Iir\Router::ROUTE_REGEXP, 'brands/([A-Za-z])', 'Brand', 'BrandsWord', 'brands/%s'),

			'brand_create' =>
				array (\Iir\Router::ROUTE_STATIC, 'brand/create', 'Brand', 'Create', 'brand/create'),
			'brand_edit' =>
				array (\Iir\Router::ROUTE_REGEXP, 'brand/(\d+)/edit', 'Brand', 'Edit', 'brand/%d/edit'),
			'brand_del' =>
				array (\Iir\Router::ROUTE_REGEXP, 'brand/(\d+)/del', 'Brand', 'Remove', 'brand/%d/del'),
			'brand_subs' =>
				array (\Iir\Router::ROUTE_REGEXP, 'brand/(\d+)/subs', 'Brand', 'SubList', 'brand/%d/subs'),
			'brand_sub_create' =>
				array (\Iir\Router::ROUTE_REGEXP, 'brand/(\d+)/sub/create', 'Brand', 'SubCreate', 'brand/%d/sub/create'),
			'brand_sub_edit' =>
				array (\Iir\Router::ROUTE_REGEXP, 'brand/(\d+)/sub/(\d+)/edit', 'Brand', 'SubEdit', 'brand/%d/sub/%d/edit'),

			'brand_types' =>
				array (\Iir\Router::ROUTE_REGEXP, 'brand/(\d+)/types', 'Brand', 'TypeList', 'brand/%d/types'),
			'brand_type_create' =>
				array (\Iir\Router::ROUTE_REGEXP, 'brand/(\d+)/type/create', 'Brand', 'TypeCreate', 'brand/%d/type/create'),
			'brand_type_edit' =>
				array (\Iir\Router::ROUTE_REGEXP, 'brand/(\d+)/type/(\d+)/edit', 'Brand', 'TypeEdit', 'brand/%d/type/%d/edit'),

			'brand_on_word' =>
				array (\Iir\Router::ROUTE_REGEXP, 'brand/word/([A-Za-z])', 'Brand', 'BrandOnWord', 'brand/word/%s'),

			'brand_type_product_list' =>
				array (\Iir\Router::ROUTE_REGEXP, 'brand/type/(\d+)/product/list', 'Product', 'List', 'brand/type/%d/product/list'),
			'product_create' =>
				array (\Iir\Router::ROUTE_REGEXP, 'brand/type/(\d+)/product/create', 'Product', 'Create', 'brand/type/%d/product/create'),
			'product_edit' =>
				array (\Iir\Router::ROUTE_REGEXP, 'brand/type/(\d+)/product/(\d+)/edit', 'Product', 'Edit', 'brand/type/%d/product/%d/edit'),

			//Дизайнеры
			'designers' =>
				array (\Iir\Router::ROUTE_STATIC, 'designers', 'Designer', 'List'),
			'designers_word' =>
				array (\Iir\Router::ROUTE_REGEXP, 'designers/([A-Za-z]+)', 'Designer', 'DesignersWord', 'designers/%s'),
			'designer_create' =>
				array (\Iir\Router::ROUTE_STATIC, 'designer/create', 'Designer', 'Create', 'designer/create'),
			'designer_edit' =>
				array (\Iir\Router::ROUTE_REGEXP, 'designer/(\d+)/edit', 'Designer', 'Edit', 'designer/%d/edit'),

			//Модели
			'models' =>
				array (\Iir\Router::ROUTE_STATIC, 'models', 'Model', 'List'),
			'models_word' =>
				array (\Iir\Router::ROUTE_REGEXP, 'models/([A-Za-z]+)', 'Model', 'ModelsWord', 'models/%s'),
			'model_create' =>
				array (\Iir\Router::ROUTE_STATIC, 'model/create', 'Model', 'Create', 'model/create'),
			'model_edit' =>
				array (\Iir\Router::ROUTE_REGEXP, 'model/(\d+)/edit', 'Model', 'Edit', 'model/%d/edit'),

			//Страны
			'countries' =>
				array (\Iir\Router::ROUTE_STATIC, 'countries', 'Country', 'List'),
			'country_create' =>
				array (\Iir\Router::ROUTE_STATIC, 'country/create', 'Country', 'Create', 'country/create'),
			'country_edit' =>
				array (\Iir\Router::ROUTE_REGEXP, 'country/(\d+)/edit', 'Country', 'Edit', 'country/%d/edit'),

			//Города
			'cities' =>
				array (\Iir\Router::ROUTE_REGEXP, 'country/(\d+)/cities', 'City', 'List', 'country/%d/cities'),
			'city_create' =>
				array (\Iir\Router::ROUTE_REGEXP, 'country/(\d+)/city/create', 'City', 'Create', 'country/%d/city/create'),
			'city_edit' =>
				array (\Iir\Router::ROUTE_REGEXP, 'country/(\d+)/city/(\d+)/edit', 'City', 'Edit', 'country/%d/city/%d/edit'),

			//Магазины
			'markets' =>
				array (\Iir\Router::ROUTE_REGEXP, 'city/(\d+)/markets', 'Market', 'List', 'city/%d/cities'),
			'market_create' =>
				array (\Iir\Router::ROUTE_REGEXP, 'city/(\d+)/market/create', 'Market', 'Create', 'city/%d/market/create'),
			'market_edit' =>
				array (\Iir\Router::ROUTE_REGEXP, 'city/(\d+)/market/(\d+)/edit', 'Market', 'Edit', 'city/%d/market/%d/edit'),
			'market_coordinate' =>
				array (\Iir\Router::ROUTE_STATIC, 'city/market/coordinate', 'Market', 'GetMarketCoordinate'),

			//Вид
			'views' =>
				array (\Iir\Router::ROUTE_STATIC, 'views', 'View', 'List'),
			'view_create' =>
				array (\Iir\Router::ROUTE_STATIC, 'view/create', 'View', 'Create', 'view/create'),
			'view_edit' =>
				array (\Iir\Router::ROUTE_REGEXP, 'view/(\d+)/edit', 'View', 'Edit', 'view/%d/edit'),

			//типы видов
			'view_types' =>
				array (\Iir\Router::ROUTE_REGEXP, 'view/(\d+)/types', 'View', 'TypesList', 'view/%d/types'),
			'view_type_create' =>
				array (\Iir\Router::ROUTE_REGEXP, 'view/(\d+)/type/create', 'View', 'TypeCreate', 'view/%d/type/create'),
			'view_type_edit' =>
				array (\Iir\Router::ROUTE_REGEXP, 'view/(\d+)/type/(\d+)/edit', 'View', 'TypeEdit', 'view/%d/type/%d/edit'),

			//очередь
			'queue_list' =>
				array (\Iir\Router::ROUTE_STATIC, 'queue', 'Queue', 'List'),
			'queue_add' =>
				array (\Iir\Router::ROUTE_STATIC, 'queue/add', 'Queue', 'AddElem'),
			'queue_elem' =>
				array (\Iir\Router::ROUTE_REGEXP, 'queue/(\d+)/elem', 'Queue', 'Elem', 'queue/%d/elem'),

			//фотогалерея
			'photo_list' =>
				array (\Iir\Router::ROUTE_REGEXP, 'photo/([A-Za-z]+)/(\d+)', 'Image', 'PhotoList', 'photo/%s/%d'),
			'photo_delete' =>
				array (\Iir\Router::ROUTE_REGEXP, 'photo/([A-Za-z]+)/(\d+)/delete/(\d+)', 'Image', 'PhotoDeleteImage', 'photo/%s/%d/delete/%d'),

		);

		$router->setRoutes (DOMAIN_HTTP_ADMIN, $routes);

		\Iir\Front_Controller::setRouter ($router);

		//обработка 404 ошибки
//			$plugin2 = new Common_Domains_FCP_404;
//			\Iir\Front_Controller::registerPlugin ($plugin2);


		//префикс контроллеров
		\Iir\Factory_Controller::setPrefix ('Admin');
	}

	/**
	 * Инициализировать обработчик ошибок
	 *
	 * @return void
	 */
	public function initErrorHandler ()
	{

	}
}