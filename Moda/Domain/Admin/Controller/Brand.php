<?php
namespace Moda\Domain\Admin\Controller;

use Moda;
use Moda\Entity;
use Moda\Common;

class Brand extends ControllerAbstract
{

	/**
	 * Список брендов
	 *
	 * @return void
	 */
	public function actionList ()
	{
		$brands = Common\Repositories::brand()->findBy(array('is_popular' => 1), array('title'=>'asc'));

		new Moda\Domain_Admin_View_Brand_List ($brands, '');
	}

	/**
	 * Список cуб брендов
	 *
	 * @param int $brand_id
	 * @return void
	 */
	public function actionSubList ($brand_id)
	{

		$brand = Common\Repositories::brand()->find((int) $brand_id);

		if ($brand === NULL) {
			return;
		}

		new Moda\Domain_Admin_View_Brand_List ($brand->getBrands()->toArray(), '', $brand);
	}

	/**
	 * Создание
	 *
	 * @param Entity\Brand | NULL $parent

	 * @return void
	 */
	public function actionCreate (Entity\Brand $parent = null)
	{

		$brand = new Entity\Brand();

		if ($parent !== null){
			$brand->setParent ($parent);
		}

		if ($this->isRequestPost ()) {

			$this->fillFromForm ($brand);

			if (Common\Repositories::brand()->store ($brand)) {
				//ok
				$url = '/brand/' . $brand->getId() . '/edit/';

				Common\Redirect::redirect ($url);
			}else{
				new Moda\Domain_Admin_View_Brand_CreateEdit($brand, 'create', true);
			}
		} else {
			//форма
			new Moda\Domain_Admin_View_Brand_CreateEdit($brand, 'create');
		}
	}

	/**
	 * Создание
	 *
	 * @param int $parent_id
	 * @return void
	 */
	public function actionSubCreate ($parent_id)
	{
		$parent = Common\Repositories::brand()->find((int) $parent_id);

		if ($parent === NULL) {
			return;
		}
		$this->actionCreate($parent);

	}

	/**
	 * Редактирование
	 *
	 * @param int $parent_id
	 * @param int $brand_id
	 * @return void
	 */
	public function actionSubEdit ($parent_id, $brand_id)
	{
		$parent = Common\Repositories::brand()->find((int) $parent_id);

		if ($parent === NULL) {
			return;
		}
		$this->actionEdit ($brand_id, $parent);

	}

	/**
	 * Редактирование
	 *
	 * @param int $brand_id
	 * @param Entity\Brand | null $parent
	 * @return void
	 */
	public function actionEdit ($brand_id, Entity\Brand $parent = null)
	{
		$brand = Common\Repositories::brand()->find((int) $brand_id);

		if ($brand === NULL) {
			return;
		}

		if ($parent !== null){
			$brand->setParent ($parent);
		}

		if ($this->isRequestPost ()) {
			//действие

			$this->fillFromForm ($brand);

			if (Common\Repositories::brand()->store ($brand)) {
				//ok
				$url = '/brand/' . $brand->getId() . '/edit/';

				Common\Redirect::redirect ($url);
			} else {
				//error
				new Moda\Domain_Admin_View_Brand_CreateEdit ($brand, 'edit', true);
			}
		} else {
			//форма
			new Moda\Domain_Admin_View_Brand_CreateEdit ($brand, 'edit');
		}
	}

	/**
	 * Заполнить объект данными из формы
	 *
	 * @access private
	 * @param Entity\Brand $brand
	 * @return void
	 */
	private function fillFromForm (Entity\Brand $brand)
	{
		$title = trim ($this->getParam ('title'));
		$brand->setTitle ($title);
		$brand->setTitleRus (trim ($this->getParam ('title_rus')));
		$brand->setSite (trim ($this->getParam ('site')));
		$brand->setSiteShop(trim ($this->getParam ('site_shop')));
		$brand->setFounder(trim ($this->getParam ('founder')));
		$brand->setYear((int)$this->getParam ('year'));
		$brand->setUrl (trim ($this->getParam ('url')));
		$brand->setDescription (trim ($this->getParam ('description')));
		$brand->setAnnotation (trim ($this->getParam ('annotation')));
		$brand->setWord (strtolower($title[0]));
		$brand->setMan($this->isParam('is_man'));
		$brand->setWoman($this->isParam('is_woman'));
		$brand->setBaby($this->isParam('is_baby'));
		$brand->setAccessories ($this->isParam('is_accessories'));
		$brand->setPerfume ($this->isParam('is_perfume'));
		$brand->setJewelry ($this->isParam('is_jewelry'));
		$brand->setPopular($this->isParam('is_popular'));

		$country = Common\Repositories::country ()->find((int)$this->getParam ('country'));

		$brand->setCountry($country);

		//Дизайнеры
		if ($this->isParam ('designers')) {
			$brand_designers = array();
			foreach ($this->getParam ('designers') as $id){
				$brand_designers[] = (int)$id;
			}

			foreach ($brand->getDesigners() as $d){
				if(!in_array($d->getId(), $brand_designers)){
					$brand->removeDesigner($d);
				}
			}

			foreach ($brand_designers as $id){
				$designer = Common\Repositories::designer()->find((int)$id);
				if($designer !== null){
					$brand->addDesigner($designer);
				}
			}
		} else {
			foreach ($brand->getDesigners() as $d){
				$brand->removeDesigner($d);
			}
		}

		//Модели
		if ($this->isParam ('models')) {
			$brand_models = array();
			foreach ($this->getParam ('models') as $id){
				$brand_models[] = (int)$id;
			}

			foreach ($brand->getModels () as $d){
				if(!in_array($d->getId(), $brand_models)){
					$brand->removeModel ($d);
				}
			}

			foreach ($brand_models as $id){
				$model = Common\Repositories::model()->find((int)$id);
				if($model !== null){
					$brand->addModel($model);
				}
			}
		} else {
			foreach ($brand->getModels () as $d){
				$brand->removeModel ($d);
			}
		}

	}

	/**
	 * Создание
	 *
	 * @param int $brand_id
	 * @return void
	 */
	public function actionTypeList ($brand_id)
	{
		$brand = Common\Repositories::brand()->find((int) $brand_id);

		if ($brand === NULL) {
			return;
		}

		new Moda\Domain_Admin_View_Brand_Type_List ($brand);
	}

	/**
	 * Создание
	 *
	 * @param int $brand_id
	 * @return void
	 */
	public function actionTypeCreate ($brand_id)
	{
		$brand = Common\Repositories::brand()->find((int) $brand_id);

		if ($brand === NULL) {
			return;
		}

		$brand_type = new Entity\Brand\Type();
		$brand_type->setBrand($brand);

		if ($this->isRequestPost ()) {
			//действие

			$this->fillFromFormBrandType ($brand_type);

			if (Common\Repositories::brandType ()->store ($brand_type)) {
				//ok
				Common\Redirect::redirect ('/brand/' . $brand->getId() . '/types/');
			} else {
				//error
				new Moda\Domain_Admin_View_Brand_Type_CreateEdit ($brand, $brand_type, 'create', true);
			}
		} else {
			//форма
			new Moda\Domain_Admin_View_Brand_Type_CreateEdit ($brand, $brand_type, 'create');
		}
	}

	/**
	 * Редактирование
	 *
	 * @param int $brand_id
	 * @param int $brand_type_id
	 * @return void
	 */
	public function actionTypeEdit ($brand_id, $brand_type_id)
	{
		$brand = Common\Repositories::brand()->find((int) $brand_id);

		if ($brand === NULL) {
			return false;
		}

		$brand_type = Common\Repositories::brandType ()->find((int) $brand_type_id);
		if ($brand_type === NULL) {
			return false;
		}
		$brand_type->setBrand($brand);

		if ($this->isRequestPost ()) {
			//действие

			$this->fillFromFormBrandType ($brand_type);

			if (Common\Repositories::brandType ()->store ($brand_type)) {
				//ok
				Common\Redirect::redirect ('/brand/' . $brand->getId() . '/types/');
			} else {
				//error
				new Moda\Domain_Admin_View_Brand_Type_CreateEdit ($brand, $brand_type, 'edit', true);
			}
		} else {
			//форма
			new Moda\Domain_Admin_View_Brand_Type_CreateEdit ($brand, $brand_type, 'edit');
		}
	}

	/**
	 * Заполнить объект данными из формы
	 *
	 * @access private
	 * @param Entity\Brand\Type $brand_type
	 * @return void
	 */
	private function fillFromFormBrandType (Entity\Brand\Type $brand_type)
	{
		$brand_type->setTitle (trim ($this->getParam ('title')));
		$brand_type->setSex (trim ($this->getParam ('sex')));
		$brand_type->setDescription (trim ($this->getParam ('description')));

		$type = Common\Repositories::type()->find((int)$this->getParam ('type'));

		$brand_type->setType ($type);

	}

	/**
	 * Бренды по 1 букве
	 *
	 * @param string $word
	 * @return void
	 */
	public function actionBrandsWord ($word){
		$brands = Common\Repositories::brand()->findBy(array('word' => $word), array('title'=>'asc'));

		new Moda\Domain_Admin_View_Brand_List ($brands, $word);

	}

	/**
	 * Бренды по 1 букве JSON
	 * @param string $word
	 * @return void
	 */
	public function	actionBrandOnWord ($word){
		$brands_pub = Common\Repositories::brand()->findBy(array('word' => $word), array('title'=>'asc'));

		$brands = array();
		foreach ($brands_pub as $item) {
			$brands[] = array (
				'id' => $item->getId (),
				'title' => $item->getTitle (),
				'alias' => 'brand',
			);
		}

		print json_encode ($brands);
	}

	/**
	 * Удалить бренд
	 *
	 * @param int $brand_id
	 * @return void
	 */
	public function actionRemove ($brand_id){
		$brand = Common\Repositories::brand()->find((int) $brand_id);

		if ($brand === NULL) {
			Common\Redirect::redirect ('/brands/');
			return;
		}

		//Удаляем картинки
		$filter = array('parent' => (string)$brand, 'parent_id' => $brand->getId());
		$images = Common\Repositories::image()->findBy($filter);
		foreach($images as $image)
		{
			Common\Repositories::image()->remove($image);
		}
		Common\Repositories::brand()->remove($brand);

		Common\Redirect::redirect ('/brands/');
	}
}
