<?php
namespace Moda\Domain\Admin\Controller;

use Moda;
use Moda\Entity;
use Moda\Common;

class City extends ControllerAbstract
{

	/**
	 * Список
	 *
	 * @param int $country_id
	 * @return void
	 */
	public function actionList ($country_id)
	{
		$country = Common\Repositories::country()->find((int) $country_id);

		if ($country === NULL) {
			return false;
		}

		new Moda\Domain_Admin_View_City_List ($country);
	}

	/**
	 * Создание
	 *
	 * @param int $country_id
	 * @return void
	 */
	public function actionCreate ($country_id)
	{

		$country = Common\Repositories::country()->find((int) $country_id);

		if ($country === NULL) {
			return false;
		}

		$city = new Entity\City ();
		$city->setCountry ($country);

		if ($this->isRequestPost ()) {

			$this->fillFromForm ($city);

			if (Common\Repositories::city()->store ($city)) {
				//ok
				Common\Redirect::redirect ('/country/' . $country->getId() . '/cities/');
			}else{
				new Moda\Domain_Admin_View_City_CreateEdit ($city, 'create', true);
			}
		} else {
			//форма
			new Moda\Domain_Admin_View_City_CreateEdit ($city, 'create');
		}
	}

	/**
	 * Редактирование
	 *
	 * @param int $country_id
	 * @param int $city_id
	 * @return void
	 */
	public function actionEdit ($country_id, $city_id)
	{
		$country = Common\Repositories::country()->find((int) $country_id);

		if ($country === NULL) {
			return false;
		}

		$city = Common\Repositories::city()->find((int) $city_id);

		if ($city === NULL) {
			return false;
		}
		$city->setCountry ($country);

		if ($this->isRequestPost ()) {
			//действие

			$this->fillFromForm ($city);

			if (Common\Repositories::city()->store ($city)) {
				//ok
				Common\Redirect::redirect ('/country/' . $country->getId() . '/cities/');
			} else {
				//error
				new Moda\Domain_Admin_View_City_CreateEdit ($city, 'edit', true);
			}
		} else {
			//форма
			new Moda\Domain_Admin_View_City_CreateEdit ($city, 'edit');
		}
	}

	/**
	 * Заполнить объект данными из формы
	 *
	 * @access private
	 * @param Entity\City $city
	 * @return void
	 */
	private function fillFromForm (Entity\City $city)
	{
		$city->setTitle (trim ($this->getParam ('title')));

		$prepositional = trim($this->getParam ('title_prepositional')) !== '' ?
			trim($this->getParam ('title_prepositional')) : $city->getTitle();
		$city->setTitlePrepositional ($prepositional);
		$title_genitive = trim($this->getParam ('title_genitive')) !== '' ?
			trim($this->getParam ('title_genitive')) : $city->getTitle();
		$city->setTitleGenitive ($title_genitive);

		$city->setDescription (trim ($this->getParam ('description')));
		$city->setUrl (trim ($this->getParam ('url')));

	}

}
