<?php
namespace Moda\Domain\Admin\Controller;

use Moda;
use Moda\Common;

abstract class ControllerAbstract
	extends \Iir\Controller_Abstract
{

	public function __construct ()
	{

	}

	/**
	 * @access protected
	 * @return View
	 */
	protected function getView ()
	{

	}

	/**
	 * Хук перед стартом работы контроллера
	 *
	 * @param Route_Result $route_result
	 * @return boolean
	 */
	public function preAction (\Iir\Route_Result $route_result)
	{
		$auth = Common\Authorization::getInstance();

		if(!($auth->isAuthorization())){
			Common\Redirect::redirect ('/');
			return false;
		}
		return true;
	}

}
