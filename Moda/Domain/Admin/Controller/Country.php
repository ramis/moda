<?php
namespace Moda\Domain\Admin\Controller;

use Moda;
use Moda\Entity;
use Moda\Common;

class Country
	extends ControllerAbstract
{

	/**
	 * Список
	 *
	 * @return void
	 */
	public function actionList ()
	{
		new Moda\Domain_Admin_View_Country_List();
	}

	/**
	 * Создание
	 *
	 * @return void
	 */
	public function actionCreate ()
	{
		if ($this->isRequestPost ()) {
			//действие
			$country = new Entity\Country();

			$this->fillFromForm ($country);

			if (Common\Repositories::country()->store ($country)) {
				Common\Redirect::redirect ('/countries/');
			} else {
				new Moda\Domain_Admin_View_Country_CreateEdit ($country, 'create', true);
			}
		} else {
			//форма
			new Moda\Domain_Admin_View_Country_CreateEdit (new Entity\Country(), 'create');
		}
	}

	/**
	 * Редактирование
	 *
	 * @param int $country_id
	 * @return void
	 */
	public function actionEdit ($country_id)
	{
		$country = Common\Repositories::country()->find ((int)$country_id);

		if ($country === null) {
			return false;
		}

		if ($this->isRequestPost ()) {
			//действие
			$this->fillFromForm ($country);

			if (Common\Repositories::country()->store ($country)) {
				Common\Redirect::redirect ('/countries/');
			} else {
				//error
				new Moda\Domain_Admin_View_Country_CreateEdit ($country, 'edit', true);
			}
		} else {
			//
			new Moda\Domain_Admin_View_Country_CreateEdit ($country, 'edit');
		}
	}

	/**
	 * Заполнить объект данными из формы
	 *
	 * @access private
	 * @param Entity\Country $country
	 * @return void
	 */
	private function fillFromForm (Entity\Country $country)
	{
		$country->setTitle (trim ($this->getParam ('title')));
		$country->setTitleRus (trim ($this->getParam ('title_rus')));
		$country->setDescription (trim ($this->getParam ('description')));
	}

}
