<?php
namespace Moda\Domain\Admin\Controller;

use Moda;
use Moda\Entity;
use Moda\Common;

class Designer
	extends ControllerAbstract
{

	/**
	 * Список брендов
	 *
	 * @return void
	 */
	public function actionList ()
	{
		$designers = Common\Repositories::designer()->findBy(
			array('is_popular' => 1), array('surname'=>'asc'));

		new Moda\Domain_Admin_View_Designer_List($designers, '');
	}

	/**
	 * Список брендов
	 *
	 * @param string $word
	 * @return void
	 */
	public function actionDesignersWord ($word)
	{
		$designers = Common\Repositories::designer()->findBy(
			array('word' => $word), array('surname'=>'asc'));

		new Moda\Domain_Admin_View_Designer_List($designers, $word);
	}

	/**
	 * Создание
	 *
	 * @return void
	 */
	public function actionCreate ()
	{
		if ($this->isRequestPost ()) {
			//действие
			$designer = new Entity\Designer();

			$this->fillFromForm ($designer);

			if (Common\Repositories::designer()->store ($designer)) {
				Common\Redirect::redirect ('/designers/' . $designer->getWord() . '/');
			} else {
				new Moda\Domain_Admin_View_Designer_CreateEdit ($designer, 'create', true);
			}
		} else {
			//форма
			new Moda\Domain_Admin_View_Designer_CreateEdit (new Entity\Designer(), 'create');
		}
	}

	/**
	 * Редактирование
	 *
	 * @param int $designer_id
	 * @return void
	 */
	public function actionEdit ($designer_id)
	{
		$designer = Common\Repositories::designer()->find ((int)$designer_id);

		if ($designer === null) {
			return false;
		}

		if ($this->isRequestPost ()) {
			//действие
			$this->fillFromForm ($designer);

			if (Common\Repositories::designer()->store ($designer)) {
				Common\Redirect::redirect ('/designers/' . $designer->getWord() . '/');
			} else {
				//error
				new Moda\Domain_Admin_View_Designer_CreateEdit($designer, 'edit', true);
			}
		} else {
			//форма
			new Moda\Domain_Admin_View_Designer_CreateEdit ($designer, 'edit');
		}
	}

	/**
	 * Заполнить объект данными из формы
	 *
	 * @access private
	 * @param Entity\Designer $designer
	 * @return void
	 */
	private function fillFromForm (Entity\Designer $designer)
	{
		$designer->setName (trim ($this->getParam ('name')));
		$surname = trim ($this->getParam ('surname'));
		$designer->setSurname ($surname);
		$designer->setPopular($this->isParam('is_popular'));

		$word = mb_strtolower(mb_substr($surname,0 ,1, 'UTF-8'), 'UTF-8');
		$designer->setWord (Moda\Domain_Admin_View_Abstract::getTranliteRus($word));

		$designer->setDescription (trim ($this->getParam ('description')));
	}

}
