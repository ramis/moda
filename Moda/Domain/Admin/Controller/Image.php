<?php
namespace Moda\Domain\Admin\Controller;

use Moda;
use Moda\Entity;
use Moda\Common;

class Image
	extends ControllerAbstract
{

	/**
	 * Добавить картинку
	 *
	 * @return void
	 */
	public function actionAdd ()
	{

		$parent_id = $this->isParam('parent_id') ? (int)$this->getParam ('parent_id') : 0;
		$parent_type = $this->isParam('parent') ? $this->getParam ('parent') : '';

		$parent = Common\Repositories::$parent_type()->find ((int)$parent_id);
		if($parent === null){
			return;
		}

		if($this->isRequestPost() && $this->isUploadedFileOk('image')){

			$hash = \Image\Storage::setFile($this->getUploadedFilePath('image'));

			$image = new Entity\Image;

			$image->setTitle (trim ($this->getParam('title')));
			$image->setParentId ($parent->getId());
			$image->setParent ((string)$parent);

			$image->setHash($hash);

			$tmp = Common\Repositories::image()->findOneBy(
				array(
					'parent' => $image->getParent(),
					'parent_id' => $image->getParentId()
				), array('sort' => 'desc'));

			$sort = ($tmp !== null ? $tmp->getSort()+1 : 0);
			$image->setMain (false);

			//Главная картинка
			if($this->getParam('fl') && (int)$this->getParam('fl') === 1){
				$sort = 0;
				$tmp = Common\Repositories::image()->findOneBy(
					array(
						'parent' => $image->getParent(),
						'parent_id' => $image->getParentId(),
						'is_main' => 1,
					), array('sort' => 'desc'));
				if($tmp !== null){
					Common\Repositories::image ()->remove($tmp);
				}
				$image->setMain (true);
				$redirect_url = '/'. (string) $parent . '/' . $parent->getId () . '/edit/';
			}

			$image->setSort($sort);

			Common\Repositories::image ()->store ($image);

			Common\Redirect::redirect ($redirect_url);

		} else {
			new Moda\Domain_Admin_View_Image_Add ($parent);
		}

	}

	/**
	 * Фотогалерея
	 *
	 * @param string $parent_class
	 * @param integer $parent_id
	 */
	public function actionPhotoList ($parent_class, $parent_id){

		$parent = Common\Repositories::$parent_class()->find ((int)$parent_id);
		if($parent === null){
			return false;
		}

		new Moda\Domain_Admin_View_Image_Photo_List ($parent);

	}

	/**
	 * Удаление картинки
	 *
	 * @param string $parent_type
	 * @param integer $parent_id
	 * @param integer $image_id
	 * @return void
	 */
	public function actionPhotoDeleteImage ($parent_type, $parent_id, $image_id){

		$redirect_url = '/'. $parent_type . '/' . $parent_id . '/edit/';

		$parent = Common\Repositories::$parent_type()->find ((int)$parent_id);
		if($parent === null){
			Common\Redirect::redirect ($redirect_url);
			return;
		}

		$redirect_url = '/'. (string)$parent . '/' . $parent->getId() . '/edit/';

		$image = Common\Repositories::image()->find ((int)$image_id);
		if($image === null){
			Common\Redirect::redirect ($redirect_url);
			return;
		}
		Common\Repositories::image ()->remove($image);

		Common\Redirect::redirect ($redirect_url);
	}

}