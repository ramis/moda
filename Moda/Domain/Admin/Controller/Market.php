<?php
namespace Moda\Domain\Admin\Controller;

use Moda;
use Moda\Entity;
use Moda\Common;

class Market
	extends ControllerAbstract
{

	/**
	 * Список брендов
	 *
	 * @param integer $city_id
	 * @return void
	 */
	public function actionList ($city_id)
	{
		$city = Common\Repositories::city()->find((int) $city_id);

		if ($city === NULL) {
			return false;
		}

		new Moda\Domain_Admin_View_Market_List ($city);
	}

	/**
	 * Создание
	 *
	 * @param integer $city_id
	 * @return void
	 */
	public function actionCreate ($city_id)
	{
		$city = Common\Repositories::city()->find((int) $city_id);

		if ($city === NULL) {
			return false;
		}
		$market = new Entity\Market();
		$market->setCity($city);

		if ($this->isRequestPost ()) {
			//действие

			$this->fillFromForm ($market);

			if (Common\Repositories::market()->store ($market)) {
				Common\Redirect::redirect ('/city/'.$city->getId().'/markets/');
			} else {
				new Moda\Domain_Admin_View_Market_CreateEdit ($market, 'create', true);
			}
		} else {
			//форма
			new Moda\Domain_Admin_View_Market_CreateEdit ($market, 'create');
		}
	}

	/**
	 * Редактирование
	 *
	 * @param integer $city_id
	 * @param int $market_id
	 * @return void
	 */
	public function actionEdit ($city_id, $market_id)
	{
		$city = Common\Repositories::city()->find((int) $city_id);

		if ($city === NULL) {
			return false;
		}

		$market = Common\Repositories::market()->find ((int)$market_id);

		if ($market === null) {
			return false;
		}

		$market->setCity($city);
		if ($this->isRequestPost ()) {
			//действие
			$this->fillFromForm ($market);

			if (Common\Repositories::market()->store ($market)) {
				Common\Redirect::redirect ('/city/'.$city->getId().'/markets/');
			} else {
				//error
				new Moda\Domain_Admin_View_Market_CreateEdit ($market, 'edit', true);
			}
		} else {
			//форма
			new Moda\Domain_Admin_View_Market_CreateEdit ($market, 'edit');
		}
	}

	/**
	 * Заполнить объект данными из формы
	 *
	 * @access private
	 * @param Entity\Market $market
	 * @return void
	 */
	private function fillFromForm (Entity\Market $market)
	{
		$market->setTitle (trim ($this->getParam ('title')));
		$market->setAddress (trim ($this->getParam ('address')));
		$market->setPhone (trim ($this->getParam ('phone')));
		$market->setSite (trim ($this->getParam ('site')));
		$market->setLatitude ($this->getParam ('latitude'));
		$market->setLongitude($this->getParam ('longitude'));
		$market->setDescription (trim ($this->getParam ('description')));


		//Бренды
		if ($this->isParam ('brand')) {
			$brands = array();
			foreach ($this->getParam ('brand') as $id){
				$brands[] = (int)$id;
			}

			foreach ($market->getBrands () as $b){
				if(!in_array($b->getId(), $brands)){
					$market->removeBrand($b);
				}
			}

			foreach ($brands as $id){
				$brand = Common\Repositories::brand()->find((int)$id);
				if($brand !== null){
					$market->addBrand($brand);
				}
			}
		} else {
			foreach ($market->getBrands() as $b){
				$market->removeBrand($b);
			}
		}

	}

	/**
	 * Получение координат магазина по адресу JSON
	 */
	public function actionGetMarketCoordinate (){

		if ($this->isParam ('address')) {
			print json_encode(Common\YandexLocation::getLocation($this->getParam('address')));
		}else{
			print json_encode(array());
		}
	}

}
