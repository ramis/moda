<?php
namespace Moda\Domain\Admin\Controller;

use Moda;
use Moda\Entity;
use Moda\Common;

class Model
	extends ControllerAbstract
{

	/**
	 * Список моделей
	 *
	 * @return void
	 */
	public function actionList ()
	{
		$models = Common\Repositories::model()->findBy(array('is_popular' => 1), array('surname'=>'desc'));
		new Moda\Domain_Admin_View_Model_List($models, '');
	}

	/**
	 * Список моделей по 1 букве
	 *
	 * @param string $word
	 * @return void
	 */
	public function actionModelsWord ($word)
	{
		$models = Common\Repositories::model()->findBy(array('word' => $word), array('surname'=>'desc'));
		new Moda\Domain_Admin_View_Model_List($models, $word);
	}

	/**
	 * Создание
	 *
	 * @return void
	 */
	public function actionCreate ()
	{
		if ($this->isRequestPost ()) {
			//действие
			$model = new Entity\Model();

			$this->fillFromForm ($model);

			if (Common\Repositories::model()->store ($model)) {
				Common\Redirect::redirect ('/models/');
			} else {
				new Moda\Domain_Admin_View_Model_CreateEdit ($model, 'create', true);
			}
		} else {
			//форма
			new Moda\Domain_Admin_View_Model_CreateEdit (new Entity\Model(), 'create');
		}
	}

	/**
	 * Редактирование
	 *
	 * @param int $model_id
	 * @return void
	 */
	public function actionEdit ($model_id)
	{
		$model = Common\Repositories::model()->find ((int)$model_id);

		if ($model === null) {
			return false;
		}

		if ($this->isRequestPost ()) {
			//действие
			$this->fillFromForm ($model);

			if (Common\Repositories::model()->store ($model)) {
				Common\Redirect::redirect ('/models/');
			} else {
				//error
				new Moda\Domain_Admin_View_Model_CreateEdit ($model, 'edit', true);
			}
		} else {
			//форма
			new Moda\Domain_Admin_View_Model_CreateEdit ($model, 'edit');
		}
	}

	/**
	 * Заполнить объект данными из формы
	 *
	 * @access private
	 * @param Entity\Model $model
	 * @return void
	 */
	private function fillFromForm (Entity\Model $model)
	{
		$model->setName (trim ($this->getParam ('name')));
		$surname = trim ($this->getParam ('surname'));
		$model->setSurname ($surname);
		$model->setPopular($this->isParam('is_popular'));

		$alpfavit = array ( 'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e',
			'ё' => 'jo', 'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'jj', 'к' => 'k',
			'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r',
			'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'kh', 'ч' => 'c',
			'ц' => 'ch', 'ш' => 'sh', 'щ' => 'shh', 'ы' => 'y', 'э' => 'eh', 'ю' => 'ju', 'я' => 'ja');
		$word = mb_strtolower(mb_substr($surname,0 ,1, 'UTF-8'), 'UTF-8');

		$model->setWord ($alpfavit[$word]);
		$model->setDescription (trim ($this->getParam ('description')));
	}

}
