<?php
namespace Moda\Domain\Admin\Controller;

use Moda;
use Moda\Entity;
use Moda\Entity\Brand\Type;
use Moda\Common;

class Product extends ControllerAbstract
{

	/**
	 * Список брендов
	 *
	 * @param int $brand_type_id
	 * @return void
	 */
	public function actionList ($brand_type_id)
	{
		$brand_typed = Common\Repositories::brandType()->find((int) $brand_type_id);

		if ($brand_typed === NULL) {
			return false;
		}

		new Moda\Domain_Admin_View_Product_List ($brand_typed);
	}


	/**
	 * Создание
	 *
	 * @param int $brand_type_id
	 * @return void
	 */
	public function actionCreate ($brand_type_id)
	{

		$product = new Entity\Brand\Type\Product();

		$brand_type = Common\Repositories::brandType()->find((int) $brand_type_id);

		if ($brand_type === NULL) {
			return false;
		}

		$product->setBrandType($brand_type);

		if ($this->isRequestPost ()) {

			$this->fillFromForm ($product);

			if (Common\Repositories::product()->store ($product)) {
				//ok
				Common\Redirect::redirect ('/brand/type/' . $brand_type->getId() . '/product/list/');
			}else{
				new Moda\Domain_Admin_View_Product_CreateEdit ($brand_type, $product, 'create', true);
			}
		} else {
			//форма
			new Moda\Domain_Admin_View_Product_CreateEdit ($brand_type, $product, 'create');
		}
	}

	/**
	 * Редактирование
	 *
	 * @param int $brand_type_id
	 * @param int $product_id
	 * @return void
	 */
	public function actionEdit ($brand_type_id, $product_id)
	{
		$product = Common\Repositories::product()->find((int) $product_id);
		$brand_type = Common\Repositories::brandType()->find((int) $brand_type_id);

		if ($brand_type === NULL) {
			return false;
		}
		if ($product === NULL) {
			return false;
		}

		$product->setBrandType($brand_type);

		if ($this->isRequestPost ()) {
			//действие

			$this->fillFromForm ($product);

			if (Common\Repositories::product()->store ($product)) {
				//ok
				Common\Redirect::redirect ('/brand/type/' . $brand_type->getId() . '/product/list/');
			} else {
				//error
				new Moda\Domain_Admin_View_Product_CreateEdit ($brand_type, $product, 'edit', true);
			}
		} else {
			//форма
			new Moda\Domain_Admin_View_Product_CreateEdit ($brand_type, $product, 'edit');
		}
	}

	/**
	 * Заполнить объект данными из формы
	 *
	 * @access private
	 * @param Type\Product $product
	 * @return void
	 */
	private function fillFromForm (Type\Product $product)
	{
		$product->setTitle (trim ($this->getParam ('title')));
		$product->setPrice ((int) $this->getParam ('price'));
		$product->setSale ((int) $this->getParam ('sale'));;
		$product->setDescription (trim ($this->getParam ('description')));
	}

}
