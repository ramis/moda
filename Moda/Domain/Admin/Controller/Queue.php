<?php
namespace Moda\Domain\Admin\Controller;

use Moda;
use Moda\Entity;
use Moda\Common;

class Queue
	extends ControllerAbstract
{

	/**
	 * Список
	 *
	 * @return void
	 */
	public function actionList ()
	{
		new Moda\Domain_Admin_View_Queue_List ();
	}

	/**
	 * Элемент очереди
	 *
	 * @param int $queue_id
	 * @return void
	 */
	public function actionElem ($queue_id)
	{
		$queue = Common\Repositories::queue()->find ((int)$queue_id);

		if ($queue === null) {
			return false;
		}
		new Moda\Domain_Admin_View_Queue_Elem ($queue);

	}

	/**
	 * Добавить элемент в очередь
	 *
	 * @return void
	 */
	public function actionAddElem ()
	{

		if($this->isRequestPost() && $this->isUploadedFileOk('excel')){

			$hash = \Image\Storage::setFile($this->getUploadedFilePath('excel'), 'xls');

			$queue = new Entity\Queue();

			$queue->setTitle(trim($this->getParam('title')));
			$queue->setCreateTime(date('Y-m-d H:i:s'));
			$queue->setProcessed(false);
			$queue->setError(false);

			$queue->setHash($hash);
			$queue->setMessage('');
			$queue->setWorker(trim ($this->getParam('worker')));

			Common\Repositories::queue ()->store ($queue);

			Common\Redirect::redirect ('/queue/');
		}

		new Moda\Domain_Admin_View_Queue_Add ();

	}

}
