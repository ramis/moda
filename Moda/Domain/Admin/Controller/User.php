<?php
namespace Moda\Domain\Admin\Controller;

use Moda;
use Moda\Entity;
use Moda\Common;

class User extends \Iir\Controller_Abstract
{

	/**
	 * @return void
	 */
	public function actionMain ()
	{
		$auth = Common\Authorization::getInstance();

		if($auth->isAuthorization()){
			Common\Redirect::redirect ('/brands/');
			return;
		}
		new Moda\Domain_Admin_View_User_Login ();
	}

	/**
	 * @return void
	 */
	public function actionLogin ()
	{

		if ($this->isRequestPost ()) {
			$nick = $this->isParam('nick') ? $this->getParam('nick') : '';
			$passwd = $this->isParam('passwd') ? md5($this->getParam('passwd')) : '';

			$auth = Moda\Common\Authorization::getInstance();
			if($auth->login($nick, $passwd)){
				Common\Redirect::redirect ('/brands/');
			} else{
				Common\Redirect::redirect ('/');
			}
		}else{
			new Moda\Domain_Admin_View_User_Login ();
		}

	}

	/**
	 * @return void
	 */
	public function actionLogout ()
	{
		$auth = Moda\Common\Authorization::getInstance();
		$auth::logout();
		Common\Redirect::redirect ('/');
	}

}