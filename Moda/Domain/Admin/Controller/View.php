<?php
namespace Moda\Domain\Admin\Controller;

use Moda;
use Moda\Entity;
use Moda\Common;

class View
	extends ControllerAbstract
{

	/**
	 * Список
	 *
	 * @return void
	 */
	public function actionList ()
	{
		new Moda\Domain_Admin_View_View_List();
	}

	/**
	 * Создание
	 *
	 * @return void
	 */
	public function actionCreate ()
	{
		if ($this->isRequestPost ()) {
			//действие
			$view = new Entity\View();

			$this->fillFromForm ($view);

			if (Common\Repositories::view()->store ($view)) {
				Common\Redirect::redirect ('/views/');
			} else {
				new Moda\Domain_Admin_View_View_CreateEdit ($view, 'create', true);
			}
		} else {
			//форма
			new Moda\Domain_Admin_View_View_CreateEdit (new Entity\View(), 'create');
		}
	}

	/**
	 * Редактирование
	 *
	 * @param int $view_id
	 * @return void
	 */
	public function actionEdit ($view_id)
	{
		$view = Common\Repositories::view()->find ((int)$view_id);

		if ($view === null) {
			return false;
		}

		if ($this->isRequestPost ()) {
			//действие
			$this->fillFromForm ($view);

			if (Common\Repositories::view()->store ($view)) {
				Common\Redirect::redirect ('/views/');
			} else {
				//error
				new Moda\Domain_Admin_View_View_CreateEdit ($view, 'edit', true);
			}
		} else {
			//форма
			new Moda\Domain_Admin_View_View_CreateEdit ($view, 'edit');
		}
	}

	/**
	 * Заполнить объект данными из формы
	 *
	 * @access private
	 * @param Entity\View $view
	 * @return void
	 */
	private function fillFromForm (Entity\View $view)
	{
		$view->setTitle (trim ($this->getParam ('title')));
		$view->setDescription (trim ($this->getParam ('description')));
	}

	/**
	 * Список
	 *
	 * @param integer $view_id
	 * @return void
	 */
	public function actionTypesList ($view_id)
	{
		$view = Common\Repositories::view()->find ((int)$view_id);

		if ($view === null) {
			return false;
		}

		new Moda\Domain_Admin_View_Type_List($view);
	}

	/**
	 * Создание
	 *
	 * @param integer $view_id
	 * @return void
	 */
	public function actionTypeCreate ($view_id)
	{
		$view = Common\Repositories::view()->find ((int)$view_id);

		if ($view === null) {
			return false;
		}

		if ($this->isRequestPost ()) {
			//действие
			$type = new Entity\Type();
			$type->setView ($view);
			$this->fillTypeFromForm ($type);

			if (Common\Repositories::type()->store ($type)) {
				Common\Redirect::redirect ('/view/' . $view->getId() . '/types/');
			} else {
				new Moda\Domain_Admin_View_Type_CreateEdit ($view, $type, 'create', true);
			}
		} else {
			//форма
			$type = new Entity\Type();
			$type->setView ($view);

			new Moda\Domain_Admin_View_Type_CreateEdit ($view, $type, 'create');
		}
	}

	/**
	 * Редактирование
	 *
	 * @param integer $view_id
	 * @param integer $type_id
	 * @return void
	 */
	public function actionTypeEdit ($view_id, $type_id){

		$view = Common\Repositories::view ()->find ((int)$view_id);
		if ($view === null) {
			return false;
		}

		$type = Common\Repositories::type ()->find ((int)$type_id);
		if ($type === null) {
			return false;
		}

		if ($this->isRequestPost ()) {
			//действие
			$this->fillTypeFromForm ($type);
			$type->setView($view);
			if (Common\Repositories::type ()->store ($type)) {
				Common\Redirect::redirect ('/view/' . $view->getId() . '/types/');
			} else {
				//error
				new Moda\Domain_Admin_View_Type_CreateEdit ($view, $type, 'edit', true);
			}
		} else {
			//форма
			new Moda\Domain_Admin_View_Type_CreateEdit ($view, $type, 'edit');
		}
	}

	/**
	 * Заполнить объект данными из формы
	 *
	 * @access private
	 * @param Entity\Type $type
	 * @return void
	 */
	private function fillTypeFromForm (Entity\Type $type)
	{
		$type->setTitle (trim ($this->getParam ('title')));
		$type->setDescription (trim ($this->getParam ('description')));
	}

}
