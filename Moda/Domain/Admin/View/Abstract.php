<?php
namespace Moda;

class Domain_Admin_View_Abstract
{

	static $alpfavit = array (
		'a' => 'а','b' => 'б','v' => 'в','g' => 'г','d' => 'д','e' => 'е','jo' => 'ё','zh' => 'ж','z' => 'з',
		'i' => 'и','jj' => 'й','k' => 'к','l' => 'л','m' => 'м','n' => 'н','o' => 'о','p' => 'п','r' => 'р',
		's' => 'с','t' => 'т','u' => 'у','f' => 'ф','kh' => 'х','c' => 'ч','ch' => 'ц','sh' => 'ш','shh' => 'щ',
		'eh' => 'э','ju' => 'ю','ja' => 'я'
	);

	public static function common ()
	{
		return Common\Bootstrap::getMustache ();
	}

	/**
	 * Русский алфавит
	 *
	 * @return array
	 */
	protected static function getRusAlpfavit (){

		$words = array();
		foreach(self::$alpfavit as $key=>$val){
			$words[] = array(
				'up' => strtoupper ($val),
				'url' => $key
			);
		}
		return $words;
	}

	/**
	 * Русская буква по английской
	 *
	 * @param string $word
	 * @return string
	 */
	protected static function getRusWord ($word){
		return self::$alpfavit[$word];
	}

	/**
	 * Траслит
	 *
	 * @param string $word
	 * @return string
	 */
	public static function getTranliteRus ($word){
		foreach(self::$alpfavit as $key=>$val){
			if($val === $word){
				return $key;
			}
		}
	}
}