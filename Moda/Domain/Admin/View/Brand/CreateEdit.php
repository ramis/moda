<?php

namespace Moda;

use Moda\Entity;

class Domain_Admin_View_Brand_CreateEdit
	extends Domain_Admin_View_Abstract
{

	public function __construct (Entity\Brand $brand, $type, $error = false)
	{
		$mustache = parent::common();

		$array_mustache = array (
			'brand_id' => $brand->getId (),
			'id' => $brand->getId (),
			'url' => $brand->getUrl (),
			'legend' => ($brand->getTitle () ? $brand->getTitle () : 'Новый бренд'),
			'title' => $brand->getTitle (),
			'title_rus' => $brand->getTitleRus (),
			'year' => $brand->getYear (),
			'site' => $brand->getSite (),
			'site_shop' => $brand->getSiteShop(),
			'founder' => $brand->getFounder(),
			'description' => $brand->getDescription (),
			'annotation' => $brand->getAnnotation (),
			'class_name' => (string)$brand,
			'man_checked' => ($brand->isMan() ? 'checked' : ''),
			'woman_checked' => ($brand->isWoman() ? 'checked' : ''),
			'baby_checked' => ($brand->isBaby() ? 'checked' : ''),
			'accessories_checked' => ($brand->isAccessories() ? 'checked' : ''),
			'perfume_checked' => ($brand->isPerfume() ? 'checked' : ''),
			'jewelry_checked' => ($brand->isJewelry() ? 'checked' : ''),
			'popular_checked' => ($brand->isPopular() ? 'checked' : ''),
		);

		$action = ($type === 'create' ? '/brand/create/' : '/brand/'. $brand->getId () .'/edit/');

		$parent = $brand->getParent();
		if($parent instanceof Entity\Brand){
			$array_mustache ['parent_id'] = $parent->getId ();
			$array_mustache ['parent_title'] = $parent->getTitle ();
			$action = ($type === 'create' ?
				'/brand/' . $parent->getId() . '/sub/create/' :
				'/brand/' . $parent->getId() . '/sub/' . $brand->getId () .'/edit/');
		}

		$array_mustache['action'] = $action;

		$array_mustache['brand_designers'] = array();
		foreach($brand->getDesigners() as $d){
			$array_mustache['brand_designers'][] = $d->getId();
		}

		$array_mustache['designers'] = array();
		foreach(Common\Repositories::designer()->findBy(array(), array('name'=>'asc')) as $d){
			$array_mustache['designers'][] = array(
				'id' => $d->getId(),
				'name' => $d->getName(),
				'checked' => (in_array($d->getId(), $array_mustache['brand_designers']) ? 'checked' : ''),
			);
		}

		$array_mustache['brand_models'] = array();
		foreach($brand->getModels () as $d){
			$array_mustache['brand_models'][] = $d->getId();
		}

		$array_mustache['models'] = array();
		foreach(Common\Repositories::model()->findBy(array(), array('name'=>'asc')) as $d){
			$array_mustache['models'][] = array(
				'id' => $d->getId(),
				'name' => $d->getName(),
				'checked' => (in_array($d->getId(), $array_mustache['brand_models']) ? 'checked' : ''),
			);
		}

		$array_mustache['countries'] = array();
		foreach(Common\Repositories::country()->findBy(array(), array('title'=>'asc')) as $c){

			$selected = false;

			if($brand->getCountry() instanceof Entity\Country) {
				$selected = $brand->getCountry()->getId() === $c->getId() ? 'selected' : '';
			}

			$array_mustache['countries'][] = array(
				'id' => $c->getId(),
				'title' => $c->getTitle(),
				'selected' => $selected,
			);
		}
		$array_mustache['brand_menu'] = true;

		foreach(Common\Repositories::image()->findBy(
			array('parent' => (string)$brand, 'parent_id' => $brand->getId()), array('sort'=>'asc')) as $i)
		{
			$array_mustache['images'][] = array(
				'id' => $i->getId (),
				'hash' => HTTP_IMAGE_P_50x50 . $i->getHash (),
			);
		}

		$array_mustache['words'] = array();
		for ($i = 0; $i< 26; $i++){
			$w = chr(97+$i);
			$array_mustache['words'][] = array(
				'up' => strtoupper ($w),
				'url' =>  '/brands/' . $w . '/',
			);
		}

		echo $mustache->render('brand/create_edit', $array_mustache);

	}

}