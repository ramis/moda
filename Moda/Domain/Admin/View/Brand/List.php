<?php
/**
 * Список брендов
 */
namespace Moda;

class Domain_Admin_View_Brand_List
	extends Domain_Admin_View_Abstract
{

	public function __construct (array $brands_published, $word, Entity\Brand $parent = null)
	{

		$mustache = parent::common();

		$array_mustache['brands'] = array ();
		foreach ($brands_published as $item) {
			$array_mustache['brands'][] = array (
				'id' => $item->getId (),
				'title' => $item->getTitle (),
			);
		}

		$array_mustache['is_sub'] = $parent === null;

		if($parent !== null){
			$array_mustache['brand_id'] = $parent->getId();
			$array_mustache['brand_title'] = $parent->getTitle();
		}else{
			$array_mustache['words'] = array();
			for ($i = 0; $i< 26; $i++){
				$w = chr(97+$i);
				$array_mustache['words'][] = array(
					'up' => strtoupper ($w),
					'url' => ($word === $w ? '' : '/brands/' . $w . '/'),
				);
			}
		}
		$array_mustache['brand_menu'] = true;

		echo $mustache->render('brand/list', $array_mustache);

	}

}