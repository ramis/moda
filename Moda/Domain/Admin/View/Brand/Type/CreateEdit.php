<?php

namespace Moda;

use Moda\Entity;

class Domain_Admin_View_Brand_Type_CreateEdit
	extends Domain_Admin_View_Abstract
{

	public function __construct (Entity\Brand $brand, Entity\Brand\Type $brand_type, $type, $error = false)
	{
		$mustache = parent::common();

		$array_mustache = array (
			'id' => $brand_type->getId (),
			'legend' => ($brand_type->getTitle () ? $brand_type->getTitle () : 'Новый тип'),
			'title' => $brand_type->getTitle (),
			'description' => $brand_type->getDescription (),
			'brand_title' => $brand->getTitle (),
			'brand_id' => $brand->getId (),
		);

		$action = ($type === 'create' ? '/brand/'. $brand->getId () .'/type/create/' :
			'/brand/'. $brand->getId () .'/type/'. $brand_type->getId () .'/edit/');

		$array_mustache['action'] = $action;

		$array_mustache['types'] = array();
		foreach(Common\Repositories::view()->findBy(array(), array('title'=>'asc')) as $v){

			$view = array(
				'id' => $v->getId (),
				'title' => $v->getTitle (),
				'types' => array(),
			);

			foreach(Common\Repositories::type ()->findBy(array('view' => $v), array('title'=>'asc')) as $t){

				$selected = false;
				if($brand_type->getType() instanceof Entity\Brand\Type) {
					$selected = $brand_type->getType()->getId() === $t->getId() ? 'selected' : '';
				}

				$view['types'][] = array(
					'id' => $t->getId (),
					'title' => $t->getTitle (),
					'selected' => $selected,
				);
			}

			$array_mustache['views'][] = $view;
		}


		$array_mustache['brand_menu'] = true;


		echo $mustache->render('brand/type/create_edit', $array_mustache);

	}

}