<?php
/**
 * Список
 */
namespace Moda;

class Domain_Admin_View_Brand_Type_List
	extends Domain_Admin_View_Abstract
{

	public function __construct (Entity\Brand $brand)
	{

		$mustache = parent::common();

		$array_mustache = array ();
		$views = array();

		foreach (Common\Repositories::brandType()->findBy(array('brand' => $brand)) as $item) {
			$view = $item->getType ()->getView ();

			$brand_type  = (isset($array_mustache[$view->getId()])) ? $array_mustache[$view->getId()]['brand_type'] : array();

			$brand_type[] = array(
				'id' => $item->getId (),
				'title' => $item->getTitle (),
				'title_type' => $item->getType ()->getTitle(),
				'sex' => $item->getSex () === Entity\Brand\Type::SEX_MALE ? 'Мужской' : 'Женский',
			);
			$views[$view->getId ()] = array (
				'id' => $view->getId (),
				'title' => $view->getTitle (),
				'brand_type' => $brand_type,
			);
		}

		$array_mustache['views'] = new \ArrayIterator( $views );

		$array_mustache['brand_id'] = $brand->getId();
		$array_mustache['brand_title'] = $brand->getTitle();

		$array_mustache['brand_menu'] = true;

		echo $mustache->render('brand/type/list', $array_mustache);

	}

}