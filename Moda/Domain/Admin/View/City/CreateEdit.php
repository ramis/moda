<?php

namespace Moda;

use Moda\Entity;

class Domain_Admin_View_City_CreateEdit
	extends Domain_Admin_View_Abstract
{

	public function __construct (Entity\City $city, $type, $error = false)
	{
		$mustache = parent::common();
		$country = $city->getCountry();
		$url = ($type === 'create' ? '/country/' . $country->getId() . '/city/create/' :
			'/country/' . $country->getId() . '/city/'. $city->getId () .'/edit/');

		echo $mustache->render('city/create_edit', array (
			'id' => $city->getId (),
			'legend' => ($city->getTitle () ? $city->getTitle () : 'Новый город'),
			'title' => $city->getTitle (),
			'title_prepositional' => $city->getTitlePrepositional(),
			'title_genitive' => $city->getTitleGenitive (),
			'url' => $city->getUrl (),
			'description' => $city->getDescription (),
			'country_id' => $country->getId (),
			'country_title' => $country->getTitle (),
			'city_menu' => true,
			'action' => $url,
		));

	}

}