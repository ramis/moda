<?php
/**
 * Список
 */
namespace Moda;

class Domain_Admin_View_City_List
	extends Domain_Admin_View_Abstract
{

	public function __construct (Entity\Country $country)
	{

		$mustache = parent::common();

		$array_mustache = array();
		$array_mustache['country_id'] = $country->getId();
		$array_mustache['country_title'] = $country->getTitle();

		$market_published = Common\Repositories::city ()->findBy(array('country' => $country), array('title'=>'asc'));
		$cities = array ();
		foreach ($market_published as $item) {
			$word = mb_substr ($item->getTitle (), 0, 1, 'UTF-8');

			$tmp_cities = isset ($cities[$word]) ? $cities[$word]['cities'] : array();
			$tmp_cities[] = array (
				'id' => $item->getId (),
				'title' => $item->getTitle (),
			);

			$cities[$word]['cities'] = $tmp_cities;
			$cities[$word]['word'] = $word;
		}

		$array_mustache['word_cities'] = new \ArrayIterator( $cities );

		echo $mustache->render('city/list', $array_mustache);
	}

}