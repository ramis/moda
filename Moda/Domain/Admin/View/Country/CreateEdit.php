<?php
/**
 * Редактирование
 */
namespace Moda;

class Domain_Admin_View_Country_CreateEdit
	extends Domain_Admin_View_Abstract
{

	public function __construct (Entity\Country $country, $type, $error = false)
	{
		$mustache = parent::common();

		echo $mustache->render('country/create_edit',  array (
			'id' => $country->getId (),
			'legend' => ($country->getTitle () ? $country->getTitle () : 'Новая страна'),
			'title' => $country->getTitle (),
			'title_rus' => $country->getTitleRus (),
			'description' => $country->getDescription (),
			'action' => ($type === 'create' ? '/country/create/' : '/country/'. $country->getId () .'/edit/'),
			'country_menu' => true,
		));
	}

}