<?php
/**
 * Список
 */
namespace Moda;

class Domain_Admin_View_Country_List
	extends Domain_Admin_View_Abstract
{

	public function __construct ()
	{

		$mustache = parent::common();

		$array_mustache['countries'] = array ();
		foreach (Common\Repositories::country()->findBy(array(), array('title'=>'ASC')) as $item) {
			$array_mustache['countries'][] = array (
				'id' => $item->getId (),
				'title' => $item->getTitle (),
			);
		}

		$array_mustache['country_menu'] = true;

		echo $mustache->render('country/list', $array_mustache);
	}

}