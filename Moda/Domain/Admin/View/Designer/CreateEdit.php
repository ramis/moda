<?php
/**
 * Редактирование дизайнеров
 */
namespace Moda;

class Domain_Admin_View_Designer_CreateEdit
	extends Domain_Admin_View_Abstract
{

	public function __construct (Entity\Designer $designer, $type)
	{
		$mustache = parent::common();

		$array_mustache =  array (
			'id' => $designer->getId (),
			'legend' => ($designer->getName () ? $designer->getName () . ' ' .
					$designer->getSurname() : 'Новый дизайнер'),
			'name' => $designer->getName (),
			'surname' => $designer->getSurname (),
			'description' => $designer->getDescription (),
			'class_name' => (string)$designer,
			'popular_checked' => ($designer->isPopular() ? 'checked' : ''),
			'action' => ($type === 'create' ? '/designer/create/' : '/designer/'. $designer->getId () .'/edit/'),
			'designer_menu' => true,
		);

		$images = Common\Repositories::image()->findBy(
			array('parent' => (string)$designer, 'parent_id' => $designer->getId()), array('sort'=>'asc'));
		foreach($images as $i)
		{
			$array_mustache['images'][] = array(
				'id' => $i->getId (),
				'hash' => HTTP_IMAGE_P_50x50 . $i->getHash (),
			);
		}

		echo $mustache->render('designer/create_edit', $array_mustache);

	}

}