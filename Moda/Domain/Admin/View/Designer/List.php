<?php
/**
 * Список дизайнеров
 */
namespace Moda;

class Domain_Admin_View_Designer_List
	extends Domain_Admin_View_Abstract
{

	public function __construct (array $designers_published, $word)
	{

		$mustache = parent::common();

		$array_mustache['designers'] = array ();
		foreach ($designers_published as $item) {
			$array_mustache['designers'][] = array (
				'id' => $item->getId (),
				'name' => $item->getName () . ' ' . $item->getSurname (),
			);
		}

		if ($word !== ''){
			$array_mustache['title'] = parent::getRusWord($word);
		}else{
			$array_mustache['words'] = parent::getRusAlpfavit();
		}
		$array_mustache['designer_menu'] = true;

		echo $mustache->render('designer/list', $array_mustache);
	}

}