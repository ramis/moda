<?php
namespace Moda;

class Domain_Admin_View_Image_Add
	extends Domain_Admin_View_Abstract
{

	public function __construct ($parent)
	{

		$mustache = parent::common();

		echo $mustache->render('image/add', array('parent_id' => $parent->getId(), 'parent' => (string)$parent));
	}

}