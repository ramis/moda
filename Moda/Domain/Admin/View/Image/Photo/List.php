<?php
namespace Moda;


class Domain_Admin_View_Image_Photo_List
	extends Domain_Admin_View_Abstract
{

	public function __construct ($parent)
	{

		$mustache = parent::common();
		$array_mustache = array(
			'parent' => (string)$parent,
			'parent_id' => $parent->getId(),
			'title' => ($parent instanceof Entity\Brand ? $parent->getTitle() :
						$parent->getName () . ' ' . $parent->getSurname()),
		);

		foreach(Common\Repositories::image()->findBy(array(
			'parent' => (string)$parent,
			'parent_id' => $parent->getId(),
			'is_gallery' => 1), array('sort'=>'asc')) as $i)
		{
			$array_mustache['images'][] = array(
				'id' => $i->getId (),
				'hash' => HTTP_IMAGE_P_200x200 . $i->getHash (),
			);
		}

		echo $mustache->render('image/photo/list', $array_mustache);
	}

}