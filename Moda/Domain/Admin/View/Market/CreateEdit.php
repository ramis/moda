<?php

namespace Moda;

use Moda\Entity;

class Domain_Admin_View_Market_CreateEdit
	extends Domain_Admin_View_Abstract
{

	public function __construct (Entity\Market $market, $type, $error = false)
	{
		$mustache = parent::common ();
		$city = $market->getCity ();
		$country = $city->getCountry ();

		$words = array();
		for ($i = 0; $i< 26; $i++){
			$w = chr(97+$i);
			$words[] = array ('word' => strtoupper ($w));
		}

		$url = ($type === 'create' ? '/city/' . $city->getId() . '/market/create/' :
									 '/city/' . $city->getId() . '/market/' . $market->getId () . '/edit/');
		$array_mustache = array (
			'id' => $market->getId (),
			'legend' => ($market->getTitle () ? $market->getTitle () : 'Новый магазин'),
			'title' => $market->getTitle (),
			'address' => $market->getAddress (),
			'phone' => $market->getPhone (),
			'site' => $market->getSite (),
			'latitude' => $market->getLatitude (),
			'longitude' => $market->getLongitude(),
			'market_menu' => true,
			'action' => $url,
			'description' => $market->getDescription (),
			'city_id' => $city->getId (),
			'city_title' => $city->getTitle (),
			'country_id' => $country->getId (),
			'country_title' => $country->getTitle (),
			'words' => $words,
			'brands' => array(),
		);

		foreach ($market->getBrands () as $brand){

			$array_mustache['brands'][] = array(
				'id' => $brand->getId (),
				'title' => $brand->getTitle (),
			);
		}
		$array_mustache['script'][] = array ('scr' => '/j/market.js');
		$array_mustache['script_var'] = json_encode(array('url' => '/brand/word/'));

		echo $mustache->render('market/create_edit', $array_mustache);
	}

}