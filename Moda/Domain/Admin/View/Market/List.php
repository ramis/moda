<?php
/**
 * Список
 */
namespace Moda;

class Domain_Admin_View_Market_List
	extends Domain_Admin_View_Abstract
{

	public function __construct (Entity\City $city)
	{

		$mustache = parent::common();

		$country = $city->getCountry();

		$array_mustache = array (
			'city_id' => $city->getId (),
			'city_title' => $city->getTitle (),
			'country_id' => $country->getId (),
			'country_title' => $country->getTitle ()
		);

		$market_published = Common\Repositories::market ()->findBy(array('city' => $city), array('title'=>'asc'));
		$markets = array ();
		foreach ($market_published as $item) {
			$word = mb_substr ($item->getTitle (), 0, 1, 'UTF-8');

			$tmp_markets = isset ($markets[$word]) ? $markets[$word]['markets'] : array();
			$tmp_markets[] = array (
				'id' => $item->getId (),
				'title' => $item->getTitle (),
			);

			$markets[$word]['markets'] = $tmp_markets;
			$markets[$word]['word'] = $word;
		}
		$array_mustache['word_markets'] = new \ArrayIterator( $markets );
		echo $mustache->render('market/list', $array_mustache);
	}

}