<?php
namespace Moda;

class Domain_Admin_View_Model_CreateEdit
	extends Domain_Admin_View_Abstract
{

	public function __construct (Entity\Model $model, $type, $error = false)
	{
		$mustache = parent::common();

		$array_mustache = array (
			'id' => $model->getId (),
			'legend' => ($model->getName() ? $model->getName () . ' ' . $model->getSurname () : 'Новая Модель'),
			'name' => $model->getName(),
			'surname' => $model->getSurname(),
			'description' => $model->getDescription (),
			'popular_checked' => ($model->isPopular() ? 'checked' : ''),
			'class_name' => (string)$model,
			'action' => ($type === 'create' ? '/model/create/' : '/model/'. $model->getId () .'/edit/'),
			'model_menu' => true,
		);


		foreach(Common\Repositories::image()->findBy
				(array('parent' => (string)$model, 'parent_id' => $model->getId()), array('sort'=>'asc')) as $i)
		{
			$array_mustache['images'][] = array(
				'id' => $i->getId (),
				'hash' => HTTP_IMAGE_P_50x50 . $i->getHash (),
			);
		}

		echo $mustache->render('model/create_edit', $array_mustache);

	}

}