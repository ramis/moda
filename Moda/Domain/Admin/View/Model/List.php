<?php
/**
 * Список
 */
namespace Moda;

class Domain_Admin_View_Model_List
	extends Domain_Admin_View_Abstract
{

	public function __construct (array $models_published, $word)
	{

		$mustache = parent::common();

		$array_mustache['models'] = array ();
		foreach ($models_published as $item) {
			$array_mustache['models'][] = array (
				'id' => $item->getId (),
				'name' => $item->getName () . ' ' . $item->getSurname (),
			);
		}


		if ($word !== ''){
			$array_mustache['title'] = parent::getRusWord($word);
		}else{
			$array_mustache['words'] = parent::getRusAlpfavit();
		}

		$array_mustache['model_menu'] = true;

		echo $mustache->render('model/list', $array_mustache);
	}

}