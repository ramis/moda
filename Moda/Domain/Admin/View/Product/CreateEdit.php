<?php
namespace Moda;

class Domain_Admin_View_Product_CreateEdit
	extends Domain_Admin_View_Abstract
{

	public function __construct (Entity\Brand\Type $brand_type, Entity\Brand\Type\Product $product, $mode, $error = false)
	{
		$mustache = parent::common();

		$array_mustache = array (
			'id' => $product->getId (),
			'legend' => ($product->getId() ? 'Редактирование' : 'Новое предложение'),
			'title' => $product->getTitle (),
			'price' => $product->getPrice (),
			'sale' => $product->getSale (),
			'description' => $product->getDescription (),
			'action' => ($mode === 'create' ? '/brand/type/' . $brand_type->getId() . '/product/create/' :
							'/brand/type/' . $brand_type->getId() . '/product/' . $product->getId () . '/edit/'),
			'brand_id' => $brand_type->getBrand()->getId(),
			'brand_title' => $brand_type->getBrand()->getTitle(),
			'brand_type_id' => $brand_type->getId(),
			'brand_type_title' => $brand_type->getTitle(),
			'brand_menu' => true,
		);

		echo $mustache->render ('product/create_edit', $array_mustache);

	}

}