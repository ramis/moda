<?php
/**
 * Список
 */
namespace Moda;

class Domain_Admin_View_Product_List
	extends Domain_Admin_View_Abstract
{

	public function __construct (Entity\Brand\Type $brand_type)
	{

		$mustache = parent::common();

		$array_mustache = array (
			'brand_id' => $brand_type->getBrand()->getId (),
			'brand_title' => $brand_type->getBrand()->getTitle (),
			'brand_type_id' => $brand_type->getId (),
			'brand_type_title' => $brand_type->getTitle (),
		);

		$array_mustache['products'] = array ();
		foreach (Common\Repositories::product()->findBy(array('brand_type' => $brand_type), array('price'=>'asc')) as $item) {
			$array_mustache['products'][] = array (
				'id' => $item->getId (),
				'title' => $item->getTitle (),
				'price' => $item->getPrice (),
				'sale' => $item->getSale (),
			);

		}

		echo $mustache->render ('product/list', $array_mustache);
	}

}