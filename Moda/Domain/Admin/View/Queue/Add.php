<?php
/**
 * Список
 */
namespace Moda;

class Domain_Admin_View_Queue_Add
	extends Domain_Admin_View_Abstract
{

	public function __construct ()
	{

		$mustache = parent::common();

		echo $mustache->render ('queue/add', array('name_file' => uniqid()));
	}

}