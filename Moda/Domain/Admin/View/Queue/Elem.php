<?php
namespace Moda;

class Domain_Admin_View_Queue_Elem
	extends Domain_Admin_View_Abstract
{

	public function __construct (Entity\Queue $queue)
	{
		$mustache = parent::common();

		$array_mustache = array (
			'id' => $queue->getId (),
			'title' => $queue->getTitle (),
			'message' => $queue->getMessage(),
			'queue_menu' => true,
		);

		echo $mustache->render ('queue/elem', $array_mustache);

	}

}