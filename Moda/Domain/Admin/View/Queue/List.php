<?php
/**
 * Список
 */
namespace Moda;

class Domain_Admin_View_Queue_List
	extends Domain_Admin_View_Abstract
{

	public function __construct ()

	{

		$mustache = parent::common();

		$array_mustache = array (
			'queue_menu' => true,
		);

		$queuies = Common\Repositories::queue()->findBy(array('is_processed' => true), array('create_time'=>'desc'), 10);
		foreach ($queuies as $item) {
			$array_mustache['queue'][] = array (
				'id' => $item->getId (),
				'title' => $item->getTitle (),
				'processed_time' => $item->getProcessedTime (),
				'create_time' => $item->getCreateTime (),
				'is_processed' => $item->isProcessed (),
				'is_error' => $item->isError (),
			);

		}

		$queuies = Common\Repositories::queue()->findBy(array('is_processed' => false), array('create_time'=>'desc'));
		foreach ($queuies as $item) {
			$array_mustache['queue_list'][] = array (
				'id' => $item->getId (),
				'title' => $item->getTitle (),
				'processed_time' => $item->getProcessedTime (),
				'create_time' => $item->getCreateTime (),
				'is_processed' => $item->isProcessed (),
				'is_error' => $item->isError (),
			);

		}

		echo $mustache->render ('queue/list', $array_mustache);
	}

}