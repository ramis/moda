<?php
namespace Moda;

class Domain_Admin_View_Type_CreateEdit
	extends Domain_Admin_View_Abstract
{

	public function __construct (Entity\View $view, Entity\Type $type, $mode, $error = false)
	{
		$mustache = parent::common();

		$array_mustache = array (
			'id' => $type->getId (),
			'legend' => ($type->getTitle() ? $type->getTitle() : 'Новый тип'),
			'title' => $type->getTitle (),
			'description' => $type->getDescription (),
			'action' => ($mode === 'create' ? '/view/' . $view->getId() . '/create/' : '/view/' . $view->getId() . '/type/' . $type->getId () . '/edit/'),
			'view_id' => $view->getId(),
			'view_title' => $view->getTitle(),
			'view_menu' => true,
		);

		echo $mustache->render ('type/create_edit', $array_mustache);

	}

}