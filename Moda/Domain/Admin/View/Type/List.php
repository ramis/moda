<?php
/**
 * Список
 */
namespace Moda;

class Domain_Admin_View_Type_List
	extends Domain_Admin_View_Abstract
{

	public function __construct (Entity\View $view)
	{

		$mustache = parent::common();

		$array_mustache = array (
			'view_id' => $view->getId (),
			'view_title' => $view->getTitle (),
		);

		$array_mustache['types'] = array ();
		foreach (Common\Repositories::type()->findBy(array('view' => $view), array('title'=>'asc')) as $item) {
			$array_mustache['types'][] = array (
				'id' => $item->getId (),
				'title' => $item->getTitle (),
			);

		}

		echo $mustache->render ('type/list', $array_mustache);
	}

}