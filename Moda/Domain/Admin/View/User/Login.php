<?php

namespace Moda;

class Domain_Admin_View_User_Login
	extends Domain_Admin_View_Abstract
{

	public function __construct ()
	{
		$mustache = parent::common();
		echo $mustache->render('login/form', array());
	}

}