<?php
namespace Moda;

class Domain_Admin_View_View_CreateEdit
	extends Domain_Admin_View_Abstract
{

	public function __construct (Entity\View $view,  $type, $error = false)
	{
		$mustache = parent::common();

		$array_mustache = array (
			'id' => $view->getId (),
			'legend' => ($view->getTitle() ? $view->getTitle() : 'Новый тип'),
			'title' => $view->getTitle (),
			'description' => $view->getDescription (),
			'action' => ($type === 'create' ? '/view/create/' : '/view/' . $view->getId() . '/edit/'),
			'view_menu' => true,
		);

		echo $mustache->render ('view/create_edit', $array_mustache);

	}

}