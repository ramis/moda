<?php
/**
 * Список
 */
namespace Moda;

class Domain_Admin_View_View_List
	extends Domain_Admin_View_Abstract
{

	public function __construct ()
	{

		$mustache = parent::common();

		$views = array ();
		foreach (Common\Repositories::view()->findBy(array(), array('id'=>'asc')) as $item) {
			$views[] = array (
				'id' => $item->getId (),
				'title' => $item->getTitle (),
			);
		}

		echo $mustache->render('view/list', array('views' => $views, 'view_menu' => true));
	}

}