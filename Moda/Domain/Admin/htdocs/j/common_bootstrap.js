$(document).ready(function(){

	function qapi(url)
	{
		$.getJSON(url, function(response) {
			switch (response.code) {
				case 'notification':
					$('body').append('<div class="notifications top-right"></div>');
					$('.notifications').notify({
						type: response.response.type,
						message: { text: response.response.message },
						fadeOut: {
							enabled: true,
							delay: 2000
						},
						onClosed: function(){
							$('.notifications').remove();
						}
					}).show();
					break;
				default:
					// @todo
					break;
			}
			if (response.response.execute != undefined) {
				eval(response.response.execute);
			}
		});
	}


	$('[modal="window"]').live('click', function(){
		$.get($(this).attr('href'), function(response){
			$('body').append(response);
			var modal = $('.modal').modal({backdrop: 'static'});
			modal.on('hidden', function(){
				modal.remove();
			});
		}).error(function(){alert(0);});
		return false;
	});

	$('[modal="confirm"]').live('click', function(){
		var link = $(this).attr('href');
		var html = '<div class="modal fade">' +
			'<div class="modal-header">' +
			'<a class="close" data-dismiss="modal">×</a>' +
			'<h3>Подтверждение</h3>' +
			'</div>' +
			'<div class="modal-body">' +
			'<p>' + $(this).attr('confirm') + '</p>' +
			'</div>' +
			'<div class="modal-footer">' +
			'<a href="#close" class="btn" data-dismiss="modal">Закрыть</a>' +
			'<a href="#ok" class="btn btn-primary">OK</a>' +
			'</div>' +
			'</div>';

		$('body').append(html);
		var modal = $('.modal').modal({backdrop: false});

		$('a[href="#ok"]', modal).click(function(){
//			window.location.href = link;
			modal.remove();
			qapi(link);
		});
		modal.on('hidden', function(){
			modal.remove();
		});
		return false;
	});

	$(document).ready(function(){
		$('a[rel="tooltip"]').tooltip();
	});

	$('form[ajax!="false"]').live('submit', function(){
		var form = $(this);
		form.ajaxSubmit({
			dataType: 'json',
			beforeSubmit: function()
			{
				$('.alert', form).remove();
				$('<div class="alert alert-waiting"><a class="close" data-dismiss="alert">×</a>...</div>').insertBefore($('fieldset:first', form));
			},
			success: function(response, status, xhr)
			{
				$('.alert', form).remove();
				switch (response.code) {
					case 'error':
						var message = '';
						for (var f in response.response) {
							message += response.response[f] + '<br />';
						}
						$('<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a><p>' + message + '</p></div>').insertBefore($('fieldset:first', form));
						break;
					case 'success':
						$('<div class="alert alert-success"><a class="close" data-dismiss="alert">×</a>' + response.response + '</div>').insertBefore($('fieldset:first', form));
						break;
					case 'redirect':
						window.location.href = response.response;
						break;
					case 'notification':
						$('body').append('<div class="notifications top-right"></div>');
						$('.notifications').notify({
							type: response.response.type,
							message: { text: response.response.message },
							fadeOut: {
								enabled: true,
								delay: 3000
							},
							onClosed: function(){
								$('.notifications').remove();
							}
						}).show();
						break;
					default:
						eval(response.status + '(\'' + xhr.responseText + '\');');
						break
				}
				if (response.response.execute != undefined) {
					eval(response.response.execute);
				}
			}
		});
		return false;
	});
});