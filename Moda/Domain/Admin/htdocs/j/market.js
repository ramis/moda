$(function(){
    $('.word').click (function(){
        $('#container_words').html('');
        word = $(this).html ();

        url = vars.url + word + '/';
        $.get( url , function( data ) {
            for (i = 0; i < data.length; i++){
                span = '<span class="element" id="element_' + data[i].alias + '_' + data[i].id + '">';
                span += ' <span id="text_' + data[i].id + '">' + data[i].title + '</span>';
                span += ' <span class="add_element icon-plus" alias="' + data[i].alias + '" id="add_' + data[i].id + '"></span>';
                span += '</span>';
                $('#container_words').append(span);
            }

            $('.add_element').click (function(){
                id = $(this).attr('id').substr(4);
                alias = $(this).attr('alias');

                if($('#'+alias + '_' + id).attr('id') == undefined){
                    div = '<div class="element" id="' + alias + '_' + id + '">' + $('#text_' + id).html();
                    div += '<input type="checkbox" name="' + alias + '[]" value="' + id + '" checked />';
                    div += ' <span class="icon-remove-sign remove_element"></span>';
                    div += '</div>';
                    $('#container_elements').append(div);

                    $('.remove_element').click (function (){
                        $(this).parent().remove();
                    });
                }
            })
        }, "json" );
    });

    $('.remove_element').click (function (){
       $(this).parent().remove();
    });

    $('#inputAddress').focusout(function() {
        city = $('#cityTitle').val();
        address = city + $('#inputAddress').val();
        url = '/city/market/coordinate/?address=' + address ;
        $.get( url , function( data ) {
            if(data.latitude){
                $('#inputLatitude').val(data.latitude);
                $('#inputLongitude').val(data.longitude);
            }
        }, "json" );
    });
});