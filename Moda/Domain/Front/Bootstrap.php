<?php

namespace Moda\Domain\Front;

class Bootstrap
	extends \Moda\Common\Bootstrap
{

	/**
	 * Файлы настроек для админской части сайта
	 *
	 * @return null
	 */
	public function initConfig ()
	{
		define ('DOMAIN_PATH_TEMPLATE', PATH . 'Moda/Domain/Front/tpl');

		require PATH . 'Moda/config/' . APPLICATION_SITE . '-config.php';
	}

	/**
	 * Настройки front-controller для админки
	 *
	 * @return null
	 */
	public function initFrontController ()
	{
		//прописываем маршруты
		$router = new \Iir\Router;

		$routes = array (

			'index' =>
				array (\Iir\Router::ROUTE_STATIC, '', 'Index', 'Index'),
			'fitting_room' =>
				array (\Iir\Router::ROUTE_STATIC, 'fitting_room', 'Index', 'FittingRoom'),

			'designers' =>
				array (\Iir\Router::ROUTE_STATIC, 'designers', 'Designer', 'List'),
			'designers_full' =>
				array (\Iir\Router::ROUTE_STATIC, 'designers/full', 'Designer', 'ListFull'),
			'designers_word' =>
				array (\Iir\Router::ROUTE_REGEXP, 'designers/([A-Za-z_\-\d]+)', 'Designer', 'ListWord', 'designers/%s'),
			'designer' =>
				array (\Iir\Router::ROUTE_REGEXP, 'designer/(\d+)', 'Designer', 'Designer', 'designer/%d'),

			'models' =>
				array (\Iir\Router::ROUTE_STATIC, 'models', 'Model', 'List'),
			'models_full' =>
				array (\Iir\Router::ROUTE_STATIC, 'models/full', 'Model', 'ListFull'),
			'models_word' =>
				array (\Iir\Router::ROUTE_REGEXP, 'models/([A-Za-z_\-\d]+)', 'Model', 'ListWord', 'models/%s'),
			'model' =>
				array (\Iir\Router::ROUTE_REGEXP, 'model/(\d+)', 'Model', 'Model', 'model/%d'),

			'product_search' =>
				array (\Iir\Router::ROUTE_STATIC, 'product/search', 'Product', 'Search'),
			'product_search_result' =>
				array (\Iir\Router::ROUTE_STATIC, 'product/search/result', 'Product', 'SearchResult'),
			'product' =>
				array (\Iir\Router::ROUTE_REGEXP, 'product/(\d+)', 'Product', 'Product', 'Product', 'product/%d'),

			'markets' =>
				array (\Iir\Router::ROUTE_REGEXP, 'markets', 'Market', 'List'),
			'markets_city' =>
				array (\Iir\Router::ROUTE_REGEXP, 'markets/([A-Za-z_\-\d]+)', 'Market', 'MarketsCity' ,'markets/%s'),
			'markets_city_brand' =>
				array (\Iir\Router::ROUTE_REGEXP, 'markets/([A-Za-z_\-\d]+)/([A-Za-z_\-\d]+)', 'Market', 'MarketsCityBrand' ,'markets/%s/%s'),
			'market' =>
				array (\Iir\Router::ROUTE_REGEXP, 'market/([A-Za-z_\-\d]+)/([A-Za-z_\-\d]+)/(\d+)', 'Market', 'Market' ,'market/%s/%s/%d'),

			'brands' =>
				array (\Iir\Router::ROUTE_STATIC, 'catalog', 'Brand', 'List'),
			'brands_full' =>
				array (\Iir\Router::ROUTE_STATIC, 'catalog/full', 'Brand', 'ListFull'),
			'brands_word' =>
				array (\Iir\Router::ROUTE_REGEXP, 'catalog/([A-Za-z_\-\d])', 'Brand', 'ListWord', 'catalog/%s'),
			'brand' =>
				array (\Iir\Router::ROUTE_REGEXP, '([A-Za-z_\-\d]+)', 'Brand', 'Brand', '%s'),

		);

		$router->setRoutes (DOMAIN_HTTP_FRONT, $routes);

		\Iir\Front_Controller::setRouter ($router);

		//обработка 404 ошибки
//			$plugin2 = new Common_Domains_FCP_404;
//			\Iir\Front_Controller::registerPlugin ($plugin2);

		//префикс контроллеров
		\Iir\Factory_Controller::setPrefix ('Front');
	}

	/**
	 * Инициализировать обработчик ошибок
	 *
	 * @return void
	 */
	public function initErrorHandler ()
	{

	}

}