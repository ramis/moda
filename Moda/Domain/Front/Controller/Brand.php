<?php

namespace Moda\Domain\Front\Controller;

use Moda;
use Moda\Common;
use Moda\Entity;

class Brand extends ControllerAbstract
{

	/**
	 * Бренды
	 *
	 * @param bool $is_full
	 * @return void
	 */
	public function actionList ($is_full = false)
	{
		new Moda\Domain_Front_View_Brand_List ($is_full);
	}

	/**
	 * Все бренды
	 *
	 * @return void
	 */
	public function actionListFull ()
	{
		$this->actionList(true);
	}

	public function actionBrand ($brand_url)
	{
		$brand = Moda\Common\Repositories::brand()->findOneBy(array('url' => $brand_url));

		if($brand instanceof Entity\Brand){
			new Moda\Domain_Front_View_Brand ($brand);
		}else{
			new Moda\Domain_Front_View_404 ();
		}

	}

	/**
	 * Бренды по 1 букве
	 *
	 * @param string $word
	 * @return void
	 */
	public function actionListWord ($word){
		new Moda\Domain_Front_View_Brand_List (false, $word);
	}

}
