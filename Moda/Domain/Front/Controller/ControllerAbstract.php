<?php

namespace Moda\Domain\Front\Controller;

abstract class ControllerAbstract
	extends \Iir\Controller_Abstract
{

	public function __construct ()
	{

	}

	/**
	 * @access protected
	 * @return View
	 */
	protected function getView ()
	{
		//return new Common_Domains_View ('Domain/Admin');
	}

	/**
	 * Хук перед стартом работы контроллера
	 *
	 * @param Route_Result $route_result
	 * @return boolean
	 */
	public function preAction (\Iir\Route_Result $route_result)
	{
		return true;
	}

}
