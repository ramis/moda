<?php
namespace Moda\Domain\Front\Controller;

use Moda;
use Moda\Common;
use Moda\Entity;

class Designer extends ControllerAbstract
{
	/**
	 * Список дизайнеров
	 *
	 * @param bool $is_full
	 * @retunt void
	 */
	public function actionList ($is_full = false)
	{
		new Moda\Domain_Front_View_Designer_List ($is_full);
	}

	/**
	 * Полный список дизайнеров
	 *
	 * @return void
	 */
	public function actionListFull ()
	{
		$this->actionList(true);
	}

	/**
	 * Дизайнер
	 *
	 * @param int $designer_id
	 * @return void
	 */
	public function actionDesigner ($designer_id)
	{

		$designer = Moda\Common\Repositories::designer()->find((int) $designer_id);

		if($designer instanceof Entity\Designer){
			new Moda\Domain_Front_View_Designer ($designer);
		}else{
			new Moda\Domain_Front_View_404 ();
		}

	}

	/**
	 * Дизайнеры по 1 букве
	 *
	 * @param string $word
	 * @return void
	 */
	public function actionListWord ($word){
		new Moda\Domain_Front_View_Designer_List (false, $word);
	}

}
