<?php
namespace Moda\Domain\Front\Controller;

use Moda;
class Index
	extends ControllerAbstract
{

	/**
	 * Главная страница
	 */
	public function actionIndex ()
	{
		new Moda\Domain_Front_View_Index ();
	}

	/**
	 * Примерочная
	 */
	public function actionFittingRoom ()
	{
		new Moda\Domain_Front_View_FittingRoom ();
	}

}
