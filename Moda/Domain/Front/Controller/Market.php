<?php

namespace Moda\Domain\Front\Controller;

use Moda;
use Moda\Common;
use Moda\Entity;
use Symfony\Component\Finder\Shell\Command;

class Market extends ControllerAbstract
{

	/**
	 * Страны + города
	 *
	 * @return void
	 */
	public function actionList ()
	{
		Common\Redirect::redirect ('/markets/msk/');
		return;
		new Moda\Domain_Front_View_Market_List ();
	}

	/**
	 * Бренды у которых есть магазины в данном городе
	 *
	 * @param string $city_url
	 * @return void
	 */
	public function actionMarketsCity ($city_url)
	{
		$city = Common\Repositories::city()->findOneBy(array('url' => $city_url));
		if($city === null){
			new Moda\Domain_Front_View_404 ();
			return;
		}

		new Moda\Domain_Front_View_Market_City ($city);
	}

	/**
	 * Магазины по бренду + городу
	 *
	 * @param string $city_url
	 * @param string $brand_url
	 * @return void
	 */
	public function actionMarketsCityBrand ($city_url, $brand_url)
	{

		$city = Common\Repositories::city ()->findOneBy (array('url' => $city_url));
		if($city === null){
			new Moda\Domain_Front_View_404 ();
			return;
		}

		$brand = Common\Repositories::brand ()->findOneBy (array('url' => $brand_url));
		if($brand === null){
			new Moda\Domain_Front_View_404 ();
			return;
		}

		new Moda\Domain_Front_View_Market_Brand ($city, $brand);
	}

	/**
	 * Инфо по магазину
	 *
	 * @param string $city_url
	 * @param string $brand_url
	 * @param integer $market_id
	 * @return void
	 */
	public function actionMarket ($city_url, $brand_url, $market_id)
	{

		$city = Common\Repositories::city ()->findOneBy (array('url' => $city_url));
		if($city === null){
			new Moda\Domain_Front_View_404 ();
			return;
		}

		$brand = Common\Repositories::brand ()->findOneBy (array('url' => $brand_url));
		if($brand === null){
			new Moda\Domain_Front_View_404 ();
			return;
		}

		$market = Common\Repositories::market ()->find ($market_id);
		if($market === null){
			new Moda\Domain_Front_View_404 ();
			return;
		}
		new Moda\Domain_Front_View_Market_Market ($city, $brand, $market);
	}


}
