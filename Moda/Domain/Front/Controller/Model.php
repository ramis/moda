<?php
namespace Moda\Domain\Front\Controller;

use Moda;
use Moda\Common;
use Moda\Entity;

class Model extends ControllerAbstract
{

	/**
	 * Модели
	 *
	 * @param bool $is_full
	 * @return void
	 */
	public function actionList ($is_full = false)
	{
		new Moda\Domain_Front_View_Model_List ($is_full);
	}

	/**
	 * Все модели
	 *
	 * @return void
	 */
	public function actionListFull ()
	{
		$this->actionList(true);
	}

	/**
	 * Модели по 1 букве
	 *
	 * @param string $word
	 * @return void
	 */
	public function actionListWord ($word){
		new Moda\Domain_Front_View_Model_List (false, $word);
	}

	/**
	 * Модель
	 *
	 * @param int $model_id
	 * @return void
	 */
	public function actionModel ($model_id)
	{

		$model = Moda\Common\Repositories::model()->find((int) $model_id);

		if($model instanceof Entity\Model){
			new Moda\Domain_Front_View_Model ($model);
		}else{
			new Moda\Domain_Front_View_404 ();
		}

	}

}
