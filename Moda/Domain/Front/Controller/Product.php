<?php
namespace Moda\Domain\Front\Controller;

use Moda;
use Moda\Common;
use Moda\Entity;

class Product extends ControllerAbstract
{

	/**
	 *
	 * @param int $product_id
	 */
	public function actionProduct ($product_id)
	{

		$product = Moda\Common\Repositories::product()->find((int) $product_id);

		if($product instanceof Entity\Product){
			new Moda\Domain_Front_View_Product ($product);
		}else{
			new Moda\Domain_Front_View_404 ();
		}

	}

	/**
	 * Поиск
	 */
	public function actionSearch ()
	{
		new Moda\Domain_Front_View_Product_Search ();
	}

	/**
	 * Поиск
	 */
	public function actionSearchResult ()
	{
		new Moda\Domain_Front_View_Product_SearchResult ();
	}

}
