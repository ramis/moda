<?php
namespace Moda;

class Domain_Front_View_404
	extends Domain_Front_View_Abstract
{

	public function __construct ()
	{
		$mustache = parent::common();

		echo $mustache->render ('404', array());
	}

}