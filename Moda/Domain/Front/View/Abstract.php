<?php
namespace Moda;

class Domain_Front_View_Abstract
{

	static $alpfavit = array (
		'a' => 'а','b' => 'б','v' => 'в','g' => 'г','d' => 'д','e' => 'е','jo' => 'ё','zh' => 'ж','z' => 'з',
		'i' => 'и','jj' => 'й','k' => 'к','l' => 'л','m' => 'м','n' => 'н','o' => 'о','p' => 'п','r' => 'р',
		's' => 'с','t' => 'т','u' => 'у','f' => 'ф','kh' => 'х','c' => 'ч','ch' => 'ц','sh' => 'ш','shh' => 'щ',
		'eh' => 'э','ju' => 'ю','ja' => 'я'
	);
	/**
	 * Шаблонизатор
	 *
	 * @return Common\Mustache_Engine
	 */
	protected static function common ()
	{
		return Common\Bootstrap::getMustache ();
	}

	/**
	 * Русский алфавит
	 *
	 * @return array
	 */
	protected static function getRusAlpfavit (){

		$words = array();
		foreach(self::$alpfavit as $key=>$val){
			$words[] = array(
				'up' => strtoupper ($val),
				'url' => $key
			);
		}
		return $words;
	}

	/**
	 * Русская буква по английской
	 *
	 * @param string $word
	 * @return string
	 */
	protected static function getRusWord ($word){
		return self::$alpfavit[$word];
	}

	/**
	 * Массив данных Бренда
	 *
	 * @param Entity\Brand $brand
	 * @param integer|null $i
	 * @param integer $delimiter
	 * @return array
	 */
	protected function getArrayBrand (Entity\Brand $brand, $i, $delimiter = 3) {

		$annotation = $brand->getAnnotation();
		if(mb_strlen($annotation, 'UTF-8') > 180){
			$annotation = mb_substr($annotation, 0, mb_stripos($annotation, ' ', 170, 'UTF-8'),'UTF-8');
		}

		return  array (
			'id' => $brand->getId (),
			'url' => $brand->getUrl (),
			'title' => $brand->getTitle (),
			'annotation' => $annotation,
			'logo' => ($brand->getMainImage () !== null ?
					HTTP_IMAGE_P_150x150 . $brand->getMainImage ()->getHash() : ''),
			'lastcolumn' => ((($i+1) % $delimiter) === 0 ? 'lastcolumn' : ''),
		);

	}

	/**
	 * Массив данных Дизайнера
	 *
	 * @param Entity\Designer $designer
	 * @return array
	 */
	protected function getArrayDesigner (Entity\Designer $designer) {

		$array_brands = array();
		foreach ($designer->getBrands() as $tmp){
			$array_brands[] = array(
				'id' => $tmp->getId (),
				'title' => $tmp->getTitle (),
			);
		}

		return array (
			'id' => $designer->getId (),
			'name' => $designer->getName () . ' ' . $designer->getSurname (),
			'description' => $designer->getDescription (),
			'url' => '/designer/' . $designer->getId() . '/',
			'image' => ($designer->getMainImage () !== null ?
					HTTP_IMAGE_P_50x50 . $designer->getMainImage ()->getHash() : ''),
			'brands' => $array_brands,
		);
	}

	/**
 	 * Массив данных Модели
	 *
	 * @param Entity\Model $model
	 * @return array
	 */
	protected function getArrayModel (Entity\Model $model){
		return array (
			'id' => $model->getId (),
			'name' => $model->getName () . ' ' . $model->getSurname (),
			'url' => '/model/' . $model->getId() . '/',
			'description' => $model->getDescription (),
			'image' => ($model->getMainImage () !== null ?
					HTTP_IMAGE_P_215x215 . $model->getMainImage ()->getHash() : ''),
		);
	}
}