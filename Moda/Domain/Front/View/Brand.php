<?php
namespace Moda;

use Moda\Entity;

class Domain_Front_View_Brand
	extends Domain_Front_View_Abstract
{

	public function __construct (Entity\Brand $brand)
	{
		$mustache = parent::common();

		$array_mustache =  array(
			'id' => $brand->getId (),
			'title' => $brand->getTitle (),
			'title_rus' => $brand->getTitleRus (),
			'year' => $brand->getYear (),
			'country' => ($brand->getCountry() ? $brand->getCountry()->getTitle() : ''),
			'founder' => $brand->getFounder(),
			'site' => $brand->getSite(),
			'site_shop' => $brand->getSiteShop(),
			'description' => $brand->getDescription(),
			'image' => ($brand->getMainImage() ? HTTP_IMAGE_P_200x200 . $brand->getMainImage ()->getHash () : ''),
		);

		$array_mustache['designers'] = array();
		foreach($brand->getDesigners () as $d){
			$array_mustache['designers'][] = $this->getArrayDesigner($d);
		}

		$array_mustache['models'] = array();
		$i = 1;
		$models = $brand->getModels();
		foreach($models as $d){
			$array_mustache['models'][] = array(
				'id' => $d->getId(),
				'name' => $d->getName() . ' ' . $d->getSurname (),
				'image' => ($d->getMainImage () !== null ? HTTP_IMAGE_P_215x215 . $d->getMainImage ()->getHash() : ''),
				'lastcolumn' => ($models->offsetExists($i) ?  '' : 'lastcolumn'),
			);
			$i++;
		}

		$array_mustache['is_designers'] = (count($array_mustache['designers']) > 0);

		$array_mustache['is_models'] = (count($array_mustache['models']) > 0);

		echo $mustache->render('brand/brand', $array_mustache);
	}

}