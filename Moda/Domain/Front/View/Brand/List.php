<?php
namespace Moda;

use Moda\Entity;

class Domain_Front_View_Brand_List
	extends Domain_Front_View_Abstract
{

	public function __construct ($is_full, $word = '')
	{
		$mustache = parent::common ();

		if($is_full === false){
			$words = array();
			for ($i = 0; $i< 26; $i++){
				$w = chr(97+$i);
				$words[] = array(
					'up' => strtoupper ($w),
					'url' => '/catalog/' . $w . '/',
					'current' => ($w === $word ? 'current' : ''),
				);
			}
		}

		$array_mustache = array ();
		if($is_full){
			$brands_pub = Common\Repositories::brand()->findBy(array('parent' => NULL), array('title'=>'asc'));

			$brands = array ();
			foreach ($brands_pub as $key=>$item) {
				$word = mb_substr($item->getTitle(), 0, 1, 'UTF-8');

				$sub_brands = array();
				if($item->getBrands ()){
					foreach($item->getBrands () as $k=>$sub_item){
						$sub_brands[] = $this->getArrayBrand($sub_item, $k);
					}
				}
				$tmp_brand = $this->getArrayBrand($item, $key);
				$tmp_brand['sub_brands'] = $sub_brands;
				$tmp_brand['is_sub_brands'] = (count($sub_brands) > 0);
				$tmp_brands = isset ($brands[$word]) ? $brands[$word]['brands'] : array();
				$tmp_brands[] = $tmp_brand;

				$brands[$word]['brands'] = $tmp_brands;
				$brands[$word]['word'] = $word;
			}
			$array_mustache['title_page'] = 'Все бренды в каталоге';

			$array_mustache['word_brands'] = new \ArrayIterator( $brands );

			echo $mustache->render('brand/list_full', $array_mustache);
		}elseif($word !== ''){
			$brands_pub = Common\Repositories::brand()->findBy(array('word' => $word), array('title'=>'asc'));

			$brands = array();
			foreach ($brands_pub as $k=>$item) {
				$brands[] = $this->getArrayBrand($item, $k);
			}

			$array_mustache['title_page'] = 'Бренды на букву ' . $word;

			$array_mustache['brands'] = $brands;
			$array_mustache['words'] = $words;
			$array_mustache['back'] = true;
			echo $mustache->render('brand/list', $array_mustache);
		}else{
			$array_mustache['words'] = $words;
			$array_mustache['title_page'] = 'Популярные бренды';

			$array_mustache['brands'] = array();
			$i = 0;
			$brands = Common\Repositories::brand()->findBy(
				array('parent' => NULL, 'is_popular' => 1), array('title' => 'asc'), 3);
			foreach ($brands as $k=>$item) {
				$array_mustache['brands'][] = $this->getArrayBrand($item, $k);
				$i++;
			}
			shuffle($array_mustache['brands']);

			echo $mustache->render('brand/list', $array_mustache);
		}

	}

}