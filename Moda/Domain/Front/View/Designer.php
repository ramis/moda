<?php
namespace Moda;

class Domain_Front_View_Designer
	extends Domain_Front_View_Abstract
{

	public function __construct (Entity\Designer $designer)
	{
		$mustache = parent::common();

		$array_mustache = array(
			'id' => $designer->getId (),
			'name' => $designer->getName() . ' ' . $designer->getSurname(),
			'description' => $designer->getDescription(),
			'image' => ($designer->getMainImage() ?
					HTTP_IMAGE_P_400x400 . $designer->getMainImage ()->getHash () : ''),
		);

		$array_mustache['brands'] = array();
		foreach($designer->getBrands () as $b){
			$array_mustache['brands'][] = array(
				'id' => $b->getId(),
				'title' => $b->getTitle (),
				'annotation' => $b->getAnnotation (),
				'url' => $b->getUrl (),
				'image' => ($b->getMainImage () !== null ? HTTP_IMAGE_P_50x50 . $b->getMainImage ()->getHash() : ''),
			);
		}


		echo $mustache->render('designer/designer', $array_mustache);

	}
}