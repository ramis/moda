<?php
namespace Moda;

use Moda\Entity;

class Domain_Front_View_Designer_List
	extends Domain_Front_View_Abstract
{

	public function __construct ($is_full, $word = '')
	{
		$mustache = parent::common ();

		$array_mustache = array ();

		if($is_full){
			$designers_published = Common\Repositories::designer()->findBy(array(), array('name'=>'desc'));

			$designers = array ();
			foreach ($designers_published as $item) {
				$word = mb_substr($item->getSurname (), 0, 1, 'UTF-8');

				$tmp_designers = isset ($designers[$word]) ? $designers[$word]['designers'] : array();
				$tmp_designers[] = $this->getArrayDesigner($item);

				$designers[$word]['designers'] = $tmp_designers;
				$designers[$word]['word'] = $word;
			}

			$array_mustache['word_designers'] = new \ArrayIterator( $designers );

			echo $mustache->render('designer/list_full', $array_mustache);
		}elseif($word !== ''){

			$designers_pub = Common\Repositories::designer()->findBy(array('word' => $word), array('name'=>'desc'));
			$designers = array();
			foreach ($designers_pub as $i=>$item) {
				$tmp = $this->getArrayDesigner($item);
				$tmp['index'] = $i+1;
				$designers[] = $tmp;
			}

			$array_mustache['title_page'] = 'Дизайнеры на букву &#171;' .
				mb_strtoupper(parent::getRusWord($word), 'utf-8') . '&#187;';

			$array_mustache['designers'] = $designers;
			$array_mustache['words'] = parent::getRusAlpfavit();
			$array_mustache['back'] = true;
			echo $mustache->render('designer/list', $array_mustache);
		}else{
			$designers = array();

			foreach (Common\Repositories::designer()->findBy(
						 array('is_popular' => 1), array('name' => 'asc')) as $i=>$item)
			{
				$tmp = $this->getArrayDesigner($item);
				$tmp['index'] = $i+1;
				$designers[] = $tmp;
			}

			$array_mustache['designers'] = $designers;
			$array_mustache['title_page'] = 'Модные дизайнеры';

			$array_mustache['words'] = parent::getRusAlpfavit();
			echo $mustache->render('designer/list', $array_mustache);
		}
	}

}