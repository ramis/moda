<?php
namespace Moda;

class Domain_Front_View_FittingRoom
	extends Domain_Front_View_Abstract
{

	public function __construct ()
	{

		$mustache = parent::common();

		$array_mustache = array ();

		echo $mustache->render('fitting_room', $array_mustache);

	}

}