<?php
namespace Moda;

use Moda\Entity;

class Domain_Front_View_Index
	extends Domain_Front_View_Abstract
{

	public function __construct ()
	{

		$mustache = parent::common();

		$array_mustache['brands'] = array();
		$brands = Common\Repositories::brand()->findBy(
			array('parent' => NULL, 'is_popular' => 1), array('title' => 'asc'));
		foreach ($brands as $k=>$item) {
			$array_mustache['brands'][] = $this->getArrayBrand($item, $k);
		}
		shuffle($array_mustache['brands']);
		$array_mustache['brands'] = array_slice($array_mustache['brands'], 0, 9);

		$array_brands_filter = array(
			array ('alias' => 'is_man', 'title' => 'Мужская одежда'),
			array ('alias' => 'is_woman', 'title' => 'Женская одежда'),
			array ('alias' => 'is_baby', 'title' => 'Детская одежда'),
			array ('alias' => 'is_accessories', 'title' => 'Аксессуары'),
			array ('alias' => 'is_perfume', 'title' => 'Парфюм'),
			array ('alias' => 'is_jewelry', 'title' => 'Ювелирные украшения'),
		);
		$array_mustache['brands_filter'] = $array_brands_filter[rand(0, 4)];

		$array_mustache['brands_block'] = array();
		$brands = Common\Repositories::brand()->findBy(array($array_mustache['brands_filter']['alias'] => 1));
		foreach ($brands as $k=>$item) {
			if($item->getMainImage()){
				$array_mustache['brands_block'][] = $this->getArrayBrand($item, $k);
			}
		}

		$array_mustache['designers'] = array();
		$designers = Common\Repositories::designer()->findBy(array('is_popular' => 1), array('name' => 'asc'), 2);
		foreach ($designers as $item){
			$array_mustache['designers'][] = $this->getArrayDesigner($item);
		}

		shuffle($array_mustache['designers']);

		$models = array();
		foreach (Common\Repositories::model()->findBy(array('is_popular' => 1), array('name' => 'asc'), 6) as $item) {
			$models[] = $this->getArrayModel($item);
		}
		shuffle($models);

		for($i=0;$i<6;$i++){
			$key = $i < 3 ? 'models_1' : 'models_2';
			$models[$i]['lastcolumn'] = $i === 2 || $i === 5 ? 'lastcolumn' : '';
			$array_mustache[$key][] = $models[$i];
		}

		$array_mustache['is_main'] = 1;
		echo $mustache->render('index', $array_mustache);

	}

}