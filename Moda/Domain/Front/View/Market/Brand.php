<?php
namespace Moda;

use Moda\Entity;

class Domain_Front_View_Market_Brand
	extends Domain_Front_View_Abstract
{

	public function __construct (Entity\City $city, Entity\Brand $brand)
	{
		$mustache = parent::common ();
		$array_mustache = array(
			'city_url' => $city->getUrl (),
			'city_title' => $city->getTitleGenitive(),
			'brand_title' => $brand->getTitle (),
			'brand_url' => $brand->getUrl (),
		);


		$center_latitude = 0;
		$center_longitude = 0;
		$count_market = 0;
		foreach($brand->getMarkets() as $k=>$market){
			$array_mustache['markets'][] = array(
				'id' => $market->getId (),
				'title' => $market->getTitle (),
				'address' => $market->getAddress (),
				'phone' => $market->getPhone (),
				'site' => $market->getSite (),
				'latitude' => $market->getLatitude (),
				'longitude' => $market->getLongitude (),
				'url' => '/markets/' . $city->getUrl () . '/' . $brand->getUrl () . '/' . $market->getId(). '/',
				'lastcolumn' => (((($k+1)%3) === 0 ) ? 'lastcolumn' : ''),
			);

			$center_latitude += $market->getLatitude ();
			$center_longitude += $market->getLongitude ();
			$count_market++;
		}

		$array_mustache['markets_json'] = json_encode($array_mustache['markets']);
		if($count_market > 0){
			$array_mustache['center_latitude'] = (float) $center_latitude / $count_market;
			$array_mustache['center_longitude'] = (float) $center_longitude / $count_market;
		}

		echo $mustache->render('market/brand', $array_mustache);
	}

}