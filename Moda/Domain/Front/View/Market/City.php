<?php
namespace Moda;

use Moda\Entity;

class Domain_Front_View_Market_City
	extends Domain_Front_View_Abstract
{

	public function __construct (Entity\City $city)
	{
		$mustache = parent::common ();
		$array_mustache = array();

		$array_mustache['city_title'] = $city->getTitleGenitive ();
		$array_mustache['city_url'] = $city->getUrl ();

		$qb = Common\Bootstrap::getEntityManager()->createQueryBuilder();
		$qb
			->select('b')
			->from('Moda\Entity\Brand', 'b')
			->innerJoin('b.markets', 'm')
			->innerJoin('m.city', 'c')
			->where('c = :city')
			->setParameter('city', $city)
			->orderBy('b.title', 'DESC');

		$brands =  $qb->getQuery()->getResult();

		foreach($brands as $k=>$brand){
			$array_mustache['brands'][] = $this->getArrayBrand($brand, $k, 4);
		}

		echo $mustache->render('market/city', $array_mustache);
	}

}