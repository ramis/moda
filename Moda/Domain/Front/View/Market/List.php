<?php
namespace Moda;

use Moda\Entity;

class Domain_Front_View_Market_List
	extends Domain_Front_View_Abstract
{

	public function __construct ()
	{
		$mustache = parent::common ();
		$array_mustache = array();

		$countries = Common\Repositories::country()->findAll(array(), array('title' => 'asc'));

		foreach($countries as $country){

			$cities = array();
			$country_cities = Common\Repositories::city()->findBy(array('country' => $country), array('title' => 'asc'));

			foreach($country_cities as $city){
				$cities[] = array(
					'id' => $city->getId (),
					'title' => $city->getTitle (),
					'url' => $city->getUrl (),
				);
			}
			$array_mustache['countries'][] = array(
				'id' => $country->getId (),
				'title' => $country->getTitle (),
				'cities' => $cities,
			);
		}

		echo $mustache->render('market/list', $array_mustache);
	}

}