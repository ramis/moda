<?php
namespace Moda;

use Moda\Entity;

class Domain_Front_View_Market_Market
	extends Domain_Front_View_Abstract
{

	public function __construct (Entity\City $city, Entity\Brand $brand, Entity\Market $market)
	{
		$mustache = parent::common ();

		$array_mustache = array(
			'city_url' => $city->getUrl (),
			'city_title' => $city->getTitle (),
			'brand_title' => $brand->getTitle (),
			'brand_url' => $brand->getUrl (),


			'id' => $market->getId (),
			'title' => $market->getTitle (),
			'address' => $market->getAddress (),
			'phone' => $market->getPhone (),
			'site' => $market->getSite (),
			'description' => $market->getDescription (),
			'center_latitude' => $market->getLatitude (),
			'center_longitude' => $market->getLongitude (),
		);

		$array_mustache['markets'][] = array(
			'id' => $market->getId (),
			'title' => $market->getTitle (),
			'address' => $market->getAddress (),
			'phone' => $market->getPhone (),
			'latitude' => $market->getLatitude (),
			'longitude' => $market->getLongitude (),
		);

		echo $mustache->render('market/market', $array_mustache);
	}

}