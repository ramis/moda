<?php
namespace Moda;

class Domain_Front_View_Model
	extends Domain_Front_View_Abstract
{

	public function __construct (Entity\Model $model)
	{
		$mustache = parent::common();


		echo $mustache->render ('model/model', array(
			'id' => $model->getId (),
			'name' => $model->getName () . ' ' . $model->getSurname(),
			'description' => $model->getDescription (),
			'image' => ($model->getMainImage() ? HTTP_IMAGE_P_400x400 . $model->getMainImage ()->getHash () : ''),

		));

	}
}