<?php
namespace Moda;

use Moda\Entity;

class Domain_Front_View_Model_List
	extends Domain_Front_View_Abstract
{

	public function __construct ($is_full, $word = '')
	{
		$mustache = parent::common ();

		$array_mustache = array ();

		if($is_full){
			$models_published = Common\Repositories::model()->findBy(array(), array('name'=>'asc'));

			$models = array ();
			foreach ($models_published as $item) {
				$word = mb_substr($item->getSurname (), 0, 1, 'UTF-8');

				$tmp_models = isset ($models[$word]) ? $models[$word]['models'] : array();
				$tmp_models[] = array (
					'id' => $item->getId (),
					'name' => $item->getName () . ' ' . $item->getSurname (),
				);

				$models[$word]['models'] = $tmp_models;
				$models[$word]['word'] = $word;
			}

			$array_mustache['word_models'] = new \ArrayIterator( $models );

			echo $mustache->render('model/list_full', $array_mustache);
		}elseif($word !== ''){

			$models_pub = Common\Repositories::model()->findBy(array('word' => $word), array('name'=>'asc'));
			$models = array();
			$i = 1;
			foreach ($models_pub as $item) {
				$models[] = $this->getArrayModel($item);
			}
			$array_mustache['title_page'] = 'Модели на букву &#171;' .
				mb_strtoupper(parent::getRusWord($word), 'utf-8') . '&#187;';

			$array_mustache['words'] = parent::getRusAlpfavit();

			$array_mustache['models'] = $models;
			$array_mustache['back'] = true;
			echo $mustache->render('model/list', $array_mustache);
		}else{

			$models = array();
			$array_models = Common\Repositories::model()->findBy(array('is_popular' => 1), array('name' => 'asc'), 6);
			foreach ($array_models as $item) {
				$models[] = $this->getArrayModel($item);
			}

			$array_mustache['models'] = $models;
			$array_mustache['title_page'] = 'Модели';
			$array_mustache['words'] = parent::getRusAlpfavit ();
			echo $mustache->render('model/list', $array_mustache);
		}
	}

}