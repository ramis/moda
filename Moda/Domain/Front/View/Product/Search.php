<?php
namespace Moda;

use Moda\Entity;

class Domain_Front_View_Product_Search
	extends Domain_Front_View_Abstract
{

	public function __construct ()
	{
		$mustache = parent::common();

		echo $mustache->render('product/search', array());
	}

}