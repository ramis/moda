<?php

if('glosstyle.ru' === $_SERVER['SERVER_NAME']){
	define ('APPLICATION_SITE', 'glosstyle.ru');
}elseif('moda.pamuc.ru' === $_SERVER['SERVER_NAME']){
	define ('APPLICATION_SITE', 'moda.pamuc.ru');
}else{
	die;
}

define ('PATH', realpath (dirname (__FILE__) . '/../../../../') . '/');

set_include_path (get_include_path () . PATH_SEPARATOR . PATH);

require PATH . 'vendor/autoload.php';

//bootstrap
$bootstrap = new Moda\Domain\Front\Bootstrap;

$bootstrap->initConfig ();
$bootstrap->initErrorHandler ();

$bootstrap->initImageStorage ();
$bootstrap->initSessionSimple ();

$bootstrap->initIir ();
$bootstrap->initFrontController ();
$bootstrap->initMemcached();

//front-controller
Iir\Front_Controller::run ();