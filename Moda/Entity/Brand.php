<?php
namespace Moda\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @Entity(repositoryClass="Moda\Repository\Brand")
 * @Table(name="brand")
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 **/
class Brand
{

	/**
	 * @Id @Column(type="integer") @GeneratedValue
	 * @var integer
	 */
	protected $id;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $title;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $title_rus;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $word;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $url;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $site;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $site_shop;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $founder;

	/**
	 * @Column(type="integer")
	 * @var int
	 */
	protected $year;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $description;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $annotation;

	/**
	 * @Column(type="integer")
	 * @var boolean
	 */
	protected $is_man;
	/**
	 * @Column(type="integer")
	 * @var boolean
	 */
	protected $is_woman;
	/**
	 * @Column(type="integer")
	 * @var boolean
	 */
	protected $is_baby;
	/**
	 * @Column(type="integer")
	 * @var boolean
	 */
	protected $is_accessories;
	/**
	 * @Column(type="integer")
	 * @var boolean
	 */
	protected $is_perfume;

	/**
	 * @Column(type="integer")
	 * @var boolean
	 */
	protected $is_jewelry;

	/**
	 * @Column(type="integer")
	 * @var boolean
	 */
	protected $is_popular;

	/**
	 * @OneToMany(targetEntity="Brand", mappedBy="parent")
	 */
	private $brands;

	/**
	 * @ManyToOne(targetEntity="Brand", inversedBy="brands")
	 * @JoinColumn(name="parent_id", referencedColumnName="id")
	 */
	private $parent;

	/**
	 * @ManyToOne(targetEntity="Country")
	 * @JoinColumn(name="country_id", referencedColumnName="id")
	 */
	private $country;

	/**
	 * @ManyToMany(targetEntity="Designer")
	 * @JoinTable(name="brand_designer_link",
	 *      joinColumns={@JoinColumn(name="brand_id", referencedColumnName="id")},
	 *      inverseJoinColumns={@JoinColumn(name="designer_id", referencedColumnName="id")}
	 *      )
	 */
	private $designers;

	/**
	 * @ManyToMany(targetEntity="Model")
	 * @JoinTable(name="brand_model_link",
	 *      joinColumns={@JoinColumn(name="brand_id", referencedColumnName="id")},
	 *      inverseJoinColumns={@JoinColumn(name="model_id", referencedColumnName="id")}
	 *      )
	 */
	private $models;

	/**
	 * @ManyToMany(targetEntity="Market")
	 * @JoinTable(name="brand_market_link",
	 *      joinColumns={@JoinColumn(name="brand_id", referencedColumnName="id")},
	 *      inverseJoinColumns={@JoinColumn(name="market_id", referencedColumnName="id")}
	 *      )
	 */
	private $markets;

	public function __construct() {
		$this->brands = new \Doctrine\Common\Collections\ArrayCollection();
		$this->designers = new \Doctrine\Common\Collections\ArrayCollection();
		$this->models = new \Doctrine\Common\Collections\ArrayCollection();
		$this->markets = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * $id
	 * @return int
	 */
	public function getId (){
		return $this->id;
	}

	/**
	 * $id
	 * @param int $id
	 */
	public function setId ($id){
		$this->id = $id;
	}

	/**
	 * $title
	 * @return string
	 */
	public function getTitle (){
		return $this->title;
	}

	/**
	 * $title
	 * @param string $title
	 */
	public function setTitle ($title){
		$this->title = $title;
	}

	/**
	 * $title_rus
	 * @return string
	 */
	public function getTitleRus (){
		return $this->title_rus;
	}

	/**
	 * $word
	 * @param string $word
	 */
	public function setWord ($word){
		$this->word = $word;
	}

	/**
	 * $word
	 * @return string
	 */
	public function getWord (){
		return $this->word;
	}

	/**
	 * $title_rus
	 * @param string $title_rus
	 */
	public function setTitleRus ($title_rus){
		$this->title_rus = $title_rus;
	}

	/**
	 * $url
	 * @return string
	 */
	public function getUrl (){
		return $this->url;
	}

	/**
	 * $url
	 * @param string $url
	 */
	public function setUrl ($url){
		$this->url = $url;
	}

	/**
	 * $site
	 * @return string
	 */
	public function getSite (){
		return $this->site;
	}

	/**
	 * $site
	 * @param string $site
	 */
	public function setSite ($site){
		$this->site = $site;
	}

	/**
	 * $site_shop
	 * @return string
	 */
	public function getSiteShop (){
		return $this->site_shop;
	}

	/**
	 * $site_shop
	 * @param string $site_shop
	 */
	public function setSiteShop ($site_shop){
		$this->site_shop = $site_shop;
	}

	/**
	 * $description
	 * @return string
	 */
	public function getDescription (){
		return $this->description;
	}

	/**
	 * $description
	 * @param string $description
	 */
	public function setDescription ($description){
		$this->description = $description;
	}

	/**
	 * $founder
	 * @return string
	 */
	public function getFounder (){
		return $this->founder;
	}

	/**
	 * $founder
	 * @param string $founder
	 */
	public function setFounder ($founder){
		$this->founder = $founder;
	}

	/**
	 * $year
	 * @return int
	 */
	public function getYear (){
		return $this->year;
	}

	/**
	 * $year
	 * @param int $year
	 */
	public function setYear ($year){
		$this->year = $year;
	}

	/**
	 * $annotation
	 * @return string
	 */
	public function getAnnotation (){
		return $this->annotation;
	}

	/**
	 * $annotation
	 * @param string $annotation
	 */
	public function setAnnotation ($annotation){
		$this->annotation = $annotation;
	}

	/**
	 * Add designer
	 *
	 * @param Designer $designer
	 * @return Brand
	 */
	public function addDesigner (Designer $designer)
	{
		if($this->designers->isEmpty()){
			$this->designers->add($designer);
		}elseif(array_search($designer, $this->designers->toArray(), true) === false){
			$this->designers->add($designer);
		}

		return $this;
	}

	/**
	 * Remove designer
	 *
	 * @param Designer $designer
	 */
	public function removeDesigner (Designer $designer)
	{
		$this->designers->removeElement($designer);
	}

	/**
	 * Get designers
	 *
	 * @return \Doctrine\Common\Collections\ArrayCollection of Designer
	 */
	public function getDesigners()
	{
		return $this->designers;
	}

	/**
	 * Get brands
	 *
	 * @return \Doctrine\Common\Collections\ArrayCollection of Brand
	 */
	public function getBrands()
	{
		return $this->brands;
	}

	/**
	 * Get parent
	 *
	 * @return Brand $parent
	 */
	public function getParent()
	{
		return $this->parent;
	}

	/**
	 * Set parent
	 *
	 * @param Brand $parent
	 * @return Brand
	 */
	public function setParent(Brand $parent = null)
	{
		$this->parent = $parent;
		return $this;
	}


	/**
	 * Add model
	 *
	 * @param Model $model
	 * @return Brand
	 */
	public function addModel (Model $model)
	{
		if($this->models->isEmpty()){
			$this->models->add($model);
		}elseif(array_search($model, $this->models->toArray(), true) === false){
			$this->models->add($model);
		}

		return $this;
	}

	/**
	 * Remove model
	 *
	 * @param Model $model
	 */
	public function removeModel (Model $model)
	{
		$this->models->removeElement($model);
	}

	/**
	 * Get models
	 *
	 * @return \Doctrine\Common\Collections\ArrayCollection of Model
	 */
	public function getModels()
	{
		return $this->models;
	}

	/**
	 * Get markets
	 *
	 * @return \Doctrine\Common\Collections\ArrayCollection of Market
	 */
	public function getMarkets ()
	{
		return $this->markets;
	}

	/**
	 * Get country
	 *
	 * @return Country $country
	 */
	public function getCountry ()
	{
		return $this->country;
	}

	/**
	 * Set country
	 *
	 * @param Country $country
	 * @return Brand
	 */
	public function setCountry (Country $country)
	{
		$this->country = null;

		$this->country = $country;
		return $this;
	}


	/**
	 * Строка
	 *
	 * @return string
	 */
	public function __toString () {
		$class = explode('\\', __CLASS__);
		return strtolower (end($class));
	}

	/**
	 * Логотип
	 *
	 * @return Image
	 */
	public function getMainImage (){
		return \Moda\Common\Repositories::image()->findOneBy(
			array('parent' => (string)$this, 'parent_id' => $this->getId(), 'is_main' => 1));
	}

	/**
	 * $is_man
	 *
	 * @return boolean
	 */
	public function isMan (){
		return $this->is_man;
	}

	/**
	 * $is_man
	 *
	 * @param boolean $is_man
	 */
	public function setMan ($is_man){
		$this->is_man = $is_man;
	}

	/**
	 * $is_woman
	 *
	 * @return boolean
	 */
	public function isWoman (){
		return $this->is_woman;
	}

	/**
	 * $is_woman
	 *
	 * @param boolean $is_woman
	 */
	public function setWoman ($is_woman){
		$this->is_woman = $is_woman;
	}

	/**
	 * $is_baby
	 *
	 * @return boolean
	 */
	public function isBaby (){
		return $this->is_baby;
	}

	/**
	 * $is_baby
	 *
	 * @param boolean $is_baby
	 */
	public function setBaby ($is_baby){
		$this->is_baby = $is_baby;
	}

	/**
	 * $is_accessories
	 *
	 * @return boolean
	 */
	public function isAccessories (){
		return $this->is_accessories;
	}

	/**
	 * $is_accessories
	 *
	 * @param boolean $is_accessories
	 */
	public function setAccessories ($is_accessories){
		$this->is_accessories = $is_accessories;
	}

	/**
	 * $is_perfume
	 *
	 * @return boolean
	 */
	public function isPerfume (){
		return $this->is_perfume;
	}

	/**
	 * $is_perfume
	 *
	 * @param boolean $is_perfume
	 */
	public function setPerfume ($is_perfume){
		$this->is_perfume = $is_perfume;
	}

	/**
	 * $is_jewelry
	 *
	 * @return boolean
	 */
	public function isJewelry (){
		return $this->is_jewelry;
	}

	/**
	 * $is_jewelry
	 *
	 * @param boolean $is_jewelry
	 */
	public function setJewelry ($is_jewelry){
		$this->is_jewelry = $is_jewelry;
	}

	/**
	 * $is_popular
	 *
	 * @return boolean
	 */
	public function isPopular (){
		return $this->is_popular;
	}

	/**
	 * $is_popular
	 *
	 * @param boolean $is_popular
	 */
	public function setPopular ($is_popular){
		$this->is_popular = $is_popular;
	}

}