<?php
namespace Moda\Entity\Brand;

use Moda;
use Moda\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @Entity(repositoryClass="Moda\Repository\Brand\Type")
 * @Table(name="brand_type")
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 **/

class Type {

	const SEX_MALE = 'male';
	const SEX_FEMALE = 'female';

	/**
	 * @Id @Column(type="integer") @GeneratedValue
	 * @var int
	 */
	protected $id;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $title;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $description;

	/**
	 * @Column(type="string", columnDefinition="ENUM('male', 'female')")
	 * @var string
	 */
	protected $sex;

	/**
	 * @ManyToOne(targetEntity="Moda\Entity\Type")
	 * @JoinColumn(name="type_id", referencedColumnName="id")
	 */
	private $type;

	/**
	 * @ManyToOne(targetEntity="Moda\Entity\Brand")
	 * @JoinColumn(name="brand_id", referencedColumnName="id")
	 */
	private $brand;

	/**
	 * $id
	 * @return int
	 */
	public function getId (){
		return $this->id;
	}

	/**
	 * $id
	 * @param int $id
	 */
	public function setId ($id){
		$this->id = $id;
	}

	/**
	 * $title
	 * @return string
	 */
	public function getTitle (){
		return $this->title;
	}

	/**
	 * $title
	 * @param string $title
	 */
	public function setTitle ($title){
		$this->title = $title;
	}

	/**
	 * $description
	 * @return string
	 */
	public function getDescription (){
		return $this->description;
	}

	/**
	 * $description
	 * @param string $description
	 */
	public function setDescription ($description){
		$this->description = $description;
	}

	/**
	 * $sex
	 * @return string
	 */
	public function getSex (){
		return $this->sex;
	}

	/**
	 * $sex
	 * @param string $sex
	 */
	public function setSex ($sex){

		if (!in_array($sex, array(self::SEX_FEMALE, self::SEX_MALE))) {
			throw new \InvalidArgumentException("Invalid sex");
		}

		$this->sex = $sex;
	}

	/**
	 * $type
	 * @return Entity\Type
	 */
	public function getType (){
		return $this->type;
	}

	/**
	 * $type
	 * @param Entity\Type $view
	 */
	public function setType (Entity\Type $type){
		$this->type = $type;
	}

	/**
	 * $brand
	 * @return Entity\Brand
	 */
	public function getBrand (){
		return $this->brand;
	}

	/**
	 * $brand
	 * @param Entity\Brand $brand
	 */
	public function setBrand (Entity\Brand $brand){
		$this->brand = $brand;
	}

}