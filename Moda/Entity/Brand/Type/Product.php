<?php
namespace Moda\Entity\Brand\Type;

use Moda;
use Moda\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @Entity(repositoryClass="Moda\Repository\Brand\Type\Product")
 * @Table(name="brand_type_product")
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 **/

class Product {

	/**
	 * @Id @Column(type="integer") @GeneratedValue
	 * @var int
	 */
	protected $id;

	/**
	 * @Column(type="integer")
	 * @var int
	 */
	protected $price;

	/**
	 * @Column(type="integer")
	 * @var int
	 */
	protected $sale;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $title;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $description;

	/**
	 * @ManyToOne(targetEntity="Moda\Entity\Brand\Type")
	 * @JoinColumn(name="brand_type_id", referencedColumnName="id")
	 */
	private $brand_type;

	/**
	 * @ManyToOne(targetEntity="Moda\Entity\Market")
	 * @JoinColumn(name="market_id", referencedColumnName="id")
	 */
	private $market;

	/**
	 * $id
	 * @return int
	 */
	public function getId (){
		return $this->id;
	}

	/**
	 * $id
	 * @param int $id
	 */
	public function setId ($id){
		$this->id = $id;
	}

	/**
	 * $price
	 * @return int
	 */
	public function getPrice (){
		return $this->price;
	}

	/**
	 * $price
	 * @param int $price
	 */
	public function setPrice ($price){
		$this->price = $price;
	}

	/**
	 * $sale
	 * @return int
	 */
	public function getSale (){
		return $this->sale;
	}

	/**
	 * $sale
	 * @param int $sale
	 */
	public function setSale ($sale){
		$this->sale = $sale;
	}

	/**
	 * $title
	 * @return string
	 */
	public function getTitle (){
		return $this->title;
	}

	/**
	 * $title
	 * @param string $title
	 */
	public function setTitle ($title){
		$this->title = $title;
	}

	/**
	 * $description
	 * @return string
	 */
	public function getDescription (){
		return $this->description;
	}

	/**
	 * $description
	 * @param string $description
	 */
	public function setDescription ($description){
		$this->description = $description;
	}

	/**
	 * $brand_type
	 * @return Entity\Brand\Type
	 */
	public function getBrandType (){
		return $this->brand_type;
	}

	/**
	 * $brand_type
	 * @param Entity\Brand\Type $brand_type
	 */
	public function setBrandType (Entity\Brand\Type $brand_type){
		$this->brand_type = $brand_type;
	}

	/**
	 * $market
	 * @return Entity\Market
	 */
	public function getMarket (){
		return $this->market;
	}

	/**
	 * $market
	 * @param Entity\Market $market
	 */
	public function setMarket (Entity\Market $market){
		$this->market = $market;
	}

}