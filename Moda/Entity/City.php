<?php
namespace Moda\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @Entity(repositoryClass="Moda\Repository\City")
 * @Table(name="city")
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 **/
class City
{

	/**
	 * @Id @Column(type="integer") @GeneratedValue
	 * @var integer
	 */
	protected $id;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $title;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $title_genitive;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $title_prepositional;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $url;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $description;

	/**
	 * @ManyToOne(targetEntity="Country")
	 * @JoinColumn(name="country_id", referencedColumnName="id")
	 */
	private $country;

	/**
	 * $id
	 * @return int
	 */
	public function getId (){
		return $this->id;
	}

	/**
	 * $id
	 * @param int $id
	 */
	public function setId ($id){
		$this->id = $id;
	}

	/**
	 * $title
	 * @return string
	 */
	public function getTitle (){
		return $this->title;
	}

	/**
	 * $title
	 * @param string $title
	 */
	public function setTitle ($title){
		$this->title = $title;
	}

	/**
	 * $title_genitive
	 * @return string
	 */
	public function getTitleGenitive (){
		return $this->title_genitive;
	}

	/**
	 * $title_genitive
	 * @param string $title_genitive
	 */
	public function setTitleGenitive ($title_genitive){
		$this->title_genitive = $title_genitive;
	}

	/**
	 * $title_prepositional
	 * @return string
	 */
	public function getTitlePrepositional (){
		return $this->title_prepositional;
	}

	/**
	 * $title_prepositional
	 * @param string $title_prepositional
	 */
	public function setTitlePrepositional ($title_prepositional){
		$this->title_prepositional = $title_prepositional;
	}

	/**
	 * $url
	 * @return string
	 */
	public function getUrl (){
		return $this->url;
	}

	/**
	 * $url
	 * @param string $url
	 */
	public function setUrl ($url){
		$this->url = $url;
	}

	/**
	 * $description
	 * @return string
	 */
	public function getDescription (){
		return $this->description;
	}

	/**
	 * $description
	 * @param string $description
	 */
	public function setDescription ($description){
		$this->description = $description;
	}

	/**
	 * Get country
	 *
	 * @return Country $country
	 */
	public function getCountry ()
	{
		return $this->country;
	}

	/**
	 * Set country
	 *
	 * @param Country $country
	 */
	public function setCountry (Country $country)
	{
		$this->country = $country;

		return $this;
	}

}