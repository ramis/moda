<?php
namespace Moda\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @Entity(repositoryClass="Moda\Repository\Country")
 * @Table(name="country")
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 **/
class Country {

	/**
	 * @Id @Column(type="integer") @GeneratedValue
	 * @var int
	 */
	protected $id;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $title;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $title_rus;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $description;

	/**
	 * $id
	 * @return int
	 */
	public function getId (){
		return $this->id;
	}

	/**
	 * $id
	 * @param int $id
	 */
	public function setId ($id){
		$this->id = $id;
	}

	/**
	 * $title
	 * @return string
	 */
	public function getTitle (){
		return $this->title;
	}

	/**
	 * $title
	 * @param string $title
	 */
	public function setTitle ($title){
		$this->title = $title;
	}

	/**
	 * $title_rus
	 * @return string
	 */
	public function getTitleRus (){
		return $this->title_rus;
	}

	/**
	 * $title_rus
	 * @param string $title_rus
	 */
	public function setTitleRus ($title_rus){
		$this->title_rus = $title_rus;
	}

	/**
	 * $description
	 * @return string
	 */
	public function getDescription (){
		return $this->description;
	}

	/**
	 * $description
	 * @param string $description
	 */
	public function setDescription ($description){
		$this->description = $description;
	}

}