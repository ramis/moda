<?php
namespace Moda\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @Entity(repositoryClass="Moda\Repository\Image")
 * @Table(name="image")
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 **/
class Image {

	/**
	 * @Id @Column(type="integer") @GeneratedValue
	 * @var int
	 */
	protected $id;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $hash;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $parent;

	/**
	 * @Column(type="integer")
	 * @var int
	 */
	protected $parent_id;

	/**
	 * @Column(type="integer")
	 * @var int
	 */
	protected $sort;

	/**
	 * @Column(type="integer")
	 * @var boolean
	 */
	protected $is_main;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $title;

	/**
	 * $id
	 * @return int
	 */
	public function getId (){
		return $this->id;
	}

	/**
	 * $id
	 * @param int $id
	 */
	public function setId ($id){
		$this->id = $id;
	}

	/**
	 * $hash
	 * @return string
	 */
	public function getHash (){
		return $this->hash;
	}

	/**
	 * $hash
	 * @param string $hash
	 */
	public function setHash ($hash){
		$this->hash = $hash;
	}

	/**
	 * $sort
	 * @return int
	 */
	public function getSort (){
		return $this->sort;
	}

	/**
	 * $sort
	 * @param int $sort
	 */
	public function setSort ($sort){
		$this->sort = $sort;
	}

	/**
	 * $parent_id
	 * @return int
	 */
	public function getParentId (){
		return $this->parent_id;
	}

	/**
	 * $parent_id
	 * @param int $parent_id
	 */
	public function setParentId ($parent_id){
		$this->parent_id = $parent_id;
	}

	/**
	 * $parent
	 * @return string
	 */
	public function getParent (){
		return $this->parent;
	}

	/**
	 * $parent
	 * @param string $parent
	 */
	public function setParent ($parent){
		$this->parent = $parent;
	}

	/**
	 * $title
	 * @return string
	 */
	public function getTitle (){
		return $this->title;
	}

	/**
	 * $title
	 * @param string $title
	 */
	public function setTitle ($title){
		$this->title = $title;
	}

	/**
	 * $is_main
	 * @return boolean
	 */
	public function isMain (){
		return $this->is_main;
	}

	/**
	 * $is_main
	 * @param boolean $is_main
	 */
	public function setMain ($is_main){
		$this->is_main = $is_main;
	}


}