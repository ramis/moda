<?php
namespace Moda\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @Entity(repositoryClass="Moda\Repository\Market")
 * @Table(name="market")
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 **/
class Market {

	/**
	 * @Id @Column(type="integer") @GeneratedValue
	 * @var int
	 */
	protected $id;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $title;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $address;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $phone;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $site;

	/**
	 * @Column(type="float")
	 * @var float
	 */
	protected $latitude;

	/**
	 * @Column(type="float")
	 * @var float
	 */
	protected $longitude;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $description;

	/**
	 * @ManyToOne(targetEntity="City")
	 * @JoinColumn(name="city_id", referencedColumnName="id")
	 */
	private $city;

	/**
	 * @ManyToMany(targetEntity="Brand")
	 * @JoinTable(name="brand_market_link",
	 *      joinColumns={@JoinColumn(name="market_id", referencedColumnName="id")},
	 *      inverseJoinColumns={@JoinColumn(name="brand_id", referencedColumnName="id")}
	 *      )
	 */
	private $brands;

	public function __construct (){
		$this->brands = new \Doctrine\Common\Collections\ArrayCollection();
	}
	/**
	 * $id
	 * @return int
	 */
	public function getId (){
		return $this->id;
	}

	/**
	 * $id
	 * @param int $id
	 */
	public function setId ($id){
		$this->id = $id;
	}

	/**
	 * $title
	 * @return string
	 */
	public function getTitle (){
		return $this->title;
	}

	/**
	 * $title
	 * @param string $title
	 */
	public function setTitle ($title){
		$this->title = $title;
	}

	/**
	 * $description
	 * @return string
	 */
	public function getDescription (){
		return $this->description;
	}

	/**
	 * $description
	 * @param string $description
	 */
	public function setDescription ($description){
		$this->description = $description;
	}

	/**
	 * $address
	 * @return string
	 */
	public function getAddress (){
		return $this->address;
	}

	/**
	 * $address
	 * @param string $address
	 */
	public function setAddress ($address){
		$this->address = $address;
	}

	/**
	 * $phone
	 * @return string
	 */
	public function getPhone (){
		return $this->phone;
	}

	/**
	 * $phone
	 * @param string $phone
	 */
	public function setPhone ($phone){
		$this->phone = $phone;
	}

	/**
	 * $site
	 * @return string
	 */
	public function getSite (){
		return $this->site;
	}

	/**
	 * $site
	 * @param string $site
	 */
	public function setSite ($site){
		$this->site = $site;
	}

	/**
	 * $latitude
	 * @return float
	 */
	public function getLatitude (){
		return $this->latitude;
	}

	/**
	 * $latitude
	 * @param float $latitude
	 */
	public function setLatitude ($latitude){
		$this->latitude = $latitude;
	}

	/**
	 * $longitude
	 * @return float
	 */
	public function getLongitude (){
		return $this->longitude;
	}

	/**
	 * $longitude
	 * @param float $longitude
	 */
	public function setLongitude ($longitude){
		$this->longitude = $longitude;
	}

	/**
	 * Get city
	 *
	 * @return Country $city
	 */
	public function getCity (){
		return $this->city;
	}

	/**
	 * Set city
	 *
	 * @param City $city
	 */
	public function setCity (City $city){
		$this->city = $city;

		return $this;
	}



	/**
	 * Add brand
	 *
	 * @param Brand $brand
	 * @return Brand
	 */
	public function addBrand (Brand $brand)
	{
		if($this->brands->isEmpty()){
			$this->brands->add($brand);
		}elseif(array_search($brand, $this->brands->toArray(), true) === false){
			$this->brands->add($brand);
		}

		return $this;
	}

	/**
	 * Remove brand
	 *
	 * @param Brand $brand
	 */
	public function removeBrand (Brand $brand)
	{
		$this->brands->removeElement($brand);
	}

	/**
	 * Get brands
	 *
	 * @return \Doctrine\Common\Collections\ArrayCollection of Brand
	 */
	public function getBrands ()
	{
		return $this->brands;
	}


}