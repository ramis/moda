<?php
namespace Moda\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @Entity(repositoryClass="Moda\Repository\Model")
 * @Table(name="model")
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 **/
class Model {

	/**
	 * @Id @Column(type="integer") @GeneratedValue
	 * @var int
	 */
	protected $id;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $name;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $surname;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $word;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $description;

	/**
	 * @Column(type="integer")
	 * @var boolean
	 */
	protected $is_popular;

	/**
	 * $id
	 * @return int
	 */
	public function getId (){
		return $this->id;
	}

	/**
	 * $id
	 * @param int $id
	 */
	public function setId ($id){
		$this->id = $id;
	}

	/**
	 * $name
	 * @return string
	 */
	public function getName (){
		return $this->name;
	}

	/**
	 * $name
	 * @param string $name
	 */
	public function setName ($name){
		$this->name = $name;
	}

	/**
	 * $description
	 * @return string
	 */
	public function getDescription (){
		return $this->description;
	}

	/**
	 * $description
	 * @param string $description
	 */
	public function setDescription ($description){
		$this->description = $description;
	}


	/**
	 * $surname
	 * @return string
	 */
	public function getSurname (){
		return $this->surname;
	}

	/**
	 * $surname
	 * @param string $surname
	 */
	public function setSurname ($surname){
		$this->surname = $surname;
	}

	/**
	 * $word
	 * @return string
	 */
	public function getWord (){
		return $this->word;
	}

	/**
	 * $word
	 * @param string $word
	 */
	public function setWord ($word){
		$this->word = $word;
	}

	/**
	 * Строка
	 *
	 * @return string
	 */
	public function __toString () {
		$class = explode('\\', __CLASS__);
		return strtolower (end($class));
	}

	/**
	 * Главная картинка
	 *
	 * @return Image | null
	 */
	public function getMainImage () {
		return \Moda\Common\Repositories::image()->findOneBy(
			array('parent' => (string)$this, 'parent_id' => $this->getId(), 'is_main' => 1));
	}

	/**
	 * $is_popular
	 *
	 * @return boolean
	 */
	public function isPopular (){
		return $this->is_popular;
	}

	/**
	 * $is_popular
	 *
	 * @param boolean $is_popular
	 */
	public function setPopular ($is_popular){
		$this->is_popular = $is_popular;
	}
}