<?php
namespace Moda\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @Entity(repositoryClass="Moda\Repository\Queue")
 * @Table(name="queue")
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 **/
class Queue {

	/**
	 * @Id @Column(type="integer") @GeneratedValue
	 * @var int
	 */
	protected $id;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $hash;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $title;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $worker;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $message;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $create_time;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $processed_time;

	/**
	 * @Column(type="integer")
	 * @var boolean
	 */
	protected $is_processed;

	/**
	 * @Column(type="integer")
	 * @var boolean
	 */
	protected $is_error;

	/**
	 * $id
	 * @return int
	 */
	public function getId (){
		return $this->id;
	}

	/**
	 * $id
	 * @param int $id
	 */
	public function setId ($id){
		$this->id = $id;
	}

	/**
	 * $hash
	 * @return string
	 */
	public function getHash (){
		return $this->hash;
	}

	/**
	 * $hash
	 * @param string $hash
	 */
	public function setHash ($hash){
		$this->hash = $hash;
	}

	/**
	 * $title
	 * @return string
	 */
	public function getTitle (){
		return $this->title;
	}

	/**
	 * $title
	 * @param string $title
	 */
	public function setTitle ($title){
		$this->title = $title;
	}

	/**
	 * $worker
	 * @return string
	 */
	public function getWorker (){
		return $this->worker;
	}

	/**
	 * $worker
	 * @param string $worker
	 */
	public function setWorker ($worker){
		$this->worker = $worker;
	}

	/**
	 * $message
	 * @return string
	 */
	public function getMessage (){
		return $this->message;
	}

	/**
	 * $message
	 * @param string $message
	 */
	public function setMessage ($message){
		$this->message = $message;
	}

	/**
	 * $create_time
	 * @return string
	 */
	public function getCreateTime (){
		return $this->create_time;
	}

	/**
	 * $create_time
	 * @param string $create_time
	 */
	public function setCreateTime ($create_time){
		$this->create_time = $create_time;
	}

	/**
	 * $processed_time
	 * @return string
	 */
	public function getProcessedTime (){
		return $this->processed_time;
	}

	/**
	 * $processed_time
	 * @param string $processed_time
	 */
	public function setProcessedTime ($processed_time){
		$this->processed_time = $processed_time;
	}

	/**
	 * $is_processed
	 * @return boolean
	 */
	public function isProcessed (){
		return $this->is_processed;
	}

	/**
	 * $is_processed
	 * @param boolean $is_processed
	 */
	public function setProcessed ($is_processed){
		$this->is_processed = $is_processed;
	}

	/**
	 * $is_error
	 * @return boolean
	 */
	public function isError (){
		return $this->is_error;
	}

	/**
	 * $is_error
	 * @param boolean $is_error
	 */
	public function setError ($is_error){
		$this->is_error = $is_error;
	}

}