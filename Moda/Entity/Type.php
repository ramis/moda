<?php
namespace Moda\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @Entity(repositoryClass="Moda\Repository\Type")
 * @Table(name="type")
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 **/
class Type {

	/**
	 * @Id @Column(type="integer") @GeneratedValue
	 * @var int
	 */
	protected $id;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $title;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $description;

	/**
	 * @ManyToOne(targetEntity="View")
	 * @JoinColumn(name="view_id", referencedColumnName="id")
	 */
	private $view;

	/**
	 * $id
	 * @return int
	 */
	public function getId (){
		return $this->id;
	}

	/**
	 * $id
	 * @param int $id
	 */
	public function setId ($id){
		$this->id = $id;
	}

	/**
	 * $title
	 * @return string
	 */
	public function getTitle (){
		return $this->title;
	}

	/**
	 * $title
	 * @param string $title
	 */
	public function setTitle ($title){
		$this->title = $title;
	}

	/**
	 * $description
	 * @return string
	 */
	public function getDescription (){
		return $this->description;
	}

	/**
	 * $description
	 * @param string $description
	 */
	public function setDescription ($description){
		$this->description = $description;
	}

	/**
	 * $view
	 * @return View
	 */
	public function getView (){
		return $this->view;
	}

	/**
	 * $view
	 * @param View $view
	 */
	public function setView (View $view){
		$this->view = $view;
	}

}