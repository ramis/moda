<?php
namespace Moda\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @Entity(repositoryClass="Moda\Repository\User")
 * @Table(name="user")
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 **/
class User {

	/**
	 * @Id @Column(type="integer") @GeneratedValue
	 * @var int
	 */
	protected $id;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $nick;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	protected $passwd;

	/**
	 * @Column(type="integer")
	 * @var boolean
	 */
	protected $is_active;

	/**
	 * $id
	 * @return int
	 */
	public function getId (){
		return $this->id;
	}

	/**
	 * $id
	 * @param int $id
	 */
	public function setId ($id){
		$this->id = $id;
	}

	/**
	 * $nick
	 * @return string
	 */
	public function getNick (){
		return $this->nick;
	}

	/**
	 * $nick
	 * @param string $nick
	 */
	public function setNick ($nick){
		$this->nick = $nick;
	}

	/**
	 * $passwd
	 * @return string
	 */
	public function getPasswd (){
		return $this->passwd;
	}

	/**
	 * $passwd
	 * @param string $passwd
	 */
	public function setPasswd ($passwd){
		$this->passwd = $passwd;
	}

	/**
	 * $is_active
	 * @return boolean
	 */
	public function isActive (){
		return $this->is_active;
	}

	/**
	 * $is_active
	 * @param boolean $is_active
	 */
	public function setActive ($is_active){
		$this->is_active = $is_active;
	}

}