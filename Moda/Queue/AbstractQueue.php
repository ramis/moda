<?php
namespace Moda\Queue;

use Moda;

abstract class AbstractQueue
{

	/**
	 * Info Message
	 *
	 * @param string $message
	 * @return string
	 */
	protected function info ($message){
		return '<div class="info_message"><code>' . date('Y-m-d H:i:s') . '</code> ' . $message . '</div>';
	}

	/**
	 * Error message
	 *
	 * @param string $message
	 * @return string
	 */
	protected function error ($message){
		return '<div class="error_message"><code>' . date('Y-m-d H:i:s') . '</code> ' . $message . '</div>';
	}
}