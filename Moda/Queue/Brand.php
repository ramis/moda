<?php
namespace Moda\Queue;

use Moda;
use Moda\Entity;
use Moda\Common;

class Brand extends AbstractQueue
{

	private $message;

	public function __construct ()
	{
		require_once PATH . "lib/PHPExcel.php";
	}

	/**
	 * Обработка элемент очереди
	 *
	 * @param Moda\Entity\Queue $item
	 * @return boolean
	 */
	public function process (Moda\Entity\Queue $item)
	{

		$ok = true;

		$this->message = '';

		//путь к excel файлу
		$filepath = tempnam ('', 'tmp-queue');
		if (\Image\Storage::copy ($item->getHash (), $filepath)) {
			//файл есть

			$this->message .= $this->info ('excel parse begin');

			$objReader = \PHPExcel_IOFactory::createReaderForFile($filepath);
			$objReader->setReadDataOnly(true);
			$xls = $objReader->load($filepath);

			unset($objReader);

			$this->message .= $this->info ('excel parse done');

//			if ($xls instanceof \Spreadsheet_Excel_Reader && $xls->isFileOk () === true) {
				$this->importBrand ($xls);
//			} else {
//				$this->message .= $this->error ('excel invalid');
//			}

			unset ($xls);

			//удаляем файл
			if (!unlink ($filepath)) {
				$this->message .= $this->error ('temporary file unlink fail');
			}

			if (!\Image\Storage::delete ($item->getHash ())) {
				$this->message .= $this->error ('filestorage file delete fail');
			}


			$this->message .= $this->info ('file process done');
		} else {
			//файла нет
			$this->message .= $this->error ('file does not exists');
			$ok = false;
		}

		$item->setProcessed(true);
		$item->setMessage($this->message);

		Moda\Common\Repositories::queue()->store ($item);

		return $ok;
	}

	/**
	 * Импортировать
	 *
	 * @param \PHPExcel $xls
	 * @return void
	 */
	public function importBrand (\PHPExcel $xls)
	{
		//сперва определяем строки, в которых указаны интересующие нас значения
		$row_numbers = array (
			'title' => false,
			'country' => false,
			'year' => false,
			'founder' => false,
			'site' => false,
			'site_shop' => false,
			'is_man' =>  false,
			'is_woman' =>  false,
			'is_baby' =>  false,
			'is_accessories' =>  false,
			'is_perfume' =>  false,
			'is_jewelry' =>  false,
			'description' => false,
		);

		$worksheet = $xls->getActiveSheet();
		$highestRow = $worksheet->getHighestRow();
		$highestColumn = $worksheet->getHighestColumn();
		$highestColumnIndex = \PHPExcel_Cell::columnIndexFromString($highestColumn);

		for ($row = 2; $row <= $highestRow; ++$row)
		{
			$item = $row_numbers;
			for ($col = 0; $col <= $highestColumnIndex; ++ $col)
			{
				$cell = $worksheet->getCellByColumnAndRow($col, 1);
				$key = (string) trim($cell->getValue());

				$cell = $worksheet->getCellByColumnAndRow($col, $row);
				$value = (string) trim($cell->getValue());
				if($key !== ''){
					$item[$key] = $value;
				}
			}
			$brands[] = $item;
		}
		$xls->disconnectWorksheets();
		//очищаем память
		unset ($xls);


		foreach($brands as $item){

			if($item['title'] === ''){
				$this->message .= $this->error ('Название не может быть пустым.');
				continue;
			}

			$item['title'] = strtolower($item['title']);

			$url = preg_replace('/&/', 'and', $item['title']);
			$url = preg_replace('/(\'|")/', '', $url);
			$url = preg_replace('/[^A-Za-z]/', '_', $url);

			$brand_checked = Common\Repositories::brand()->findOneBy(array('url' => $url));

			if($brand_checked instanceof Entity\Brand && $brand_checked->getTitle() !== $item['title']){
				$this->message .= $this->error ('Бренд с урлом ' . $url . ' уже есть.');
				$this->message .= $this->error ('Измените название бренда или проставте урл в ручную.');
				continue;
			}

			$brand = Common\Repositories::brand()->findOneBy(array('title' => $item['title']));

			if($brand === NULL){
				$this->message .= $this->info ('Нет такова бренда ' . $item['title'] . ' добавляем новый');
				$brand = new Entity\Brand();
			}

			$country = Common\Repositories::country()->findOneBy(array('title' => $item['country']));

			if($country === NULL && $item['country'] !== ''){
				$this->message .= $this->info ('Нет такой страны ' . $item['country'] . ' добавляем новую');

				$country = new Entity\Country();
				$country->setTitle (trim ($item['country']));
				$country->setTitleRus (trim ($item['country']));
				$country->setDescription ('');

				Common\Repositories::country()->store($country);
			}else{
				$country = null;
			}

			if($country instanceof Entity\Country){
				$brand->setCountry($country);
			}
			$brand->setTitle ($item['title']);
			$brand->setTitleRus (trim ($item['title']));

			$brand->setSite (trim($item['site']));
			$brand->setSiteShop (trim($item['site_shop']));
			$brand->setFounder(trim ($item['founder']));
			$brand->setYear((int)$item['year']);
			$brand->setDescription (trim ($item['description']));
			$brand->setAnnotation ('');
			$brand->setWord ($item['title'][0]);


			$brand->setMan ( trim ($item['is_man']) !== '');
			$brand->setWoman ( trim ($item['is_woman']) !== '');
			$brand->setBaby ( trim ($item['is_baby']) !== '');
			$brand->setAccessories ( trim ($item['is_accessories']) !== '');
			$brand->setPerfume ( trim ($item['is_perfume']) !== '');
			$brand->setJewelry ( trim ($item['is_jewelry']) !== '');
			$brand->setPopular(false);
			$brand->setUrl ($url);

			Common\Repositories::brand ()->store($brand);
			$this->message .= $this->info ('Бренд ' . $brand->getTitle() . ' добавлен');

		}
	}

}
