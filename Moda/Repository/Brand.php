<?php
// Entity/Brand.php
namespace Moda\Repository;

use Doctrine\ORM\EntityRepository;
use Moda\Entity;

/**
 * Santal
 *
 * @method Entity\Brand[] findAll
 * @method Entity\Brand find ($id)
 */
class Brand extends EntityRepository
{
	public function store (Entity\Brand $brand){
		$this->_em->persist ($brand);
		$this->_em->flush ();
		return $brand;
	}

	public function remove (Entity\Brand $brand){
		$this->_em->remove($brand);
		$this->_em->flush();
	}

}
