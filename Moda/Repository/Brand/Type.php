<?php
// Entity/Brand/Type.php
namespace Moda\Repository\Brand;

use Moda\Entity\Brand;
use Doctrine\ORM\EntityRepository;

/**
 * Santal
 *
 * @method \Moda\Entity\Brand\Type[] findAll
 * @method \Moda\Entity\Brand\Type find ($id)
 */
class Type extends EntityRepository
{
	public function store (Brand\Type $type){
		$this->_em->persist ($type);
		$this->_em->flush ();
		return $type;
	}

	public function remove (Brand\Type $type){
		$this->_em->remove($type);
		$this->_em->flush();
	}

}