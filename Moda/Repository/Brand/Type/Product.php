<?php
// Entity/Brand/Type.php
namespace Moda\Repository\Brand\Type;

use Moda\Entity\Brand\Type;
use Doctrine\ORM\EntityRepository;

/**
 * Santal
 *
 * @method \Moda\Entity\Brand\Type\Product[] findAll
 * @method \Moda\Entity\Brand\Type\Product find ($id)
 */
class Product extends EntityRepository
{
	public function store (Type\Product $brand_type){
		$this->_em->persist ($brand_type);
		$this->_em->flush ();
		return $brand_type;
	}

	public function remove (Type\Product $brand_type){
		$this->_em->remove($brand_type);
		$this->_em->flush();
	}

}