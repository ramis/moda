<?php
// Entity/City.php
namespace Moda\Repository;

use Doctrine\ORM\EntityRepository;
use Moda\Entity;

/**
 * Santal
 *
 * @method Entity\City[] findAll
 * @method Entity\City find ($id)
 */
class City extends EntityRepository
{
	public function store (Entity\City $city){
		$this->_em->persist ($city);
		$this->_em->flush ();
		return $city;
	}

	public function remove (Entity\City $city){
		$this->_em->remove($city);
		$this->_em->flush();
	}

}
