<?php
// Entity/Country.php
namespace Moda\Repository;

use Moda\Entity;
use Doctrine\ORM\EntityRepository;

/**
 * Santal
 *
 * @method Entity\Country[] findAll
 * @method Entity\Country find ($id)
 */
class Country extends EntityRepository
{
	public function store (Entity\Country $country){
		$this->_em->persist ($country);
		$this->_em->flush ();
		return $country;
	}

	public function remove (Entity\Country $country){
		$this->_em->remove($country);
		$this->_em->flush();
	}

}