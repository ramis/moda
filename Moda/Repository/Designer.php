<?php
// Entity/Designer.php
namespace Moda\Repository;

use Moda\Entity;
use Doctrine\ORM\EntityRepository;

/**
 * Santal
 *
 * @method Entity\Designer[] findAll
 * @method Entity\Designer find ($id)
 */
class Designer extends EntityRepository
{
	public function store (Entity\Designer $designer){
		$this->_em->persist ($designer);
		$this->_em->flush ();
		return $designer;
	}

	public function remove (Entity\Designer $designer){
		$this->_em->remove($designer);
		$this->_em->flush();
	}

}