<?php
// Entity/Image.php
namespace Moda\Repository;

use Moda\Entity;
use Doctrine\ORM\EntityRepository;

/**
 * Santal
 *
 * @method \Moda\Entity\Image[] findAll
 * @method \Moda\Entity\Image find ($id)
 */
class Image extends EntityRepository
{
	public function store (Entity\Image $image){
		$this->_em->persist ($image);
		$this->_em->flush ();
		return $image;
	}

	public function remove (Entity\Image $image){
		//remove file image
		\Image\Storage::delete($image->getHash());

		$this->_em->remove($image);
		$this->_em->flush();
	}

}