<?php
// Entity/Market.php
namespace Moda\Repository;

use Moda\Entity;
use Doctrine\ORM\EntityRepository;

/**
 * Santal
 *
 * @method Entity\Market[] findAll
 * @method Entity\Market find ($id)
 */
class Market extends EntityRepository
{
	public function store (Entity\Market $market){
		$this->_em->persist ($market);
		$this->_em->flush ();
		return $market;
	}

	public function remove (Entity\Market $market){
		$this->_em->remove($market);
		$this->_em->flush();
	}

}