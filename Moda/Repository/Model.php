<?php
// Entity/Model.php
namespace Moda\Repository;

use Moda\Entity;
use Doctrine\ORM\EntityRepository;

/**
 * Santal
 *
 * @method Entity\Model[] findAll
 * @method Entity\Model find ($id)
 */
class Model extends EntityRepository
{
	public function store (Entity\Model $model){
		$this->_em->persist ($model);
		$this->_em->flush ();
		return $model;
	}

	public function remove (Entity\Model $model){
		$this->_em->remove($model);
		$this->_em->flush();
	}

}