<?php
// Entity/Queue.php
namespace Moda\Repository;

use Moda\Entity;
use Doctrine\ORM\EntityRepository;

/**
 * Santal
 *
 * @method \Moda\Entity\Queue[] findAll
 * @method \Moda\Entity\Queue find ($id)
 */
class Queue extends EntityRepository
{
	public function store (Entity\Queue $queue){
		$this->_em->persist ($queue);
		$this->_em->flush ();
		return $queue;
	}

	public function remove (Entity\Queue $queue){
		//remove file
		\Image\Storage::delete($queue->getHash());

		$this->_em->remove($queue);
		$this->_em->flush();
	}

}