<?php
// Entity/Type.php
namespace Moda\Repository;

use Moda\Entity;
use Doctrine\ORM\EntityRepository;

/**
 * Santal
 *
 * @method Entity\Type[] findAll
 * @method Entity\Type find ($id)
 */
class Type extends EntityRepository
{
	public function store (Entity\Type $type){
		$this->_em->persist ($type);
		$this->_em->flush ();
		return $type;
	}

	public function remove (Entity\Type $type){
		$this->_em->remove($type);
		$this->_em->flush();
	}

}