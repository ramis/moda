<?php
// Entity/User.php
namespace Moda\Repository;

use Moda\Entity;
use Doctrine\ORM\EntityRepository;

/**
 * Santal
 *
 * @method Entity\User[] findAll
 * @method Entity\User find ($id)
 */
class User extends EntityRepository
{
	public function store (Entity\User $user){
		$this->_em->persist ($user);
		$this->_em->flush ();
		return $user;
	}

	public function remove (Entity\User $user){
		$this->_em->remove($user);
		$this->_em->flush();
	}

}