<?php
// Entity/View.php
namespace Moda\Repository;

use Moda\Entity;
use Doctrine\ORM\EntityRepository;

/**
 * Santal
 *
 * @method Entity\View[] findAll
 * @method Entity\View find ($id)
 */
class View extends EntityRepository
{
	public function store (Entity\View $view){
		$this->_em->persist ($view);
		$this->_em->flush ();
		return $view;
	}

	public function remove (Entity\View $view){
		$this->_em->remove($view);
		$this->_em->flush();
	}

}