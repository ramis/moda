CREATE TABLE `designer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `word` varchar(10) NOT NULL,
  `description` mediumtext NOT NULL,
  `is_popular` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `designer_word` (`word`),
  KEY `i_designer_popular` (`is_popular`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

CREATE TABLE `model` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `word` varchar(10) NOT NULL,
  `description` mediumtext NOT NULL,
  `is_popular` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `model_word` (`word`),
  KEY `i_model_popular` (`is_popular`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

CREATE TABLE `view` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

CREATE TABLE `type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `view_id` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_view_id` (`view_id`),
  CONSTRAINT `fk_tbl_view_id` FOREIGN KEY (`view_id`) REFERENCES `view` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;