CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nick` varchar(255) NOT NULL,
  `passwd` varchar(255) NOT NULL,
  `is_active` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `queue` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `worker` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `create_time` datetime NOT NULL,
  `processed_time` datetime DEFAULT NULL,
  `is_processed` tinyint(1) unsigned NOT NULL,
  `is_error` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

CREATE TABLE `image` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent` varchar(255) NOT NULL,
  `parent_id` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `is_main` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_is_logo` (`is_main`),
  KEY `I_image_parent_parent_id` (`parent`,`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;