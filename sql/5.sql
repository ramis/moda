CREATE TABLE `brand_designer_link` (
  `brand_id` int(10) unsigned NOT NULL,
  `designer_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`brand_id`,`designer_id`),
  KEY `fk_tbl_brand_designer_designer_id` (`designer_id`),
  CONSTRAINT `fk_tbl_brand_designer_brand_id` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_brand_designer_designer_id` FOREIGN KEY (`designer_id`) REFERENCES `designer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `brand_market_link` (
  `brand_id` int(10) unsigned NOT NULL,
  `market_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`brand_id`,`market_id`),
  KEY `fk_tbl_brand_market_market_id` (`market_id`),
  CONSTRAINT `fk_tbl_brand_market_brand_id` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_brand_market_market_id` FOREIGN KEY (`market_id`) REFERENCES `market` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `brand_model_link` (
  `brand_id` int(10) unsigned NOT NULL,
  `model_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`brand_id`,`model_id`),
  KEY `fk_tbl_brand_model_model_id` (`model_id`),
  CONSTRAINT `fk_tbl_brand_model_brand_id` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_brand_model_model_id` FOREIGN KEY (`model_id`) REFERENCES `model` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `brand_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(10) unsigned NOT NULL,
  `brand_id` int(10) unsigned NOT NULL,
  `sex` enum('male','female') NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_type_brand_link_type_id` (`type_id`),
  KEY `fk_type_brand_link_brand_id` (`brand_id`),
  CONSTRAINT `fk_type_brand_link_brand_id` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_type_brand_link_type_id` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `brand_type_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `brand_type_id` int(10) unsigned NOT NULL,
  `market_id` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `price` int(10) unsigned NOT NULL,
  `sale` int(10) unsigned NOT NULL,
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_brand_type_product_type_id` (`brand_type_id`),
  CONSTRAINT `fk_brand_type_product_type_id` FOREIGN KEY (`brand_type_id`) REFERENCES `brand_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;